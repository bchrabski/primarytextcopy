/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var moduleRef;

var runningProcess = false;

$(function () {
  if (window.RM) {
    $("#selectTermsButton").on("click", function () {
      selectGlossaryTerms();
    });

    $("#findAllTerms").on("click", function () {
      findAllGlossaryTerms();
    });

    gadgets.window.adjustHeight();

    //
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, async function (selected) {
  var outcome = "";
  if (selected.length === 1) {
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

        process(selected);
      });
    $(".status").removeClass("warning correct incorrect");
    $(".status").html("<b>Message:</b> One artifact is selected.");
  } else if (selected.length > 1) {
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");
        process(selected);
      });
    $(".status").html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // clear the display area...
    lockButton("#actionButton");
    $("#actionButton").unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html("<b>Message:</b> Select one or multiple objects.");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
  $("#actionInModuleButton").off("click");
  moduleRef = null;
});

async function process(selected) {
  var outcome = "";
  var respose;

  return new Promise(async function (resolve, reject) {
    for (var q = 0; q < selected.length; q++) {
      var obj = selected[q]; // tutaj

      var opResult = await getAttributes(obj); // tutaj

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
      } else {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("warning")
          .html(`<b>Message:</b> Processing artifact ${q + 1} out of ${selected.length}.`);

        var toSave = [];

        for (artAttrs of opResult.data) {
          var tmp = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
          var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE].name;

          if (type.indexOf("Initiating Cause, Hazards, Hazardous Situation and Harm") > -1) {
            var preP1 = artAttrs.values["Pre Likelihood of Occurrence"];
            var p2 = artAttrs.values["Pre Probability of Harm"];

            // PRE
            if (preP1 == null || p2 == null) {
              artAttrs.values["Pre Probability of Occurrence of Harm"] = null;
            } else {
              if (preP1.indexOf("1") > -1) {
                artAttrs.values["Pre Probability of Occurrence of Harm"] = 1;
              } else if (preP1.indexOf("2") > -1) {
                if (p2.indexOf("1") > -1 || p2.indexOf("2") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 1;
                } else {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 2;
                }
              } else if (preP1.indexOf("3") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 1;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 2;
                } else {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 3;
                }
              } else if (preP1.indexOf("4") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 2;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 3;
                } else if (p2.indexOf("3") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 4;
                } else {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 5;
                }
              } else if (preP1.indexOf("5") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 3;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 4;
                } else {
                  artAttrs.values["Pre Probability of Occurrence of Harm"] = 5;
                }
              }
            }

            var preP = artAttrs.values["Pre Probability of Occurrence of Harm"];
            var s = artAttrs.values["Severity"];

            // PRE Risk - to finish
            if (preP == null || s == null) {
              artAttrs.values["Pre Risk"] = null;
            } else {
              artAttrs.values["Pre Risk"] = "High";
              if (preP == "1") {
                artAttrs.values["Pre Risk"] = "Low";
              } else if (preP == "2") {
                if (s == "4") {
                  artAttrs.values["Pre Risk"] = "Medium";
                } else {
                  artAttrs.values["Pre Risk"] = "Low";
                }
              } else if (preP == "3") {
                if (s == "4") {
                  artAttrs.values["Pre Risk"] = "High";
                } else if (s == "3") {
                  artAttrs.values["Pre Risk"] = "Medium";
                } else {
                  artAttrs.values["Pre Risk"] = "Low";
                }
              } else if (preP == "4") {
                if (s == "1") {
                  artAttrs.values["Pre Risk"] = "Low";
                } else if (s == "2") {
                  artAttrs.values["Pre Risk"] = "Medium";
                } else {
                  artAttrs.values["Pre Risk"] = "High";
                }
              } else {
                if (s == "1") {
                  artAttrs.values["Pre Risk"] = "Low";
                } else {
                  artAttrs.values["Pre Risk"] = "High";
                }
              }
            }

            // FULL

            var p1 = artAttrs.values["Likelihood of Occurrence"];
            var p2 = artAttrs.values["Probability of Harm"];

            if (p1 == null || p2 == null) {
              artAttrs.values["Probability of Occurrence of Harm"] = null;
            } else {
              if (p1.indexOf("1") > -1) {
                artAttrs.values["Probability of Occurrence of Harm"] = 1;
              } else if (p1.indexOf("2") > -1) {
                if (p2.indexOf("1") > -1 || p2.indexOf("2") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 1;
                } else {
                  artAttrs.values["Probability of Occurrence of Harm"] = 2;
                }
              } else if (p1.indexOf("3") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 1;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 2;
                } else {
                  artAttrs.values["Probability of Occurrence of Harm"] = 3;
                }
              } else if (p1.indexOf("4") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 2;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 3;
                } else if (p2.indexOf("3") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 4;
                } else {
                  artAttrs.values["Probability of Occurrence of Harm"] = 5;
                }
              } else if (p1.indexOf("5") > -1) {
                if (p2.indexOf("1") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 3;
                } else if (p2.indexOf("2") > -1) {
                  artAttrs.values["Probability of Occurrence of Harm"] = 4;
                } else {
                  artAttrs.values["Probability of Occurrence of Harm"] = 5;
                }
              }
            }

            var p = artAttrs.values["Probability of Occurrence of Harm"];

            // Risk - to finish
            if (p == null || s == null) {
              artAttrs.values["Risk"] = null;
            } else {
              if (p == "1") {
                artAttrs.values["Risk"] = "Low";
              } else if (p == "2") {
                if (s == "4") {
                  artAttrs.values["Risk"] = "Medium";
                } else {
                  artAttrs.values["Risk"] = "Low";
                }
              } else if (p == "3") {
                if (s == "4") {
                  artAttrs.values["Risk"] = "High";
                } else if (s == "3") {
                  artAttrs.values["Risk"] = "Medium";
                } else {
                  artAttrs.values["Risk"] = "Low";
                }
              } else if (p == "4") {
                if (s == "1") {
                  artAttrs.values["Risk"] = "Low";
                } else if (s == "2") {
                  artAttrs.values["Risk"] = "Medium";
                } else {
                  artAttrs.values["Risk"] = "High";
                }
              } else {
                if (s == "1") {
                  artAttrs.values["Risk"] = "Low";
                } else {
                  artAttrs.values["Risk"] = "High";
                }
              }
            }
            toSave.push(artAttrs);
          }
        }
        if (toSave.length != 0) {
          var z = await modifyArtifacts(toSave);
          respose = z.code;
        } else {
          respose = RM.OperationResult.OPERATION_OK;
        }
      }

      if (respose != RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        return;
      }
    }
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("correct").html("<b>Success:</b> All selected artifacts(s) were processed.");
    resolve("");
  });
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      unlockButton("#actionInModuleButton");
      $("#actionInModuleButton").on("click", processForModule);
    } else {
      lockButton("#actionInModuleButton");
    }
  }
}

async function processForModule() {
  lockAllButtons();

  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

  var result = await getModuleArtifacts(moduleRef);

  if (result.code !== RM.OperationResult.OPERATION_OK) {
    unlockButton("#actionInModuleButton");
    unlockButton("#selectTermsButton");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
  } else {
    var tmp = [];
    for (item of result.data) {
      tmp.push(item.ref);
    }
    process(tmp);
  }
}

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning").html("");

  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();

    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");

    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
    return;
  }
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.PRIMARY_TEXT,
        RM.Data.Attributes.IS_HEADING,
        RM.Data.Attributes.ARTIFACT_TYPE,
        "Pre Likelihood of Occurrence",
        "Probability of Harm",
        "Pre Probability of Occurrence of Harm",
        "Severity",
        "Pre Risk",
        "Probability of Occurrence of Harm",
        "Likelihood of Occurrence",
        "Probability of Harm",
        "Pre Probability of Harm",
      ],
      resolve
    );
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function createTermLink(source, target) {
  return new Promise(function (resolve, reject) {
    RM.Data.createLink(source, RM.Data.LinkTypes.REFERENCES_TERM, target, resolve);
  });
}

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");
  $("#selectTermsButton").attr("disabled", "disabled");
  $("#selectTermsButton").removeClass("btn-primary");
  $("#selectTermsButton").addClass("btn-secondary");
  $("#findAllTerms").attr("disabled", "disabled");
  $("#findAllTerms").removeClass("btn-primary");
  $("#findAllTerms").addClass("btn-secondary");
  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#actionButton").removeAttr("disabled");
  $("#actionButton").addClass("btn-primary");
  $("#actionButton").removeClass("btn-secondary");
  $("#selectTermsButton").removeAttr("disabled");
  $("#selectTermsButton").addClass("btn-primary");
  $("#selectTermsButton").removeClass("btn-secondary");
  $("#findAllTerms").removeAttr("disabled");
  $("#findAllTerms").addClass("btn-primary");
  $("#findAllTerms").removeClass("btn-secondary");

  if (moduleRef == null) return;
  $("#actionInModuleButton").removeAttr("disabled");
  $("#actionInModuleButton").addClass("btn-primary");
  $("#actionInModuleButton").removeClass("btn-secondary");
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  if (button == "#actionInModuleButton" && moduleRef == null) return;
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri) {
  var tmp = uri.replace("/resources/", "/publish/resources?resourceURI=");
  var url_req = new XMLHttpRequest();
  url_req.open("GET", tmp, false);
  url_req.setRequestHeader("Accept", "application/xml");
  url_req.setRequestHeader("Content-Type", "application/xml");

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rrm/1.0/", "core");
    var newURL = url[0].innerHTML;

    if (newURL) return uri.split("/resources/")[0] + "/resources/" + newURL;
    else return uri;
  } catch (err) {
    return;
  }
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}
