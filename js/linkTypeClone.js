/**
 * Licensed Materials - Property of ReqPro.com & SmarterProcess - Copyright ReqPro 2020
 * Contact information: info@reqpro.com | info@smarterprocess.pl
 *
 *
 */

var sourceLinkType;
var targetLinkType;
var sourceDirection;
var targetDirection;
var artRef;
var debug = false;
var succeeses;
var failures;

async function coverLinks() {
  succeeses = 0;
  failures = 0;
  var targetType;
  var sourceType;
  if (targetDirection == 0) {
    targetType = new RM.LinkTypeDefinition(targetLinkType, RM.Data.LinkDirection.IN);
  } else if (targetDirection == 1) {
    targetType = new RM.LinkTypeDefinition(targetLinkType, RM.Data.LinkDirection.OUT);
  } else {
    targetType = new RM.LinkTypeDefinition(targetLinkType, RM.Data.LinkDirection.BIDIRECTIONAL);
  }

  var directionText = "_OBJ";
  if (sourceDirection == 1) directionText = "_SUB";
  else if (sourceDirection == 2) directionText = "_BI";
  else {
    directionText = "_OBJ";
  }

  for (var i = 0; i < artRef.length; i++) {
    var result = await getLinksForArtifact(artRef[i]);
    if (result.code == RM.OperationResult.OPERATION_OK) {
      var artLinks = result.data.artifactLinks;

      if (artLinks.length > 0) {
        for (var link = 0; link < artLinks.length; link++) {
          if (artLinks[link].linktype.uri == sourceLinkType && artLinks[link].linktype.direction == directionText) {
            for (var z = 0; z < artLinks[link].targets.length; z++) {
              var tarRef = artLinks[link].targets[z];

              var resultCreate = await createLink(artRef[i], targetType, tarRef);

              if (resultCreate.code == RM.OperationResult.OPERATION_OK) {
              } else {
                failures = failures + 1;
              }
              $(".status").html(
                `<b>Message:</b> Cloning links for <b> ${i + 1}/${artRef.length}</b> artifacts.
              <p><b>Errors:</b> Number of errors ${failures}</p>
              <p>Please do not perform any actions until process is finished.</p>`
              );
              gadgets.window.adjustHeight();
            }
          }
        }
      }
    }
  }

  $(".status").removeClass("warning correct incorrect");
  clearSelectedInfo();
  $("#actionButton").removeAttr("disabled");
  $(".status").addClass("correct");
  $(".status").html(`<b> Message:</b> Proccessed finished.
  <p><b> Status:</b> Links switched successfully, errors ${failures}.</p>
  `);
  gadgets.window.adjustHeight();
}

function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

function createLink(ref, linkType, tarRef) {
  return new Promise(function (resolve, reject) {
    RM.Data.createLink(ref, linkType, tarRef, resolve);
  });
}

function deleteLink(ref, linkType, tarRef) {
  return new Promise(function (resolve, reject) {
    RM.Data.deleteLink(ref, linkType, tarRef, resolve);
  });
}

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();
    sourceLinkType = prefs.getString("sourceLinkType");
    targetLinkType = prefs.getString("targetLinkType");
    sourceDirection = prefs.getInt("sourceLinkDirection");
    targetDirection = prefs.getInt("targetLinkDirection");

    $("#actionButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      $("#actionButton").attr("disabled", "disabled");
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      // DODAC KONWERSJE
      $(".status").html(
        `<b>Message:</b> Cloning links for <b>${artRef.length}</b> artifacts.
      <p>Please do not perform any actions until process is finished.</p>
      `
      );
      gadgets.window.adjustHeight();

      if (
        sourceLinkType == null ||
        sourceLinkType == "" ||
        sourceLinkType == "undefined" ||
        sourceLinkType == undefined ||
        targetLinkType == null ||
        targetLinkType == "" ||
        targetLinkType == "undefined" ||
        targetLinkType == undefined
      ) {
        clearSelectedInfo();
        $(".status").addClass("incorrect").html("<b>Error:</b> Source or target link types name can not be empty.");
        $("#actionButton").removeAttr("disabled");
      } else {
        coverLinks();
      }
    });
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    $("#artifactList").attr("disabled", "disabled");
    gadgets.window.adjustHeight();
  }
  //
});

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");
}

function covertLinkTypes(ref) {
  RM.Data.getLinkedArtifacts(ref[0], [], function (result) {
    console.log(result.code);
    var artLinks = result.data.artifactLinks;
    console.log(artLinks);
    artLinks.forEach(function (linkDefinition) {
      console.log(linkDefinition.linktype + ": " + linkDefinition.targets.length);
    });
  });
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (selected.length === 1) {
    artRef = selected;
    $("#actionButton").removeAttr("disabled");
    clearSelectedInfo();
    $(".status").html(
      `<b>Message:</b> Press button "Clone link types" to change link types for ${selected.length} artifact.`
    );
    gadgets.window.adjustHeight();
  } else if (selected.length > 1) {
    artRef = selected;
    $("#actionButton").removeAttr("disabled");
    clearSelectedInfo();
    $(".status").html(
      `<b>Message:</b> Press button "Clone link types" to change link types for ${selected.length} artifact.`
    );
    gadgets.window.adjustHeight();
  } else {
    // clear the display area...
    $("#actionButton").attr("disabled", "disabled");
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html(
      `<b>Message:</b> Please select artifacts for which You want to switch link types and then press "Clone link types" button.`
    );
    gadgets.window.adjustHeight();
  }
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}
