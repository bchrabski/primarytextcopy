/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2021
 * Contact information: info@reqpro.com
 * Validation Logic modified by SmarterProcess (B. Chrabski); August - 2022
 */

//  1. Informacja na temat opisu przepieta do calej linii
//  2. Number which artifact is processed

var moduleRef;
var artRef;
var logs = [];
var refs = [];
var errorMessages = [];

var _sePspec_Rqmt_name;
var _sePspec_Rqmt___test_Method;
var _sePspec_Rqmt___Vers_Initial;
var _sePspec_Rqmt___Vers_Rev;
var _sePspec_Rqmt__seGrp_Mission;
var _sePspec_Rqmt__xTr_Rqmt_Src;
var _sePspec_Rqmt__xTr_SPR_SCR;

var _sePspec_Rqmt_PT_system;
var _sePspec_Rqmt_PT_bullets;
var _sePspec_Rqmt_PT_x_where_lowercase;
var _sePspec_Rqmt_PT_COMMmessage;

var _sePspec_Rqmt_PT_wordsToAvoid;
var _sePspec_Rqmt_PT_wordStyleChecks;
var _sePspec_Rqmt_PT_textWithoutPeriod;
var _sePspec_Rqmt_PT_unwantedChar;
var _sePspec_Rqmt_PT_dPeriod;
var _sePspec_Rqmt_PT_nonBreakingSpace;
var _sePspec_Rqmt_PT_trailingWhiteSpaceEndText;

var _seSRS_Rqmt_name;
var _seSRS_Rqmt___test_Method;
var _seSRS_Rqmt___Vers_Initial;
var _seSRS_Rqmt___Vers_Rev;
var _seSRS_Rqmt___Vers_RevSub;
var _seSRS_Rqmt__seAssignedPOC;
var _seSRS_Rqmt__seGrp_Mission;
var _seSRS_Rqmt__seGrp_Tags;
var _seSRS_Rqmt__SRS_CSC;
var _seSRS_Rqmt__SRS_CSCSub;
var _seSRS_Rqmt__test_PassCriteria;
var _seSRS_Rqmt__xLnk_Pspec;
var _seSRS_Rqmt__xTr_SPR_SCR;
var _seSRS_Rqmt__xLnk_PspecLink;

var _seSRS_Rqmt_PT_system;
var _seSRS_Rqmt_PT_bullets;
var _seSRS_Rqmt_PT_x_where_lowercase;
var _seSRS_Rqmt_PT_MMI_Mismatch;
var _seSRS_Rqmt_PT_COMM_Mismatch;
var _seSRS_Rqmt_PT_COMMmessage;
var _seSRS_Rqmt_PT_COMMshallSend;

var _seSRS_Rqmt_PT_wordsToAvoid;
var _seSRS_Rqmt_PT_wordStyleChecks;
var _seSRS_Rqmt_PT_textWithoutPeriod;
var _seSRS_Rqmt_PT_unwantedChar;
var _seSRS_Rqmt_PT_dPeriod;
var _seSRS_Rqmt_PT_nonBreakingSpace;
var _seSRS_Rqmt_PT_trailingWhiteSpaceEndText;

var _seParameter_parameterBySRS;
var _seParameter__Parameter_Unit;
var _seParameter__Parameter_Value;
var _seParameter_PT_period;
var _seParameter_PT_notEmpty;

var term_PT_textOfTerm;

var runningProcess = false;

window.addEventListener("load", function () {
  gadgets.window.adjustHeight();
});

window.addEventListener("resize", function () {
  gadgets.window.adjustHeight();
});

$("#all-_seSRS_Rqmt-PT").on("click", function () {
  var prefs = new gadgets.Prefs();
  $("#_seSRS_Rqmt_PT_system").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_system", true);
  _seSRS_Rqmt_PT_system = true;

  $("#_seSRS_Rqmt_PT_bullets").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_bullets", true);
  _seSRS_Rqmt_PT_bullets = true;

  $("#_seSRS_Rqmt_PT_x_where_lowercase").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_x_where_lowercase", true);
  _seSRS_Rqmt_PT_x_where_lowercase = true;

  $("#_seSRS_Rqmt_PT_MMI_Mismatch").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_MMI_Mismatch", true);
  _seSRS_Rqmt_PT_MMI_Mismatch = true;

  $("#_seSRS_Rqmt_PT_COMM_Mismatch").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_COMM_Mismatch", true);
  _seSRS_Rqmt_PT_COMM_Mismatch = true;

  $("#_seSRS_Rqmt_PT_COMMmessage").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_COMMmessage", true);
  _seSRS_Rqmt_PT_COMMmessage = true;

  $("#_seSRS_Rqmt_PT_COMMshallSend").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_COMMshallSend", true);
  _seSRS_Rqmt_PT_COMMshallSend = true;

  $("#_seSRS_Rqmt_PT_wordsToAvoid").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_wordsToAvoid", true);
  _seSRS_Rqmt_PT_wordsToAvoid = true;

  $("#_seSRS_Rqmt_PT_wordStyleChecks").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_wordStyleChecks", true);
  _seSRS_Rqmt_PT_wordStyleChecks = true;

  $("#_seSRS_Rqmt_PT_textWithoutPeriod").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_textWithoutPeriod", true);
  _seSRS_Rqmt_PT_textWithoutPeriod = true;

  $("#_seSRS_Rqmt_PT_unwantedChar").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_unwantedChar", true);
  _seSRS_Rqmt_PT_unwantedChar = true;

  $("#_seSRS_Rqmt_PT_dPeriod").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_dPeriod", true);
  _seSRS_Rqmt_PT_dPeriod = true;

  $("#_seSRS_Rqmt_PT_nonBreakingSpace").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_nonBreakingSpace", true);
  _seSRS_Rqmt_PT_nonBreakingSpace = true;

  $("#_seSRS_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_trailingWhiteSpaceEndText", true);
  _seSRS_Rqmt_PT_trailingWhiteSpaceEndText = true;

  $("#_seSRS_Rqmt_PT_hardCarriageReturn").prop("checked", true);
  prefs.set("_seSRS_Rqmt_PT_hardCarriageReturn", true);
  _seSRS_Rqmt_PT_hardCarriageReturn = true;
});

$("#all-_seSRS_Rqmt").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seSRS_Rqmt_name").prop("checked", true);
  prefs.set("_seSRS_Rqmt_name", true);
  _seSRS_Rqmt_name = true;

  $("#_seSRS_Rqmt___test_Method").prop("checked", true);
  prefs.set("_seSRS_Rqmt___test_Method", true);
  _seSRS_Rqmt___test_Method = true;

  $("#_seSRS_Rqmt___Vers_Initial").prop("checked", true);
  prefs.set("_seSRS_Rqmt___Vers_Initial", true);
  _seSRS_Rqmt___Vers_Initial = true;

  $("#_seSRS_Rqmt___Vers_Rev").prop("checked", true);
  prefs.set("_seSRS_Rqmt___Vers_Rev", true);
  _seSRS_Rqmt___Vers_Rev = true;

  $("#_seSRS_Rqmt___Vers_RevSub").prop("checked", true);
  prefs.set("_seSRS_Rqmt___Vers_RevSub", true);
  _seSRS_Rqmt___Vers_RevSub = true;

  $("#_seSRS_Rqmt__seAssignedPOC").prop("checked", true);
  prefs.set("_seSRS_Rqmt__seAssignedPOC", true);
  _seSRS_Rqmt__seAssignedPOC = true;

  $("#_seSRS_Rqmt__seGrp_Mission").prop("checked", true);
  prefs.set("_seSRS_Rqmt__seGrp_Mission", true);
  _seSRS_Rqmt__seGrp_Mission = true;

  $("#_seSRS_Rqmt__seGrp_Tags").prop("checked", true);
  prefs.set("_seSRS_Rqmt__seGrp_Tags", true);
  _seSRS_Rqmt__seGrp_Tags = true;

  $("#_seSRS_Rqmt__SRS_CSC").prop("checked", true);
  prefs.set("_seSRS_Rqmt__SRS_CSC", true);
  _seSRS_Rqmt__SRS_CSC = true;

  $("#_seSRS_Rqmt__SRS_CSCSub").prop("checked", true);
  prefs.set("_seSRS_Rqmt__SRS_CSCSub", true);
  _seSRS_Rqmt__SRS_CSCSub = true;

  $("#_seSRS_Rqmt__test_PassCriteria").prop("checked", true);
  prefs.set("_seSRS_Rqmt__test_PassCriteria", true);
  _seSRS_Rqmt__test_PassCriteria = true;

  $("#_seSRS_Rqmt__xLnk_Pspec").prop("checked", true);
  prefs.set("_seSRS_Rqmt__xLnk_Pspec", true);
  _seSRS_Rqmt__xLnk_Pspec = true;

  $("#_seSRS_Rqmt__xLnk_PspecLink").prop("checked", true);
  prefs.set("_seSRS_Rqmt__xLnk_PspecLink", true);
  _seSRS_Rqmt__xLnk_PspecLink = true;

  $("#_seSRS_Rqmt__xTr_SPR_SCR").prop("checked", true);
  prefs.set("_seSRS_Rqmt__xTr_SPR_SCR", true);
  _seSRS_Rqmt__xTr_SPR_SCR = true;
});

$("#none-_seSRS_Rqmt-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seSRS_Rqmt_PT_system").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_system", false);
  _seSRS_Rqmt_PT_system = false;

  $("#_seSRS_Rqmt_PT_bullets").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_bullets", false);
  _seSRS_Rqmt_PT_bullets = false;

  $("#_seSRS_Rqmt_PT_x_where_lowercase").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_x_where_lowercase", false);
  _seSRS_Rqmt_PT_x_where_lowercase = false;

  $("#_seSRS_Rqmt_PT_MMI_Mismatch").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_MMI_Mismatch", false);
  _seSRS_Rqmt_PT_MMI_Mismatch = false;

  $("#_seSRS_Rqmt_PT_COMM_Mismatch").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_COMM_Mismatch", false);
  _seSRS_Rqmt_PT_COMM_Mismatch = false;

  $("#_seSRS_Rqmt_PT_COMMmessage").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_COMMmessage", false);
  _seSRS_Rqmt_PT_COMMmessage = false;

  $("#_seSRS_Rqmt_PT_COMMshallSend").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_COMMshallSend", false);
  _seSRS_Rqmt_PT_COMMshallSend = false;

  $("#_seSRS_Rqmt_PT_wordsToAvoid").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_wordsToAvoid", false);
  _seSRS_Rqmt_PT_wordsToAvoid = false;

  $("#_seSRS_Rqmt_PT_wordStyleChecks").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_wordStyleChecks", false);
  _seSRS_Rqmt_PT_wordStyleChecks = false;

  $("#_seSRS_Rqmt_PT_textWithoutPeriod").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_textWithoutPeriod", false);
  _seSRS_Rqmt_PT_textWithoutPeriod = false;

  $("#_seSRS_Rqmt_PT_unwantedChar").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_unwantedChar", false);
  _seSRS_Rqmt_PT_unwantedChar = false;

  $("#_seSRS_Rqmt_PT_dPeriod").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_dPeriod", false);
  _seSRS_Rqmt_PT_dPeriod = false;

  $("#_seSRS_Rqmt_PT_nonBreakingSpace").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_nonBreakingSpace", false);
  _seSRS_Rqmt_PT_nonBreakingSpace = false;

  $("#_seSRS_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_trailingWhiteSpaceEndText", false);
  _seSRS_Rqmt_PT_trailingWhiteSpaceEndText = false;

  $("#_seSRS_Rqmt_PT_hardCarriageReturn").prop("checked", false);
  prefs.set("_seSRS_Rqmt_PT_hardCarriageReturn", false);
  _seSRS_Rqmt_PT_hardCarriageReturn = false;
});

$("#none-_seSRS_Rqmt").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seSRS_Rqmt_name").prop("checked", false);
  prefs.set("_seSRS_Rqmt_name", false);
  _seSRS_Rqmt_name = false;

  $("#_seSRS_Rqmt___test_Method").prop("checked", false);
  prefs.set("_seSRS_Rqmt___test_Method", false);
  _seSRS_Rqmt___test_Method = false;

  //

  $("#_seSRS_Rqmt___Vers_Initial").prop("checked", false);
  prefs.set("_seSRS_Rqmt___Vers_Initial", false);
  _seSRS_Rqmt___Vers_Initial = false;

  $("#_seSRS_Rqmt___Vers_Rev").prop("checked", false);
  prefs.set("_seSRS_Rqmt___Vers_Rev", false);
  _seSRS_Rqmt___Vers_Rev = false;

  $("#_seSRS_Rqmt___Vers_RevSub").prop("checked", false);
  prefs.set("_seSRS_Rqmt___Vers_RevSub", false);
  _seSRS_Rqmt___Vers_RevSub = false;

  $("#_seSRS_Rqmt__seAssignedPOC").prop("checked", false);
  prefs.set("_seSRS_Rqmt__seAssignedPOC", false);
  _seSRS_Rqmt__seAssignedPOC = false;

  $("#_seSRS_Rqmt__seGrp_Mission").prop("checked", false);
  prefs.set("_seSRS_Rqmt__seGrp_Mission", false);
  _seSRS_Rqmt__seGrp_Mission = false;

  $("#_seSRS_Rqmt__seGrp_Tags").prop("checked", false);
  prefs.set("_seSRS_Rqmt__seGrp_Tags", false);
  _seSRS_Rqmt__seGrp_Tags = false;

  $("#_seSRS_Rqmt__SRS_CSC").prop("checked", false);
  prefs.set("_seSRS_Rqmt__SRS_CSC", false);
  _seSRS_Rqmt__SRS_CSC = false;

  $("#_seSRS_Rqmt__SRS_CSCSub").prop("checked", false);
  prefs.set("_seSRS_Rqmt__SRS_CSCSub", false);
  _seSRS_Rqmt__SRS_CSCSub = false;

  $("#_seSRS_Rqmt__test_PassCriteria").prop("checked", false);
  prefs.set("_seSRS_Rqmt__test_PassCriteria", false);
  _seSRS_Rqmt__test_PassCriteria = false;

  $("#_seSRS_Rqmt__xLnk_Pspec").prop("checked", false);
  prefs.set("_seSRS_Rqmt__xLnk_Pspec", false);
  _seSRS_Rqmt__xLnk_Pspec = false;

  $("#_seSRS_Rqmt__xTr_SPR_SCR").prop("checked", false);
  prefs.set("_seSRS_Rqmt__xTr_SPR_SCR", false);
  _seSRS_Rqmt__xTr_SPR_SCR = false;

  $("#_seSRS_Rqmt__xLnk_PspecLink").prop("checked", false);
  prefs.set("_seSRS_Rqmt__xLnk_PspecLink", false);
  _seSRS_Rqmt__xLnk_PspecLink = false;
});

$("#all-_seParameter-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter_PT_period").prop("checked", true);
  prefs.set("_seParameter_PT_period", true);
  _seParameter_PT_period = true;

  $("#_seParameter_PT_notEmpty").prop("checked", true);
  prefs.set("_seParameter_PT_notEmpty", true);
  _seParameter_PT_notEmpty = true;
});

$("#none-_seParameter-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter_PT_period").prop("checked", false);
  prefs.set("_seParameter_PT_period", false);
  _seParameter_PT_period = false;

  $("#_seParameter_PT_notEmpty").prop("checked", false);
  prefs.set("_seParameter_PT_notEmpty", false);
  _seParameter_PT_notEmpty = false;
});

$("#all-term-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#term_PT_textOfTerm").prop("checked", true);
  prefs.set("term_PT_textOfTerm", true);
  _seParameter_PT_period = true;

  $("#term_PT_textOfTerm").prop("checked", true);
  prefs.set("term_PT_textOfTerm", true);
  _seParameter_PT_notEmpty = true;
});

$("#none-term-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#term_PT_textOfTerm").prop("checked", false);
  prefs.set("term_PT_textOfTerm", false);
  _seParameter_PT_period = false;

  $("#term_PT_textOfTerm").prop("checked", false);
  prefs.set("term_PT_textOfTerm", false);
  _seParameter_PT_notEmpty = false;
});

$("#all-_seParameter-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter_PT_period").prop("checked", true);
  prefs.set("_seParameter_PT_period", true);
  _seParameter_PT_period = true;

  $("#_seParameter_PT_notEmpty").prop("checked", true);
  prefs.set("_seParameter_PT_notEmpty", true);
  _seParameter_PT_notEmpty = true;
});

$("#none-_seParameter-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter_PT_period").prop("checked", false);
  prefs.set("_seParameter_PT_period", false);
  _seParameter_PT_period = false;

  $("#_seParameter_PT_notEmpty").prop("checked", false);
  prefs.set("_seParameter_PT_notEmpty", false);
  _seParameter_PT_notEmpty = false;
});

$("#all-_seParameter").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter__Parameter_Value").prop("checked", true);
  prefs.set("_seParameter__Parameter_Value", true);
  _seParameter__Parameter_Value = true;

  $("#_seParameter__Parameter_Unit").prop("checked", true);
  prefs.set("_seParameter__Parameter_Unit", true);
  _seParameter__Parameter_Unit = true;

  $("#_seParameter_parameterBySRS").prop("checked", true);
  prefs.set("_seParameter_parameterBySRS", true);
  _seParameter_parameterBySRS = true;
});

$("#none-_seParameter").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_seParameter__Parameter_Value").prop("checked", false);
  prefs.set("_seParameter__Parameter_Value", false);
  _seParameter__Parameter_Value = false;

  $("#_seParameter__Parameter_Unit").prop("checked", false);
  prefs.set("_seParameter__Parameter_Unit", false);
  _seParameter__Parameter_Unit = false;

  $("#_seParameter_parameterBySRS").prop("checked", false);
  prefs.set("_seParameter_parameterBySRS", false);
  _seParameter_parameterBySRS = false;
});

$("#none-_sePspec_Rqmt-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_sePspec_Rqmt_PT_system").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_system", false);
  _sePspec_Rqmt_PT_system = false;
  $("#_sePspec_Rqmt_PT_bullets").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_bullets", false);
  _sePspec_Rqmt_PT_bullets = false;
  $("#_sePspec_Rqmt_PT_x_where_lowercase").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_x_where_lowercase", false);
  _sePspec_Rqmt_PT_x_where_lowercase = false;

  $("#_sePspec_Rqmt_PT_COMMmessage").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_COMMmessage", false);
  _sePspec_Rqmt_PT_COMMmessage = false;

  $("#_sePspec_Rqmt_PT_wordsToAvoid").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_wordsToAvoid", false);
  _sePspec_Rqmt_PT_wordsToAvoid = false;

  $("#_sePspec_Rqmt_PT_wordStyleChecks").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_wordStyleChecks", false);
  _sePspec_Rqmt_PT_wordStyleChecks = false;

  $("#_sePspec_Rqmt_PT_textWithoutPeriod").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_textWithoutPeriod", false);
  _sePspec_Rqmt_PT_textWithoutPeriod = false;

  $("#_sePspec_Rqmt_PT_unwantedChar").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_unwantedChar", false);
  _sePspec_Rqmt_PT_unwantedChar = false;

  $("#_sePspec_Rqmt_PT_MMI_Mismatch").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_MMI_Mismatch", false);
  _sePspec_Rqmt_PT_MMI_Mismatch = false;
  $("#_sePspec_Rqmt_PT_COMM_Mismatch").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_COMM_Mismatch", false);
  _sePspec_Rqmt_PT_COMM_Mismatch = false;

  $("#_sePspec_Rqmt_PT_dPeriod").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_dPeriod", false);
  _sePspec_Rqmt_PT_dPeriod = false;
  $("#_sePspec_Rqmt_PT_nonBreakingSpace").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_nonBreakingSpace", false);
  _sePspec_Rqmt_PT_nonBreakingSpace = false;
  $("#_sePspec_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_trailingWhiteSpaceEndText", false);
  _sePspec_Rqmt_PT_trailingWhiteSpaceEndText = false;
  $("#_sePspec_Rqmt_PT_hardCarriageReturn").prop("checked", false);
  prefs.set("_sePspec_Rqmt_PT_hardCarriageReturn", false);
  _sePspec_Rqmt_PT_hardCarriageReturn = false;
});

$("#none-_sePspec_Rqmt").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_sePspec_Rqmt_name").prop("checked", false);
  prefs.set("_sePspec_Rqmt_name", false);
  _sePspec_Rqmt_name = false;
  $("#_sePspec_Rqmt___test_Method").prop("checked", false);
  prefs.set("_sePspec_Rqmt___test_Method", false);
  _sePspec_Rqmt___test_Method = false;
  $("#_sePspec_Rqmt___Vers_Initial").prop("checked", false);
  prefs.set("_sePspec_Rqmt___Vers_Initial", false);
  _sePspec_Rqmt___Vers_Initial = false;
  $("#_sePspec_Rqmt___Vers_Rev").prop("checked", false);
  prefs.set("_sePspec_Rqmt___Vers_Rev", false);
  _sePspec_Rqmt___Vers_Rev = false;
  $("#_sePspec_Rqmt__seGrp_Mission").prop("checked", false);
  prefs.set("_sePspec_Rqmt__seGrp_Mission", false);
  _sePspec_Rqmt__seGrp_Mission = false;
  $("#_sePspec_Rqmt__xTr_Rqmt_Src").prop("checked", false);
  prefs.set("_sePspec_Rqmt__xTr_Rqmt_Src", false);
  _sePspec_Rqmt__xTr_Rqmt_Src = false;
  $("#_sePspec_Rqmt__xTr_SPR_SCR").prop("checked", false);
  prefs.set("_sePspec_Rqmt__xTr_SPR_SCR", false);
  _sePspec_Rqmt__xTr_SPR_SCR = false;
});

$("#all-_sePspec_Rqmt-PT").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_sePspec_Rqmt_PT_system").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_system", true);
  _sePspec_Rqmt_PT_system = true;
  $("#_sePspec_Rqmt_PT_bullets").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_bullets", true);
  _sePspec_Rqmt_PT_bullets = true;
  $("#_sePspec_Rqmt_PT_x_where_lowercase").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_x_where_lowercase", true);
  _sePspec_Rqmt_PT_x_where_lowercase = true;

  $("#_sePspec_Rqmt_PT_COMMmessage").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_COMMmessage", true);
  _sePspec_Rqmt_PT_COMMmessage = true;

  $("#_sePspec_Rqmt_PT_wordsToAvoid").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_wordsToAvoid", true);
  _sePspec_Rqmt_PT_wordsToAvoid = true;

  $("#_sePspec_Rqmt_PT_wordStyleChecks").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_wordStyleChecks", true);
  _sePspec_Rqmt_PT_wordStyleChecks = true;

  $("#_sePspec_Rqmt_PT_textWithoutPeriod").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_textWithoutPeriod", true);
  _sePspec_Rqmt_PT_textWithoutPeriod = true;

  $("#_sePspec_Rqmt_PT_unwantedChar").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_unwantedChar", true);
  _sePspec_Rqmt_PT_unwantedChar = true;

  $("#_sePspec_Rqmt_PT_MMI_Mismatch").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_MMI_Mismatch", true);
  _sePspec_Rqmt_PT_MMI_Mismatch = true;
  $("#_sePspec_Rqmt_PT_COMM_Mismatch").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_COMM_Mismatch", true);
  _sePspec_Rqmt_PT_COMM_Mismatch = true;
  $("#_sePspec_Rqmt_PT_dPeriod").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_dPeriod", true);
  _sePspec_Rqmt_PT_dPeriod = true;
  $("#_sePspec_Rqmt_PT_nonBreakingSpace").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_nonBreakingSpace", true);
  _sePspec_Rqmt_PT_nonBreakingSpace = true;
  $("#_sePspec_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_trailingWhiteSpaceEndText", true);
  _sePspec_Rqmt_PT_trailingWhiteSpaceEndText = true;
  $("#_sePspec_Rqmt_PT_hardCarriageReturn").prop("checked", true);
  prefs.set("_sePspec_Rqmt_PT_hardCarriageReturn", true);
  _sePspec_Rqmt_PT_hardCarriageReturn = true;
});

$("#all-_sePspec_Rqmt").on("click", function () {
  var prefs = new gadgets.Prefs();

  $("#_sePspec_Rqmt_name").prop("checked", true);
  prefs.set("_sePspec_Rqmt_name", true);
  _sePspec_Rqmt_name = true;
  $("#_sePspec_Rqmt___test_Method").prop("checked", true);
  prefs.set("_sePspec_Rqmt___test_Method", true);
  _sePspec_Rqmt___test_Method = true;
  $("#_sePspec_Rqmt___Vers_Initial").prop("checked", true);
  prefs.set("_sePspec_Rqmt___Vers_Initial", true);
  _sePspec_Rqmt___Vers_Initial = true;
  $("#_sePspec_Rqmt___Vers_Rev").prop("checked", true);
  prefs.set("_sePspec_Rqmt___Vers_Rev", true);
  _sePspec_Rqmt___Vers_Rev = true;
  $("#_sePspec_Rqmt__seGrp_Mission").prop("checked", true);
  prefs.set("_sePspec_Rqmt__seGrp_Mission", true);
  _sePspec_Rqmt__seGrp_Mission = true;
  $("#_sePspec_Rqmt__xTr_Rqmt_Src").prop("checked", true);
  prefs.set("_sePspec_Rqmt__xTr_Rqmt_Src", true);
  _sePspec_Rqmt__xTr_Rqmt_Src = true;
  $("#_sePspec_Rqmt__xTr_SPR_SCR").prop("checked", true);
  prefs.set("_sePspec_Rqmt__xTr_SPR_SCR", true);
  _sePspec_Rqmt__xTr_SPR_SCR = true;
});

$(".btn").on("click", function () {
  setTimeout(function () {
    gadgets.window.adjustHeight();
  }, 200);
});

$("#generateExcelReport").on("click", function () {
  $("#generateReport").attr("disabled", "disabled");
  var wb = XLSX.utils.book_new();
  wb.Props = {
    Title: "Rule Checker Results",
    Subject: "Rule Checker Results",
    Author: "Rule Checker Results",
  };

  wb.SheetNames.push("Rule Checker Results");
  var wscols = [{ wch: 6 }, { wch: 15 }, { wch: 15 }, { wch: 25 }, { wch: 40 }, { wch: 25 }, { wch: 20 }, { wch: 20 }];

  var ws_data = [
    ["ID", "CATEGORY", "RULE GROUP", "RULE DESCRIPTION", "CHNG_PROPOSAL", "NAME", "TYPE", "STATE (Rqmt_WF_01)"],
  ];

  logs = removeDuplicates(logs);

  for (var i = 0; i < logs.length; i++) {
    var id = logs[i].values[RM.Data.Attributes.IDENTIFIER];
    var name = logs[i].values[RM.Data.Attributes.NAME];
    var status = logs[i].values["State (Rqmt_WF_01)"];
    var type = logs[i].values[RM.Data.Attributes.ARTIFACT_TYPE].name;
    var cp = logs[i].values["__Chng_Proposal"];

    var uri = logs[i].ref.uri;

    for (var q = 0; q < errorMessages.length; q++) {
      var element = errorMessages[q];
      if (element[0] == uri) {
        ws_data.push([id, element[1], element[2], element[3], cp, name, type, status]);
      }
    }
  }

  var ws = XLSX.utils.aoa_to_sheet(ws_data);
  ws["!cols"] = wscols;
  wb.Sheets["Rule Checker Results"] = ws;

  var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });

  saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "RuleChecker-Results.xlsx");

  $("#generateExcelReport").removeAttr("disabled");
});

function s2ab(s) {
  var buf = new ArrayBuffer(s.length);
  var view = new Uint8Array(buf);
  for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
  return buf;
}

/// _sePspec

$("#_sePspec_Rqmt_name").on("change", function () {
  var val = $("#_sePspec_Rqmt_name").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_name", val);
  _sePspec_Rqmt_name = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt___test_Method").on("change", function () {
  var val = $("#_sePspec_Rqmt___test_Method").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt___test_Method", val);
  _sePspec_Rqmt___test_Method = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt___Vers_Initial").on("change", function () {
  var val = $("#_sePspec_Rqmt___Vers_Initial").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt___Vers_Initial", val);
  _sePspec_Rqmt___Vers_Initial = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt___Vers_Rev").on("change", function () {
  var val = $("#_sePspec_Rqmt___Vers_Rev").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt___Vers_Rev", val);
  _sePspec_Rqmt___Vers_Rev = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt__seGrp_Mission").on("change", function () {
  var val = $("#_sePspec_Rqmt__seGrp_Mission").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt__seGrp_Mission", val);
  _sePspec_Rqmt__seGrp_Mission = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt__xTr_Rqmt_Src").on("change", function () {
  var val = $("#_sePspec_Rqmt__xTr_Rqmt_Src").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt__xTr_Rqmt_Src", val);
  _sePspec_Rqmt__xTr_Rqmt_Src = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt__xTr_SPR_SCR").on("change", function () {
  var val = $("#_sePspec_Rqmt__xTr_SPR_SCR").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt__xTr_SPR_SCR", val);
  _sePspec_Rqmt__xTr_SPR_SCR = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_system").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_system").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_system", val);
  _sePspec_Rqmt_PT_system = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_bullets").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_bullets").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_bullets", val);
  _sePspec_Rqmt_PT_bullets = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_x_where_lowercase").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_x_where_lowercase").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_x_where_lowercase", val);
  _sePspec_Rqmt_PT_x_where_lowercase = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_COMMmessage").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_COMMmessage").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_COMMmessage", val);
  _sePspec_Rqmt_PT_COMMmessage = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_wordsToAvoid").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_wordsToAvoid").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_wordsToAvoid", val);
  _sePspec_Rqmt_PT_wordsToAvoid = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_wordStyleChecks").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_wordStyleChecks").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_wordStyleChecks", val);
  _sePspec_Rqmt_PT_wordStyleChecks = val;
  gadgets.window.adjustHeight();
});

//
$("#_sePspec_Rqmt_PT_textWithoutPeriod").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_textWithoutPeriod").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_textWithoutPeriod", val);
  _sePspec_Rqmt_PT_textWithoutPeriod = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_unwantedChar").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_unwantedChar").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_unwantedChar", val);
  _sePspec_Rqmt_PT_unwantedChar = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_MMI_Mismatch").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_MMI_Mismatch").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_MMI_Mismatch", val);
  _sePspec_Rqmt_PT_MMI_Mismatch = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_COMM_Mismatch").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_COMM_Mismatch").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_COMM_Mismatch", val);
  _sePspec_Rqmt_PT_COMM_Mismatch = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_dPeriod").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_dPeriod").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_dPeriod", val);
  _sePspec_Rqmt_PT_dPeriod = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_nonBreakingSpace").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_nonBreakingSpace").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_nonBreakingSpace", val);
  _sePspec_Rqmt_PT_nonBreakingSpace = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_trailingWhiteSpaceEndText").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_trailingWhiteSpaceEndText", val);
  _sePspec_Rqmt_PT_trailingWhiteSpaceEndText = val;
  gadgets.window.adjustHeight();
});

$("#_sePspec_Rqmt_PT_hardCarriageReturn").on("change", function () {
  var val = $("#_sePspec_Rqmt_PT_hardCarriageReturn").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_sePspec_Rqmt_PT_hardCarriageReturn", val);
  _sePspec_Rqmt_PT_hardCarriageReturn = val;
  gadgets.window.adjustHeight();
});

/// _seSRS_Rqmt

$("#_seSRS_Rqmt_name").on("change", function () {
  var val = $("#_seSRS_Rqmt_name").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_name", val);
  _seSRS_Rqmt_name = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt___Vers_Initial").on("change", function () {
  var val = $("#_seSRS_Rqmt___Vers_Initial").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt___Vers_Initial", val);
  _seSRS_Rqmt___Vers_Initial = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt___test_Method").on("change", function () {
  var val = $("#_seSRS_Rqmt___test_Method").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt___test_Method", val);
  _seSRS_Rqmt___test_Method = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt___Vers_Rev").on("change", function () {
  var val = $("#_seSRS_Rqmt___Vers_Rev").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt___Vers_Rev", val);
  _seSRS_Rqmt___Vers_Rev = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt___Vers_RevSub").on("change", function () {
  var val = $("#_seSRS_Rqmt___Vers_RevSub").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt___Vers_RevSub", val);
  _seSRS_Rqmt___Vers_RevSub = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__seAssignedPOC").on("change", function () {
  var val = $("#_seSRS_Rqmt__seAssignedPOC").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__seAssignedPOC", val);
  _seSRS_Rqmt__seAssignedPOC = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__seGrp_Mission").on("change", function () {
  var val = $("#_seSRS_Rqmt__seGrp_Mission").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__seGrp_Mission", val);
  _seSRS_Rqmt__seGrp_Mission = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__seGrp_Tags").on("change", function () {
  var val = $("#_seSRS_Rqmt__seGrp_Tags").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__seGrp_Tags", val);
  _seSRS_Rqmt__seGrp_Tags = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__SRS_CSC").on("change", function () {
  var val = $("#_seSRS_Rqmt__SRS_CSC").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__SRS_CSC", val);
  _seSRS_Rqmt__SRS_CSC = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__SRS_CSCSub").on("change", function () {
  var val = $("#_seSRS_Rqmt__SRS_CSCSub").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__SRS_CSCSub", val);
  _seSRS_Rqmt__SRS_CSCSub = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__test_PassCriteria").on("change", function () {
  var val = $("#_seSRS_Rqmt__test_PassCriteria").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__test_PassCriteria", val);
  _seSRS_Rqmt__test_PassCriteria = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__xLnk_Pspec").on("change", function () {
  var val = $("#_seSRS_Rqmt__xLnk_Pspec").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__xLnk_Pspec", val);
  _seSRS_Rqmt__xLnk_Pspec = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__xTr_SPR_SCR").on("change", function () {
  var val = $("#_seSRS_Rqmt__xTr_SPR_SCR").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__xTr_SPR_SCR", val);
  _seSRS_Rqmt__xTr_SPR_SCR = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt__xLnk_PspecLink").on("change", function () {
  var val = $("#_seSRS_Rqmt__xLnk_PspecLink").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt__xLnk_PspecLink", val);
  _seSRS_Rqmt__xLnk_PspecLink = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_system").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_system").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_system", val);
  _seSRS_Rqmt_PT_system = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_bullets").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_bullets").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_bullets", val);
  _seSRS_Rqmt_PT_bullets = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_x_where_lowercase").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_x_where_lowercase").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_x_where_lowercase", val);
  _seSRS_Rqmt_PT_x_where_lowercase = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_MMI_Mismatch").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_MMI_Mismatch").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_MMI_Mismatch", val);
  _seSRS_Rqmt_PT_MMI_Mismatch = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_COMM_Mismatch").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_COMM_Mismatch").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_COMM_Mismatch", val);
  _seSRS_Rqmt_PT_COMM_Mismatch = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_COMMmessage").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_COMMmessage").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_COMMmessage", val);
  _seSRS_Rqmt_PT_COMMmessage = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_COMMshallSend").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_COMMshallSend").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_COMMshallSend", val);
  _seSRS_Rqmt_PT_COMMshallSend = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_wordsToAvoid").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_wordsToAvoid").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_wordsToAvoid", val);
  _seSRS_Rqmt_PT_wordsToAvoid = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_wordStyleChecks").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_wordStyleChecks").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_wordStyleChecks", val);
  _seSRS_Rqmt_PT_wordStyleChecks = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_textWithoutPeriod").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_textWithoutPeriod").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_textWithoutPeriod", val);
  _seSRS_Rqmt_PT_textWithoutPeriod = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_unwantedChar").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_unwantedChar").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_unwantedChar", val);
  _seSRS_Rqmt_PT_unwantedChar = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_dPeriod").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_dPeriod").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_dPeriod", val);
  _seSRS_Rqmt_PT_dPeriod = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_nonBreakingSpace").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_nonBreakingSpace").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_nonBreakingSpace", val);
  _seSRS_Rqmt_PT_nonBreakingSpace = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_trailingWhiteSpaceEndText").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_trailingWhiteSpaceEndText", val);
  _seSRS_Rqmt_PT_trailingWhiteSpaceEndText = val;
  gadgets.window.adjustHeight();
});

$("#_seSRS_Rqmt_PT_hardCarriageReturn").on("change", function () {
  var val = $("#_seSRS_Rqmt_PT_hardCarriageReturn").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seSRS_Rqmt_PT_hardCarriageReturn", val);
  _seSRS_Rqmt_PT_hardCarriageReturn = val;
  gadgets.window.adjustHeight();
});

/// _seParamter

$("#_seParameter_parameterBySRS").on("change", function () {
  var val = $("#_seParameter_parameterBySRS").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seParameter_parameterBySRS", val);
  _seParameter_parameterBySRS = val;
  gadgets.window.adjustHeight();
});

$("#_seParameter__Parameter_Unit").on("change", function () {
  var val = $("#_seParameter__Parameter_Unit").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seParameter__Parameter_Unit", val);
  _seParameter__Parameter_Unit = val;
  gadgets.window.adjustHeight();
});

$("#_seParameter__Parameter_Value").on("change", function () {
  var val = $("#_seParameter__Parameter_Value").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seParameter__Parameter_Value", val);
  _seParameter__Parameter_Value = val;
  gadgets.window.adjustHeight();
});

$("#_seParameter_PT_period").on("change", function () {
  var val = $("#_seParameter_PT_period").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seParameter_PT_period", val);
  _seParameter_PT_period = val;
  gadgets.window.adjustHeight();
});

$("#_seParameter_PT_notEmpty").on("change", function () {
  var val = $("#_seParameter_PT_notEmpty").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("_seParameter_PT_notEmpty", val);
  _seParameter_PT_notEmpty = val;
  gadgets.window.adjustHeight();
});

/// term

$("#term_PT_textOfTerm").on("change", function () {
  var val = $("#term_PT_textOfTerm").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("term_PT_textOfTerm", val);
  term_PT_textOfTerm = val;
  gadgets.window.adjustHeight();
});

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();

    // pSPEC
    _sePspec_Rqmt_name = prefs.getBool("_sePspec_Rqmt_name");
    if (_sePspec_Rqmt_name == true) $("#_sePspec_Rqmt_name").prop("checked", true);

    _sePspec_Rqmt___test_Method = prefs.getBool("_sePspec_Rqmt___test_Method");
    if (_sePspec_Rqmt___test_Method == true) $("#_sePspec_Rqmt___test_Method").prop("checked", true);

    _sePspec_Rqmt___Vers_Initial = prefs.getBool("_sePspec_Rqmt___Vers_Initial");
    if (_sePspec_Rqmt___Vers_Initial == true) $("#_sePspec_Rqmt___Vers_Initial").prop("checked", true);

    _sePspec_Rqmt___Vers_Rev = prefs.getBool("_sePspec_Rqmt___Vers_Rev");
    if (_sePspec_Rqmt___Vers_Rev == true) $("#_sePspec_Rqmt___Vers_Rev").prop("checked", true);

    _sePspec_Rqmt__seGrp_Mission = prefs.getBool("_sePspec_Rqmt__seGrp_Mission");
    if (_sePspec_Rqmt__seGrp_Mission == true) $("#_sePspec_Rqmt__seGrp_Mission").prop("checked", true);

    _sePspec_Rqmt__xTr_Rqmt_Src = prefs.getBool("_sePspec_Rqmt__xTr_Rqmt_Src");
    if (_sePspec_Rqmt__xTr_Rqmt_Src == true) $("#_sePspec_Rqmt__xTr_Rqmt_Src").prop("checked", true);

    _sePspec_Rqmt__xTr_SPR_SCR = prefs.getBool("_sePspec_Rqmt__xTr_SPR_SCR");
    if (_sePspec_Rqmt__xTr_SPR_SCR == true) $("#_sePspec_Rqmt__xTr_SPR_SCR").prop("checked", true);

    _sePspec_Rqmt_PT_system = prefs.getBool("_sePspec_Rqmt_PT_system");
    if (_sePspec_Rqmt_PT_system == true) $("#_sePspec_Rqmt_PT_system").prop("checked", true);

    _sePspec_Rqmt_PT_bullets = prefs.getBool("_sePspec_Rqmt_PT_bullets");
    if (_sePspec_Rqmt_PT_bullets == true) $("#_sePspec_Rqmt_PT_bullets").prop("checked", true);

    _sePspec_Rqmt_PT_x_where_lowercase = prefs.getBool("_sePspec_Rqmt_PT_x_where_lowercase");
    if (_sePspec_Rqmt_PT_x_where_lowercase == true) $("#_sePspec_Rqmt_PT_x_where_lowercase").prop("checked", true);

    _sePspec_Rqmt_PT_COMMmessage = prefs.getBool("_sePspec_Rqmt_PT_COMMmessage");
    if (_sePspec_Rqmt_PT_COMMmessage == true) $("#_sePspec_Rqmt_PT_COMMmessage").prop("checked", true);

    _sePspec_Rqmt_PT_wordsToAvoid = prefs.getBool("_sePspec_Rqmt_PT_wordsToAvoid");
    if (_sePspec_Rqmt_PT_wordsToAvoid == true) $("#_sePspec_Rqmt_PT_wordsToAvoid").prop("checked", true);

    _sePspec_Rqmt_PT_wordStyleChecks = prefs.getBool("_sePspec_Rqmt_PT_wordStyleChecks");
    if (_sePspec_Rqmt_PT_wordStyleChecks == true) $("#_sePspec_Rqmt_PT_wordStyleChecks").prop("checked", true);

    _sePspec_Rqmt_PT_textWithoutPeriod = prefs.getBool("_sePspec_Rqmt_PT_textWithoutPeriod");
    if (_sePspec_Rqmt_PT_textWithoutPeriod == true) $("#_sePspec_Rqmt_PT_textWithoutPeriod").prop("checked", true);

    _sePspec_Rqmt_PT_unwantedChar = prefs.getBool("_sePspec_Rqmt_PT_unwantedChar");
    if (_sePspec_Rqmt_PT_unwantedChar == true) $("#_sePspec_Rqmt_PT_unwantedChar").prop("checked", true);

    _sePspec_Rqmt_PT_MMI_Mismatch = prefs.getBool("_sePspec_Rqmt_PT_MMI_Mismatch");
    if (_sePspec_Rqmt_PT_MMI_Mismatch == true) $("#_sePspec_Rqmt_PT_MMI_Mismatch").prop("checked", true);

    _sePspec_Rqmt_PT_COMM_Mismatch = prefs.getBool("_sePspec_Rqmt_PT_COMM_Mismatch");
    if (_sePspec_Rqmt_PT_COMM_Mismatch == true) $("#_sePspec_Rqmt_PT_COMM_Mismatch").prop("checked", true);

    _sePspec_Rqmt_PT_dPeriod = prefs.getBool("_sePspec_Rqmt_PT_dPeriod");
    if (_sePspec_Rqmt_PT_dPeriod == true) $("#_sePspec_Rqmt_PT_dPeriod").prop("checked", true);

    _sePspec_Rqmt_PT_nonBreakingSpace = prefs.getBool("_sePspec_Rqmt_PT_nonBreakingSpace");
    if (_sePspec_Rqmt_PT_nonBreakingSpace == true) $("#_sePspec_Rqmt_PT_nonBreakingSpace").prop("checked", true);

    _sePspec_Rqmt_PT_trailingWhiteSpaceEndText = prefs.getBool("_sePspec_Rqmt_PT_trailingWhiteSpaceEndText");
    if (_sePspec_Rqmt_PT_trailingWhiteSpaceEndText == true)
      $("#_sePspec_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", true);

    _sePspec_Rqmt_PT_hardCarriageReturn = prefs.getBool("_sePspec_Rqmt_PT_hardCarriageReturn");
    if (_sePspec_Rqmt_PT_hardCarriageReturn == true) $("#_sePspec_Rqmt_PT_hardCarriageReturn").prop("checked", true);

    /// SRS

    _seSRS_Rqmt_name = prefs.getBool("_seSRS_Rqmt_name");
    if (_seSRS_Rqmt_name == true) $("#_seSRS_Rqmt_name").prop("checked", true);

    _seSRS_Rqmt___test_Method = prefs.getBool("_seSRS_Rqmt___test_Method");
    if (_seSRS_Rqmt___test_Method == true) $("#_seSRS_Rqmt___test_Method").prop("checked", true);

    _seSRS_Rqmt___Vers_Initial = prefs.getBool("_seSRS_Rqmt___Vers_Initial");
    if (_seSRS_Rqmt___Vers_Initial == true) $("#_seSRS_Rqmt___Vers_Initial").prop("checked", true);

    _seSRS_Rqmt___Vers_Rev = prefs.getBool("_seSRS_Rqmt___Vers_Rev");
    if (_seSRS_Rqmt___Vers_Rev == true) $("#_seSRS_Rqmt___Vers_Rev").prop("checked", true);

    _seSRS_Rqmt___Vers_RevSub = prefs.getBool("_seSRS_Rqmt___Vers_RevSub");
    if (_seSRS_Rqmt___Vers_RevSub == true) $("#_seSRS_Rqmt___Vers_RevSub").prop("checked", true);

    _seSRS_Rqmt__seAssignedPOC = prefs.getBool("_seSRS_Rqmt__seAssignedPOC");
    if (_seSRS_Rqmt__seAssignedPOC == true) $("#_seSRS_Rqmt__seAssignedPOC").prop("checked", true);

    _seSRS_Rqmt__seGrp_Mission = prefs.getBool("_seSRS_Rqmt__seGrp_Mission");
    if (_seSRS_Rqmt__seGrp_Mission == true) $("#_seSRS_Rqmt__seGrp_Mission").prop("checked", true);

    _seSRS_Rqmt__seGrp_Tags = prefs.getBool("_seSRS_Rqmt__seGrp_Tags");
    if (_seSRS_Rqmt__seGrp_Tags == true) $("#_seSRS_Rqmt__seGrp_Tags").prop("checked", true);

    _seSRS_Rqmt__SRS_CSC = prefs.getBool("_seSRS_Rqmt__SRS_CSC");
    if (_seSRS_Rqmt__SRS_CSC == true) $("#_seSRS_Rqmt__SRS_CSC").prop("checked", true);

    _seSRS_Rqmt__SRS_CSCSub = prefs.getBool("_seSRS_Rqmt__SRS_CSCSub");
    if (_seSRS_Rqmt__SRS_CSCSub == true) $("#_seSRS_Rqmt__SRS_CSCSub").prop("checked", true);

    _seSRS_Rqmt__test_PassCriteria = prefs.getBool("_seSRS_Rqmt__test_PassCriteria");
    if (_seSRS_Rqmt__test_PassCriteria == true) $("#_seSRS_Rqmt__test_PassCriteria").prop("checked", true);

    _seSRS_Rqmt__xLnk_Pspec = prefs.getBool("_seSRS_Rqmt__xLnk_Pspec");
    if (_seSRS_Rqmt__xLnk_Pspec == true) $("#_seSRS_Rqmt__xLnk_Pspec").prop("checked", true);

    _seSRS_Rqmt__xTr_SPR_SCR = prefs.getBool("_seSRS_Rqmt__xTr_SPR_SCR");
    if (_seSRS_Rqmt__xTr_SPR_SCR == true) $("#_seSRS_Rqmt__xTr_SPR_SCR").prop("checked", true);

    _seSRS_Rqmt__xLnk_PspecLink = prefs.getBool("_seSRS_Rqmt__xLnk_PspecLink");
    if (_seSRS_Rqmt__xLnk_PspecLink == true) $("#_seSRS_Rqmt__xLnk_PspecLink").prop("checked", true);

    _seSRS_Rqmt_PT_system = prefs.getBool("_seSRS_Rqmt_PT_system");
    if (_seSRS_Rqmt_PT_system == true) $("#_seSRS_Rqmt_PT_system").prop("checked", true);

    _seSRS_Rqmt_PT_bullets = prefs.getBool("_seSRS_Rqmt_PT_bullets");
    if (_seSRS_Rqmt_PT_bullets == true) $("#_seSRS_Rqmt_PT_bullets").prop("checked", true);

    _seSRS_Rqmt_PT_x_where_lowercase = prefs.getBool("_seSRS_Rqmt_PT_x_where_lowercase");
    if (_seSRS_Rqmt_PT_x_where_lowercase == true) $("#_seSRS_Rqmt_PT_x_where_lowercase").prop("checked", true);

    _seSRS_Rqmt_PT_MMI_Mismatch = prefs.getBool("_seSRS_Rqmt_PT_MMI_Mismatch");
    if (_seSRS_Rqmt_PT_MMI_Mismatch == true) $("#_seSRS_Rqmt_PT_MMI_Mismatch").prop("checked", true);

    _seSRS_Rqmt_PT_COMM_Mismatch = prefs.getBool("_seSRS_Rqmt_PT_COMM_Mismatch");
    if (_seSRS_Rqmt_PT_COMM_Mismatch == true) $("#_seSRS_Rqmt_PT_COMM_Mismatch").prop("checked", true);

    _seSRS_Rqmt_PT_COMMmessage = prefs.getBool("_seSRS_Rqmt_PT_COMMmessage");
    if (_seSRS_Rqmt_PT_COMMmessage == true) $("#_seSRS_Rqmt_PT_COMMmessage").prop("checked", true);

    _seSRS_Rqmt_PT_COMMshallSend = prefs.getBool("_seSRS_Rqmt_PT_COMMshallSend");
    if (_seSRS_Rqmt_PT_COMMshallSend == true) $("#_seSRS_Rqmt_PT_COMMshallSend").prop("checked", true);

    _seSRS_Rqmt_PT_wordsToAvoid = prefs.getBool("_seSRS_Rqmt_PT_wordsToAvoid");
    if (_seSRS_Rqmt_PT_wordsToAvoid == true) $("#_seSRS_Rqmt_PT_wordsToAvoid").prop("checked", true);

    _seSRS_Rqmt_PT_wordStyleChecks = prefs.getBool("_seSRS_Rqmt_PT_wordStyleChecks");
    if (_seSRS_Rqmt_PT_wordStyleChecks == true) $("#_seSRS_Rqmt_PT_wordStyleChecks").prop("checked", true);

    _seSRS_Rqmt_PT_textWithoutPeriod = prefs.getBool("_seSRS_Rqmt_PT_textWithoutPeriod");
    if (_seSRS_Rqmt_PT_textWithoutPeriod == true) $("#_seSRS_Rqmt_PT_textWithoutPeriod").prop("checked", true);

    _seSRS_Rqmt_PT_unwantedChar = prefs.getBool("_seSRS_Rqmt_PT_unwantedChar");
    if (_seSRS_Rqmt_PT_unwantedChar == true) $("#_seSRS_Rqmt_PT_unwantedChar").prop("checked", true);

    _seSRS_Rqmt_PT_dPeriod = prefs.getBool("_seSRS_Rqmt_PT_dPeriod");
    if (_seSRS_Rqmt_PT_dPeriod == true) $("#_seSRS_Rqmt_PT_dPeriod").prop("checked", true);

    _seSRS_Rqmt_PT_nonBreakingSpace = prefs.getBool("_seSRS_Rqmt_PT_nonBreakingSpace");
    if (_seSRS_Rqmt_PT_nonBreakingSpace == true) $("#_seSRS_Rqmt_PT_nonBreakingSpace").prop("checked", true);

    _seSRS_Rqmt_PT_trailingWhiteSpaceEndText = prefs.getBool("_seSRS_Rqmt_PT_trailingWhiteSpaceEndText");
    if (_seSRS_Rqmt_PT_trailingWhiteSpaceEndText == true)
      $("#_seSRS_Rqmt_PT_trailingWhiteSpaceEndText").prop("checked", true);

    _seSRS_Rqmt_PT_hardCarriageReturn = prefs.getBool("_seSRS_Rqmt_PT_hardCarriageReturn");
    if (_seSRS_Rqmt_PT_hardCarriageReturn == true) $("#_seSRS_Rqmt_PT_hardCarriageReturn").prop("checked", true);

    // _seParamter

    _seParameter_parameterBySRS = prefs.getBool("_seParameter_parameterBySRS");
    if (_seParameter_parameterBySRS == true) $("#_seParameter_parameterBySRS").prop("checked", true);

    _seParameter__Parameter_Unit = prefs.getBool("_seParameter__Parameter_Unit");
    if (_seParameter__Parameter_Unit == true) $("#_seParameter__Parameter_Unit").prop("checked", true);

    _seParameter__Parameter_Value = prefs.getBool("_seParameter__Parameter_Value");
    if (_seParameter__Parameter_Value == true) $("#_seParameter__Parameter_Value").prop("checked", true);

    _seParameter_PT_period = prefs.getBool("_seParameter_PT_period");
    if (_seParameter_PT_period == true) $("#_seParameter_PT_period").prop("checked", true);

    _seParameter_PT_notEmpty = prefs.getBool("_seParameter_PT_notEmpty");
    if (_seParameter_PT_notEmpty == true) $("#_seParameter_PT_notEmpty").prop("checked", true);

    // term

    term_PT_textOfTerm = prefs.getBool("term_PT_textOfTerm");
    if (term_PT_textOfTerm == true) $("#term_PT_textOfTerm").prop("checked", true);

    $("#verifyButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      lockAllButtons();
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html("<b>Message:</b> Verifying selected artifacts. PLEASE WAIT...");
      runningProcess = true;
      verifyArtifacts(artRef);

      gadgets.window.adjustHeight();
    });
  } else {
    clearSelectedInfo();
    lockButton("#verifyButton");
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

async function verifyArtifactsInModule() {
  runningProcess = true;
  logs = [];
  refs = [];
  errorMessages = [];
  $("#logs").attr("hidden", "hidden");
  $("#log").html(
    `<p class="p-1 m-0"><b>Log:</b><span id="all" class="badge badge-success" style="float:right;">Select all</span><br/><span id="none" class="badge badge-danger" style="float:right;">Deselect all</span></p>`
  );
  enableResultsControl();
  lockAllButtons();
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning");
  $(".status").html("<b>Message:</b> Verifying selected artifacts. PLEASE WAIT..");
  gadgets.window.adjustHeight();

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var opResult = await getModuleArtifacts(moduleRef);

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockButton("#verifyInModuleButton");
        $("#rulesButton").removeAttr("disabled");
        $("#rulesButton").addClass("btn-warning");
        $("#rulesButton").removeClass("btn-secondary");
        $("#termButton").removeAttr("disabled");
        $("#termButton").removeClass("btn-secondary");
        $("#termButton").addClass("btn-dark");
        $("#_seParameterButton").removeAttr("disabled");
        $("#_seParameterButton").removeClass("btn-secondary");
        $("#_seParameterButton").addClass("btn-dark");
        $("#_seSRS_RqmtButton").removeAttr("disabled");
        $("#_seSRS_RqmtButton").removeClass("btn-secondary");
        $("#_seSRS_RqmtButton").addClass("btn-dark");
        $("#_sePspec_RqmButton").removeAttr("disabled");
        $("#_sePspec_RqmButton").removeClass("btn-secondary");
        $("#_sePspec_RqmButton").addClass("btn-dark");
        $(".status").removeClass("warning correct incorrect");
        runningProcess = false;
        $(".status")
          .addClass("incorrect")
          .html(
            "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
          );
      } else {
        var data = opResult.data;
        data = removeDuplicatesOfArtifacts(data);
        for (var k = 0; k < data.length; k++) {
          $(".status").removeClass("warning correct incorrect");
          $(".status").addClass("warning");
          var id = data[k].values[RM.Data.Attributes.IDENTIFIER];
          $(".status").html(
            `<b>Message:</b> Verifying artifact <b>${id}</b> (Progress: <b>${k + 1}/${data.length}</b>).`
          );
          await validate(data[k]);
        }

        await displayLogs(logs, errorMessages);

        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("correct");
        if (logs.length < 1) {
          $(".status").html("<b>Message:</b> Verification process is complete - NO ISSUES found");
        } else {
          var uniqueLogs = removeDuplicates(logs).length;
          $(".status").html(
            `<b>Message:</b> Verification process is complete - ${uniqueLogs} of artifacts with findings: ` +
              logs.length
          );
        }
        gadgets.window.adjustHeight();
        runningProcess = false;
      }
    }
  });
}

async function verifyArtifacts(ref) {
  logs = [];
  errorMessages = [];
  refs = [];
  $("#logs").attr("hidden", "hidden");
  $("#log").html(`<p class="p-1 m-0"><b>Log:</b></p>`);

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;

      RM.Data.getAttributes(
        ref /* ArtifactAttributes[] */, // RULES#
        [
          RM.Data.Attributes.ARTIFACT_TYPE,
          RM.Data.Attributes.NAME,
          RM.Data.Attributes.IDENTIFIER,
          RM.Data.Attributes.PRIMARY_TEXT,
          "State (Rqmt_WF_01)",
          "_Parameter_Value",
          "_Parameter_Unit/Type",
          "__Chng_Proposal",
          "__Subsystems",
          "__test_Method",
          "__test_SafeLOR",
          "__Vers_Initial",
          "__Vers_Rev",
          "__Vers_RevSub",
          "_seGrp_Mission",
          "_seGrp_Tags",
          "_seAssignedPOC",
          "_seSRS_CSC",
          "_seSRS_CSCSub",
          "_test_PassCriteria",
          "_xLnk_Pspec",
          "_xTr_Rqmt_Src",
          "_xTr_SPR_SCR",
          "yRuleBreak_OK",
        ],

        async function (opResult) {
          if (opResult.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            lockButton("#verifyButton");
            $("#rulesButton").removeAttr("disabled");
            $("#rulesButton").addClass("btn-warning");
            $("#rulesButton").removeClass("btn-secondary");

            $("#termButton").removeAttr("disabled");
            $("#termButton").removeClass("btn-secondary");
            $("#termButton").addClass("btn-dark");

            $("#_seParameterButton").removeAttr("disabled");
            $("#_seParameterButton").removeClass("btn-secondary");
            $("#_seParameterButton").addClass("btn-dark");

            $("#_seSRS_RqmtButton").removeAttr("disabled");
            $("#_seSRS_RqmtButton").removeClass("btn-secondary");
            $("#_seSRS_RqmtButton").addClass("btn-dark");

            $("#_sePspec_RqmButton").removeAttr("disabled");
            $("#_sePspec_RqmButton").removeClass("btn-secondary");
            $("#_sePspec_RqmButton").addClass("btn-dark");
            runningProcess = false;
            $(".status")
              .addClass("incorrect")
              .html(
                "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
              );

            return;
          } else {
            var data = opResult.data;
            data = removeDuplicatesOfArtifacts(data);
            for (var k = 0; k < data.length; k++) {
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("warning");
              var id = data[k].values[RM.Data.Attributes.IDENTIFIER];
              $(".status").html(
                `<b>Message:</b> Verifying artifact <b>${id}</b> (Progress: <b>${k + 1}/${data.length}</b>).`
              );
              await validate(data[k]);
            }

            await displayLogs(logs, errorMessages);

            unlockAllButtons();
            $(".status").removeClass("warning correct incorrect");
            $(".status").addClass("correct");
            if (logs.length < 1) {
              $(".status").html("<b>Message:</b> Verification process is complete - NO ISSUES found.");
            } else {
              var uniqueLogs = removeDuplicates(logs).length;
              $(".status").html(
                `<b>Message:</b> Verification process is complete - ${uniqueLogs} of artifacts with findings: ` +
                  logs.length
              );
            }
            gadgets.window.adjustHeight();
            runningProcess = false;
          }
        }
      );
    }
  });
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (!runningProcess) {
    if (moduleRef != null) {
      unlockButton("#verifyInModuleButton");
    }

    if (selected.length === 1) {
      artRef = selected;
      unlockButton("#verifyButton");

      clearSelectedInfo();
      $(".status").html(`<b>Message:</b> Press button to Verify ${selected.length} artifact(s)`);
    } else if (selected.length > 1) {
      artRef = selected;
      unlockButton("#verifyButton");

      clearSelectedInfo();
      $(".status").html(`<b>Message:</b> Press button to Verify ${selected.length} artifact(s)`);
    } else {
      // clear the display area...
      lockButton("#verifyButton");
      clearSelectedInfo();
      // inform the user that they need to select only one thing.
      $(".status").html(
        `<b>Message:</b> Please select artifacts (e.g., requirements) for verification and then press "Verify selected artifacts" button.`
      );
      gadgets.window.adjustHeight();
    }
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  lockButton("#verifyInModuleButton");
  $("#verifyInModuleButton").off("click");
  moduleRef = null;
  ref = null;
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();
    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");
    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    clearSelectedInfo();
    $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
    lockButton("#verifyButton");
    return;
  }
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];
    if (
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE ||
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.COLLECTION
    ) {
      if (moduleRef != attrs.ref) {
        moduleRef = attrs.ref;
        lockButton("#verifyButton");
        unlockButton("#verifyInModuleButton");
        $("#verifyInModuleButton").on("click", verifyArtifactsInModule);
      }
    }
  }
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(
      // RULES#
      ref,
      [
        RM.Data.Attributes.ARTIFACT_TYPE,
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.IDENTIFIER,
        RM.Data.Attributes.PRIMARY_TEXT,
        "State (Rqmt_WF_01)",
        "_Parameter_Value",
        "_Parameter_Unit/Type",
        "__Chng_Proposal",
        "__Subsystems",
        "__test_Method",
        "__test_SafeLOR",
        "__Vers_Initial",
        "__Vers_Rev",
        "__Vers_RevSub",
        "_seGrp_Mission",
        "_seGrp_Tags",
        "_seAssignedPOC",
        "_seSRS_CSC",
        "_seSRS_CSCSub",
        "_test_PassCriteria",
        "_xLnk_Pspec",
        "_xTr_Rqmt_Src",
        "_xTr_SPR_SCR",
        "yRuleBreak_OK",
      ],
      resolve
    );
  });
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

function lockAllButtons() {
  $("#verifyButton").attr("disabled", "disabled");
  $("#verifyButton").removeClass("btn-primary");
  $("#verifyButton").addClass("btn-secondary");
  $("#verifyInModuleButton").attr("disabled", "disabled");
  $("#verifyInModuleButton").removeClass("btn-primary");
  $("#verifyInModuleButton").addClass("btn-secondary");

  $("#rulesButton").attr("disabled", "disabled");
  $("#rulesButton").removeClass("btn-warning");
  $("#rulesButton").addClass("btn-secondary");

  $("#_sePspec_RqmButton").attr("disabled", "disabled");
  $("#_sePspec_RqmButton").removeClass("btn-dark");
  $("#_sePspec_RqmButton").addClass("btn-secondary");

  $("#_seSRS_RqmtButton").attr("disabled", "disabled");
  $("#_seSRS_RqmtButton").removeClass("btn-dark");
  $("#_seSRS_RqmtButton").addClass("btn-secondary");

  $("#_seParameterButton").attr("disabled", "disabled");
  $("#_seParameterButton").removeClass("btn-dark");
  $("#_seParameterButton").addClass("btn-secondary");

  $("#termButton").attr("disabled", "disabled");
  $("#termButton").removeClass("btn-dark");
  $("#termButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  if (moduleRef == null) {
    $("#verifyButton").removeAttr("disabled");
    $("#verifyButton").addClass("btn-primary");
    $("#verifyButton").removeClass("btn-secondary");
  }
  $("#rulesButton").removeAttr("disabled");
  $("#rulesButton").addClass("btn-warning");
  $("#rulesButton").removeClass("btn-secondary");

  $("#termButton").removeAttr("disabled");
  $("#termButton").removeClass("btn-secondary");
  $("#termButton").addClass("btn-dark");

  $("#_seParameterButton").removeAttr("disabled");
  $("#_seParameterButton").removeClass("btn-secondary");
  $("#_seParameterButton").addClass("btn-dark");

  $("#_seSRS_RqmtButton").removeAttr("disabled");
  $("#_seSRS_RqmtButton").removeClass("btn-secondary");
  $("#_seSRS_RqmtButton").addClass("btn-dark");

  $("#_sePspec_RqmButton").removeAttr("disabled");
  $("#_sePspec_RqmButton").removeClass("btn-secondary");
  $("#_sePspec_RqmButton").addClass("btn-dark");

  if (moduleRef == null) return;
  $("#verifyInModuleButton").removeAttr("disabled");
  $("#verifyInModuleButton").addClass("btn-primary");
  $("#verifyInModuleButton").removeClass("btn-secondary");
}
function removeDuplicates(arr) {
  const uniqueIds = [];

  const unique = arr.filter((element) => {
    const isDuplicate = uniqueIds.includes(element.ref);

    if (!isDuplicate) {
      uniqueIds.push(element.ref);

      return true;
    }
  });
  return unique;
}

// FUNCTION VALIDATE ARTIFACTS
async function validate(artAttrs) {
  var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
  var name = artAttrs.values[RM.Data.Attributes.NAME];
  if (name == null) name = "";
  name = name.toUpperCase();

  var changeProposal = new String(artAttrs.values["__Chng_Proposal"]);

  var primaryText = new String(artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT]);

  // link

  var pt_clean = primaryText;
  pt_clean = pt_clean.trim();
  pt_clean = pt_clean.replace(/(<\/p>)/gi, "\n");
  pt_clean = pt_clean.replace(/(<br\/>)/gi, "\n");
  pt_clean = pt_clean.replace(/(<br>)/gi, "\n");
  pt_clean = pt_clean.replace(/(<\/li>)/gi, "\n");
  pt_clean = pt_clean.replace(/(<\/ul>)/gi, "\n");
  pt_clean = pt_clean.replace(/(<([^>]+)>)/gi, "");
  pt_clean = pt_clean.trim();

  var __test_Method = artAttrs.values["__test_Method"];
  if (__test_Method == null || __test_Method == undefined) __test_Method = "";

  var _SRS_CSC = artAttrs.values["_seSRS_CSC"];
  if (_SRS_CSC == null || _SRS_CSC == undefined) _SRS_CSC = "";

  var yRuleBreak_OK = new String(artAttrs.values["yRuleBreak_OK"]);
  if (yRuleBreak_OK == null || yRuleBreak_OK == undefined) yRuleBreak_OK = "";

  var pt_ref = " (ref: *PT*)";
  var cp_ref = " (ref: CP text)";

  var checks_complete_debug = false; //temporary logic...will be deleted after code is stable. TRUE is for use by zAdm only, as needed
  var rule_debug = "DEBUG";

  var rule_grp = "Missing info"; //default group
  var rule_2verify = "Please verify";
  var rule_verified = "Previously verified";
  var rule_error = "Error";

  //-------------------Check for artifacts to skip processing--------------------
  // if (name.indexOf("PLACEHOLDER") > -1 || primaryText.toUpperCase().indexOf("PLACEHOLDER") > -1) {
  //   logs.push(artAttrs);
  //   errorMessages.push([
  //     artAttrs.ref.uri,
  //     "Placeholder",
  //     "Rule Checker processing n/a",
  //     "If unused rqmt, please set state to Recycle and then remove from Collection or P-spec/SRS module, as applicable",
  //   ]);

  //   return;
  // }

  // if (
  //   name.indexOf("OBE") > -1 ||
  //   primaryText.toUpperCase().indexOf("DELETED IN FAVOR OF") > -1 ||
  //   primaryText.toUpperCase().indexOf("DELETED PER") > -1 ||
  //   primaryText.toUpperCase().indexOf("DELETED IAW") > -1 ||
  //   (changeProposal.toUpperCase().indexOf("SHALL") < 0 && changeProposal.toUpperCase().indexOf("DELETE") > -1)
  // ) {
  //   logs.push(artAttrs);
  //   errorMessages.push([
  //     artAttrs.ref.uri,
  //     "OBE artifact",
  //     "Rule Checker processing n/a",
  //     "Artifact scheduled for deletion after Peer Review",
  //   ]);

  //   return;
  // }

  //----------------------------------------------  Begin SRS Checks  ---------------------------------------------------------------------

  //_seSRS_Rqmt
  if (type.name == "_seSRS_Rqmt") {
    //_seSRS_Rqmt_name
    if (_seSRS_Rqmt_name == true) {
      if (name.length == 0 || name.indexOf("PLACEHOLDER") > -1) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS Name missing"]);
      }
    }

    // _seSRS_Rqmt___test_Method
    if (_seSRS_Rqmt___test_Method == true) {
      if (__test_Method.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS test_Method missing"]);
      }
    }

    //_seSRS_Rqmt___Vers_Initial
    if (_seSRS_Rqmt___Vers_Initial == true) {
      var __Vers_Initial = artAttrs.values["__Vers_Initial"];
      if (__Vers_Initial == null || __Vers_Initial == undefined) __Vers_Initial = "";
      if (__Vers_Initial.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS Vers_Initial missing"]);
      }
    }

    //_seSRS_Rqmt___Vers_Rev
    if (_seSRS_Rqmt___Vers_Rev == true) {
      var __Vers_Rev = artAttrs.values["__Vers_Rev"];
      if (__Vers_Rev == null || __Vers_Rev == undefined) __Vers_Rev = "";
      if (__Vers_Rev.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS Vers_Rev missing"]);
      }
    }

    //_seSRS_Rqmt___Vers_RevSub
    if (_seSRS_Rqmt___Vers_RevSub == true) {
      var __Vers_Rev = artAttrs.values["__Vers_Rev"];
      if (__Vers_Rev == null || __Vers_Rev == undefined) __Vers_Rev = "";

      var __Vers_RevSub = artAttrs.values["__Vers_RevSub"];
      if (__Vers_RevSub == null || __Vers_RevSub == undefined) __Vers_RevSub = "";

      if (__Vers_Rev == "V5.5") {
        if (__Vers_RevSub.toUpperCase().indexOf("LEGACY") < 0) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS V5.5 Vers_RevSub Legacy setting missing"]);
        }
      } else {
        if (__Vers_RevSub.length == 0) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS V5.6 Vers_RevSub missing"]);
        } else if (__Vers_RevSub.length != 1) {
          logs.push(artAttrs);
          errorMessages.push([
            artAttrs.ref.uri,
            rule_error,
            rule_grp,
            "SRS V5.6 Vers_RevSub should = single character",
          ]);
        }
      }
    }

    //_seSRS_Rqmt__seAssignedPOC
    if (_seSRS_Rqmt__seAssignedPOC == true) {
      var _seAssignedPOC = artAttrs.values["_seAssignedPOC"];
      if (_seAssignedPOC == null || _seAssignedPOC == undefined) _seAssignedPOC = "";
      if (_seAssignedPOC.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS AssignedPOC missing"]);
      }
    }

    //_seSRS_Rqmt__seGrp_Mission
    if (_seSRS_Rqmt__seGrp_Mission == true) {
      var _seGrp_Mission = artAttrs.values["_seGrp_Mission"];
      if (_seGrp_Mission == null || _seGrp_Mission == undefined) _seGrp_Mission = [];
      if (_seGrp_Mission.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS Grp_Mission missing"]);
      }
    }

    //_seSRS_Rqmt__seGrp_Tags
    if (_seSRS_Rqmt__seGrp_Tags == true) {
      var _seGrp_Tags = artAttrs.values["_seGrp_Tags"];
      if (_seGrp_Tags == null || _seGrp_Tags == undefined) _seGrp_Tags = [];
      if (_seGrp_Tags.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS Grp_Tags missing"]);
      }
    }

    // _seSRS_Rqmt__SRS_CSC
    if (_seSRS_Rqmt__SRS_CSC == true) {
      ptUC_clean = pt_clean.toUpperCase();

      cpUC = changeProposal.toUpperCase();

      var txtUC = "shall".toUpperCase();
      var verify_type = "";
      var rule_no = "ar09"; //unique ID for validation rule

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      //Logic here primarily applies to CSC with TM. Rqmt text is only checked for exclusions
      //To avoid duplicate error msg: Only need to check once, so check CP first. If no CP-shall, then test PT

      if (_SRS_CSC.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS CSC missing"]);
      } else {
        var rule_grp_CSC = "CSC/TM mismatch";
        if (cpUC.indexOf(txtUC) > -1) {
          var cp_out = checkCSC_rules("SRS", cp_ref, cpUC, _SRS_CSC, __test_Method);
          if (cp_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp_CSC, cp_out]);
          }
        } else if (ptUC_clean.indexOf(txtUC) > -1) {
          var pt_out = checkCSC_rules("SRS", pt_ref, ptUC_clean, _SRS_CSC, __test_Method);
          if (pt_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp_CSC, pt_out]);
          }
        } // end check for CP or PT shall
      } // end missing CSC
    } // end CSC Mismatch

    //_seSRS_Rqmt__SRS_CSCSub
    if (_seSRS_Rqmt__SRS_CSCSub == true) {
      var _SRS_CSCSub = artAttrs.values["_seSRS_CSCSub"];
      if (_SRS_CSCSub == null || _SRS_CSCSub == undefined) _SRS_CSCSub = "";

      if ((_SRS_CSC == "BM" || _SRS_CSC == "TM") && _SRS_CSCSub.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS CSCSub missing for CSC=" + _SRS_CSC]);
      } else if (_SRS_CSC != "BM" && _SRS_CSC != "TM" && _SRS_CSCSub.length != 0) {
        var rule_detail = "SRS has bad _CSCSub = " + _SRS_CSCSub + " for CSC = " + _SRS_CSC;
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, "CSCsub issue", rule_detail]);
      }
    }

    //_seSRS_Rqmt__test_PassCriteria
    if (_seSRS_Rqmt__test_PassCriteria == true) {
      var _test_PassCriteria = artAttrs.values["_test_PassCriteria"];
      if (_test_PassCriteria == null || _test_PassCriteria == undefined) _test_PassCriteria = "";
      if (_test_PassCriteria.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS test_PassCriteria missing"]);
      }
    }

    //_seSRS_Rqmt__xLnk_Pspec
    if (_seSRS_Rqmt__xLnk_Pspec == true) {
      var _xLnk_Pspec = artAttrs.values["_xLnk_Pspec"];
      if (_xLnk_Pspec == null || _xLnk_Pspec == undefined) _xLnk_Pspec = "";
      if (_xLnk_Pspec.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS xLnk_Pspec missing"]);
      } else {
        _xLnk_Pspec = _xLnk_Pspec.trim();
        var table = _xLnk_Pspec.split(",");
        var flag = false;
        for (var a = 0; a < table.length; a++) {
          var val = table[a];
          if (!isNumeric(val)) {
            flag = true;
          }
        }

        if (flag) {
          logs.push(artAttrs);
          errorMessages.push([
            artAttrs.ref.uri,
            rule_error,
            rule_grp,
            "SRS xLnk_Pspec - value conflicts with comma separated integers rule",
          ]);
        }
      }
    }

    //_seSRS_Rqmt__xTr_SPR_SCR
    if (_seSRS_Rqmt__xTr_SPR_SCR == true) {
      var _xTr_SPR_SCR = artAttrs.values["_xTr_SPR_SCR"];
      if (_xTr_SPR_SCR == null || _xTr_SPR_SCR == undefined) _xTr_SPR_SCR = "";
      if (_xTr_SPR_SCR.length == 0 && name.indexOf("PLACEHOLDER") < 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS SPR_SCR trace missing"]);
      }
    }

    if (_seSRS_Rqmt__xLnk_PspecLink == true) {
      var _xLnk_Pspec = artAttrs.values["_xLnk_Pspec"];
      if (_xLnk_Pspec == null || _xLnk_Pspec == undefined) _xLnk_Pspec = "";

      var linkSet = new Set();
      var attributeSet = new Set();

      var _xLnk_PspecArray = _xLnk_Pspec.split(",");

      for (let item of _xLnk_PspecArray) {
        var element = parseInt(item.trim());
        if (!isNaN(element)) {
          attributeSet.add(element);
        }
      }

      var result = await getLinksForArtifact(artAttrs.ref);
      if (result.code == RM.OperationResult.OPERATION_OK) {
        var artLinks = result.data.artifactLinks;
        for (linkType of artLinks) {
          if (
            linkType.linktype.uri == "http://faad.ngc.com/rm/linktypes/SRStoPspec" &&
            linkType.linktype.direction == "_SUB"
          ) {
            for (target of linkType.targets) {
              var resultAttr = await getAttributesForArtifact(target);
              if (resultAttr.code == RM.OperationResult.OPERATION_OK) {
                if (resultAttr.data.length > 0) {
                  var identifier = resultAttr.data[0].values[RM.Data.Attributes.IDENTIFIER];
                  linkSet.add(identifier);
                }
              }
            }
          }
        }

        if (!setsAreEqual(linkSet, attributeSet)) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "SRS to Pspec Mismatch"]);
        }
      }
    }

    //_seSRS_Rqmt_PT_system (use pt to check mixed-case strings)
    if (_seSRS_Rqmt_PT_system == true) {
      var txt = "shall";
      var systems = new String(artAttrs.values["__Subsystems"]);
      var test_LOR = new String(artAttrs.values["__test_SafeLOR"]);
      var rule_grp_subsys = "Subsystem mismatch";

      //Testing for "shall" applies to both Primary Text and Change_Proposal (indicates change in progress)
      if (changeProposal.indexOf(txt) < 0 && pt_clean.indexOf(txt) < 0) {
        //at least one must have shall
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp_subsys,
          "SRS word shall is missing (ref: PT and CP)",
        ]);
      } else {
        if (changeProposal.indexOf(txt) > -1) {
          var cp_out = checksubsys_rules("SRS", cp_ref, changeProposal, systems, test_LOR, name);

          if (cp_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_subsys, cp_out]);
          }
        } else {
          // since not a shall, check for comment needing resolution...

          var cp_trim = changeProposal.trim();
          if (cp_trim == "null" || cp_trim == null || cp_trim == undefined) cp_trim = "";

          if (cp_trim.length > 1) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              "Open comment",
              "SRS - please resolve Change Proposal comment",
            ]);
          }
        } //end check for chng proposal with shall

        // Primary Text
        //--------------
        if (pt_clean.indexOf(txt) > -1) {
          var pt_out = checksubsys_rules("SRS", pt_ref, pt_clean, systems, test_LOR, name);

          if (pt_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_subsys, pt_out]);
          }
        } //...is this a new rqmt (No shall in PT, but word "New" exists)
        else if (pt_clean.toUpperCase().indexOf("NEW") > -1) {
          if (checks_complete_debug == true) {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_subsys, "detects NEW SRS..."]);
          }
        } //end check for PT with/without shall
      } //end no shalls check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_subsys, "SRS checks complete..."]);
      }
    } //end check subsystems type

    //_seSRS_Rqmt_PT_bullets
    if (_seSRS_Rqmt_PT_bullets == true) {
      var cp_gt = changeProposal;
      cp_gt = cp_gt.replace(/>/g, "&gt;"); //Replace with gt to use same logic as PT in function checkbullet
      cp_gt = cp_gt.replace(/</g, "&lt;");

      var txt = "shall";

      //Check changeProposal (only process for shalls; other uses of > are ok)
      //---------------------------------------------------------------------------------
      if (cp_gt.indexOf(txt) > -1) {
        checkbullet_rules("SRS", cp_ref, cp_gt, artAttrs);
      } // end CP shall check

      // Check Primary Text
      // --------------------------------------------------------------------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkbullet_rules("SRS", pt_ref, pt_clean, artAttrs);
      } // end check for PT shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Bullet format", "SRS checks complete..."]);
      }
    } //end check bullets option selected

    //_seSRS_Rqmt_PT_x_where_lowercase
    if (_seSRS_Rqmt_PT_x_where_lowercase == true) {
      var cp_gt = changeProposal;
      cp_gt = cp_gt.replace(/>/g, "&gt;"); //Replace with gt to use same logic as PT in function checkbullets
      cp_gt = cp_gt.replace(/</g, "&lt;");

      var txt = "shall";
      var regex = /&lt;[a-z]{1}?&gt;/gi;

      // Check ChangeProposal
      // --------------------
      if (cp_gt.indexOf(txt) > -1 && cp_gt.match(regex) != null) {
        checkEnumList_rules("SRS", cp_ref, cp_gt, artAttrs);
      } // end shall and regex check

      // Check PrimaryText
      // ------------------
      if (pt_clean.indexOf(txt) > -1 && pt_clean.match(regex) != null) {
        checkEnumList_rules("SRS", pt_ref, pt_clean, artAttrs);
      } // end shall and regex check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Enum list", "Pspec checks complete..."]);
      }
    } //end check enumerated option

    //  _seSRS_Rqmt_PT_MMI_Mismatch  (check PT and Change Proposal)
    if (_seSRS_Rqmt_PT_MMI_Mismatch == true) {
      ptUC_clean = pt_clean.toUpperCase();
      cpUC = changeProposal.toUpperCase();

      var txtUC = "shall".toUpperCase();

      var rule_grp_MMI = "MMI mismatch";
      var verify_type = "";
      var rule_no = "ptR04";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall check occurs with Subsystem logic, so missing shall message is not needed here
      // if (cpUC.indexOf(txtUC)<0 && ptUC_clean.indexOf(txtUC)<0)

      // Change Proposal
      // --------------------
      if (cpUC.indexOf(txtUC) > -1) {
        var cp_out = checkMMImismatch_rules("SRS", cp_ref, cpUC, _SRS_CSC, name);

        if (cp_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp_MMI, cp_out]);
        }
      } // end CP has shall

      // Primary Text
      // --------------------
      if (ptUC_clean.indexOf(txtUC) > -1) {
        var pt_out = checkMMImismatch_rules("SRS", pt_ref, ptUC_clean, _SRS_CSC, name);

        if (pt_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp_MMI, pt_out]);
        }
      } // end PT has shall check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_MMI, "SRS checks complete..."]);
      }
    } //end MMI Mismatch option

    //_seSRS_Rqmt_PT_COMM_Mismatch (check PT and Change Proposal)
    if (_seSRS_Rqmt_PT_COMM_Mismatch == true) {
      ptUC_clean = pt_clean.toUpperCase();
      cpUC = changeProposal.toUpperCase();

      var txtUC = "shall".toUpperCase();

      var rule_grp_COMM = "COMM mismatch";
      var verify_type = "";
      var rule_no = "ptR05";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall check occurs with Subsystem logic, so missing shall message is not needed here
      // if (cpUC.indexOf(txtUC)<0 && ptUC_clean.indexOf(txtUC)<0)

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "WE ARE HERE", _SRS_CSC]);
      }

      if (cpUC.indexOf(txtUC) > -1) {
        var cp_out = checkCOMMmismatch_SRS("SRS", cp_ref, cpUC, _SRS_CSC);
        if (cp_out != "") {
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_COMM, cp_out]); //classified as error
        }
      } //end CP has shall

      if (ptUC_clean.indexOf(txtUC) > -1) {
        var pt_out = checkCOMMmismatch_SRS("SRS", pt_ref, ptUC_clean, _SRS_CSC);
        if (pt_out != "") {
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_COMM, pt_out]); //classified as error
        }
      } // end PT has shall check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_COMM, "SRS checks complete..."]);
      }
    } //end check COMM Mismatch

    // _seSRS_Rqmt_PT_COMMmessage
    if (_seSRS_Rqmt_PT_COMMmessage == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR06";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall check occurs with Subsystem logic, so additional shall output msg is not needed here
      // if (changeProposal.toUpperCase().indexOf(txt)<0 && pt_clean.toUpperCase().indexOf(txt)<0)

      if (changeProposal.indexOf(txt) > -1) {
        checkCOMMmsg_SRS("SRS", cp_ref, changeProposal, _SRS_CSC, __test_Method, artAttrs, verify_type);
      } // end check CP has a shall

      // Primary Text
      // ----------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkCOMMmsg_SRS("SRS", pt_ref, pt_clean, _SRS_CSC, __test_Method, artAttrs, verify_type);
      } // end check PT has a shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "COMM msg", "SRS checks complete..."]);
      }
    } //end check COMM Message

    //     _seSRS_Rqmt_PT_COMMshallSend
    if (_seSRS_Rqmt_PT_COMMshallSend == true) {
      ptUC_clean = pt_clean.toUpperCase();
      cpUC = changeProposal.toUpperCase();

      var txtUC = "shall".toUpperCase();
      var rule_grp_send = "COMM shall send";

      var rule_no = "not supported"; //code here for consistency, but Break Rule option is currently UNSUPPORTED in DOORS
      var verify_type = "";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test included with Subsystem check; no need to duplicate error message here
      // if (changeProposal.indexOf(txt)<0 && primaryText.indexOf(txt)<0)

      // Change Proposal
      // -------------------------
      if (cpUC.indexOf(txtUC) > -1) {
        var cp_out = checkCOMMsend_rules("SRS", cp_ref, cpUC, _SRS_CSC);
        if (cp_out != "") {
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_send, cp_out]); //classified as error
        }
      } //end CP has shall

      // Primary Text
      // -------------------------------------
      if (ptUC_clean.indexOf(txtUC) > -1) {
        var pt_out = checkCOMMsend_rules("SRS", pt_ref, ptUC_clean, _SRS_CSC);
        if (pt_out != "") {
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_send, pt_out]); //classified as error
        }
      } //end PT has shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_send, "SRS checks complete..."]);
      }
    } //end check COMM Shall Send option selected

    //_seSRS_Rqmt_PT_wordsToAvoid
    if (_seSRS_Rqmt_PT_wordsToAvoid == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR08";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test occurs with Subsystem check; no need to duplicate error msg here
      //if (changeProposal.indexOf(txt)<0 && pt_clean.indexOf(txt)<0)

      // Check Change Proposal
      // ---------------------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkword_rules("SRS", cp_ref, changeProposal, artAttrs, verify_type);
      } //end CP has shall

      // Check Primary Text
      // ----------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkword_rules("SRS", pt_ref, pt_clean, artAttrs, verify_type);
      } //end PT has shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule, "Words to avoid", "SRS checks complete..."]);
      }
    } //end check Words to Avoid checks selected

    //_seSRS_Rqmt_PT_wordStyleChecks
    if (_seSRS_Rqmt_PT_wordStyleChecks == true) {
      var txt = "shall";

      var verify_type = "";
      var rule_no = "ptR09"; //Code here for consistency; break rule is unsupported in DOORS for this rule

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test occurs with Subsystem check; no need to duplicate error msg here
      //if (changeProposal.indexOf(txt)<0 && pt_clean.indexOf(txt)<0)

      // Check Change Proposal
      // ---------------------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkwordStyle_rules("SRS", cp_ref, changeProposal, artAttrs);
      } //end CP has shall

      // Check Primary Text
      // ----------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkwordStyle_rules("SRS", pt_ref, pt_clean, artAttrs);
      } //end PT has shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Writing Style", "SRS checks complete..."]);
      }
    } //end Word Style Checks

    //_seSRS_Rqmt_PT_textWithoutPeriod
    if (_seSRS_Rqmt_PT_textWithoutPeriod == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR10";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      if (changeProposal.indexOf(txt) > -1) {
        var cp_out = checkEndPeriod_rules("SRS", cp_ref, changeProposal);
        if (cp_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, cp_out]); //classified as error
        }
      } //end CP has shall

      // Primary Text
      // -------------------------------------
      if (pt_clean.indexOf(txt) > -1) {
        var pt_out = checkEndPeriod_rules("SRS", pt_ref, pt_clean);
        if (pt_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, pt_out]); //classified as error
        }
      } //end PT has shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "End period", "SRS checks complete..."]);
      }
    } //end text without period check

    //  _seSRS_Rqmt_PT_unwantedChar;
    if (_seSRS_Rqmt_PT_unwantedChar == true) {
      var pt_noTrim = new String(artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT]);
      //        pt = pt.trim(); don't trim since checking for extra spaces
      pt_noTrim = pt_noTrim.replace(/(<\/p>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<br\/>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<br>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<\/li>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<\/ul>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<([^>]+)>)/gi, "");

      var txt = "shall";

      // Change Proposal; Note: Rule Break is not supported for these checks
      // ---------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkBadChar_rules("SRS", cp_ref, changeProposal, artAttrs);
      }

      // Primary Text
      // ---------------------------------
      if (pt_noTrim.indexOf(txt) > -1) {
        checkBadChar_rules("SRS", pt_ref, pt_noTrim, artAttrs);
      }

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Unwanted Chars", "SRS checks complete..."]);
      }
    } // end unwanted Char check
  } //end artifact SRS validation

  //----------------------------------------------  Begin P-spec Checks  ---------------------------------------------------------------------
  //_sePSPEC_Rqmt
  if (type.name == "_sePSPEC_Rqmt") {
    //_sePspec_Rqmt_name
    if (_sePspec_Rqmt_name == true) {
      if (name.length == 0 || name.indexOf("PLACEHOLDER") > -1) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec Name missing"]);
      }
    }

    //_sePspec_Rqmt___test_Method
    if (_sePspec_Rqmt___test_Method == true) {
      var __test_Method = artAttrs.values["__test_Method"];
      if (__test_Method == null || __test_Method == undefined) __test_Method = "";
      if (__test_Method.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec test_Method missing"]);
      }
    }

    //_sePspec_Rqmt___Vers_Initial
    if (_sePspec_Rqmt___Vers_Initial == true) {
      var __Vers_Initial = artAttrs.values["__Vers_Initial"];
      if (__Vers_Initial == null || __Vers_Initial == undefined) __Vers_Initial = "";
      if (__Vers_Initial.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec Vers_Initial missing"]);
      }
    }

    //_sePspec_Rqmt___Vers_Rev (Note: RevSub is n/a for Pspec)
    if (_sePspec_Rqmt___Vers_Rev == true) {
      var __Vers_Rev = artAttrs.values["__Vers_Rev"];
      if (__Vers_Rev == null || __Vers_Rev == undefined) __Vers_Rev = "";
      if (__Vers_Rev.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec Vers_Rev missing"]);
      }
    }

    //_sePspec_Rqmt__seGrp_Mission (Note: Grp_Tags is optional for Pspec)
    if (_sePspec_Rqmt__seGrp_Mission == true) {
      var _seGrp_Mission = artAttrs.values["_seGrp_Mission"];
      if (_seGrp_Mission == null || _seGrp_Mission == undefined) _seGrp_Mission = [];
      if (_seGrp_Mission.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec seGrp_Mission missing"]);
      }
    }

    //_sePspec_Rqmt__xTr_SPR_SCR
    if (_sePspec_Rqmt__xTr_SPR_SCR == true) {
      var _xTr_SPR_SCR = artAttrs.values["_xTr_SPR_SCR"];
      if (_xTr_SPR_SCR == null || _xTr_SPR_SCR == undefined) _xTr_SPR_SCR = "";
      if (_xTr_SPR_SCR.length == 0 && name.indexOf("PLACEHOLDER") < 0) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "Pspec SPR_SCR trace missing"]);
      }
    }
    //_sePspec_Rqmt__xTr_Rqmt_Src

    if (_sePspec_Rqmt__xTr_Rqmt_Src == true) {
      var _xTr_Rqmt_Src = artAttrs.values["_xTr_Rqmt_Src"];
      if (_xTr_Rqmt_Src == null || _xTr_Rqmt_Src == undefined) _xTr_Rqmt_Src = "";
      if (_xTr_Rqmt_Src.length == 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          "Pspec Rqmt_Src missing -- can be SCR/SPR for new capabilities",
        ]);
      }
    }

    //_sePspec_Rqmt_PT_system (use pt to check mixed-case strings)
    if (_sePspec_Rqmt_PT_system == true) {
      var changeProposal = new String(artAttrs.values["__Chng_Proposal"]);
      var txt = "shall";
      var systems = new String(artAttrs.values["__Subsystems"]);
      var rule_grp_subsys = "Subsystem mismatch";

      // LOR is applicable to SRS only
      // var test_LOR = new String(artAttrs.values["__test_SafeLOR"]);

      //Testing for "shall" applies to both Primary Text and Change_Proposal (indicates change in progress)
      if (changeProposal.indexOf(txt) < 0 && pt_clean.indexOf(txt) < 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp_subsys,
          "Pspec Subsystem check - word shall is missing (ref: PT and CP)",
        ]);
      } else {
        // Change Proposal
        //-----------------
        if (changeProposal.indexOf(txt) > -1) {
          var cp_out = checksubsys_rules("Pspec", cp_ref, changeProposal, systems, "", "");

          if (cp_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_subsys, cp_out]);
          }
        } else {
          // since not a shall, check for comment needing resolution...
          var cp_trim = changeProposal.trim();
          if (cp_trim == "null" || cp_trim == null || cp_trim == undefined) cp_trim = "";

          if (cp_trim.length > 1) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              "Open comment",
              "Pspec - please resolve Change Proposal comment",
            ]);
          }
        } //end check for chng proposal with shall

        // Primary Text
        //--------------
        if (pt_clean.indexOf(txt) > -1) {
          var pt_out = checksubsys_rules("Pspec", pt_ref, pt_clean, systems, "", "");

          if (pt_out != "") {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp_subsys, pt_out]);
          }
        } //...is this a new rqmt
        else if (pt_clean.toUpperCase().indexOf("NEW") > -1) {
          if (checks_complete_debug == true) {
            logs.push(artAttrs);
            errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_subsys, "detects NEW Pspec..." + pt_ref]);
          }
        } //end check for PT with/without shall
      } //end no PT shalls check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, rule_grp_subsys, "Pspec checks complete..."]);
      }
    } //end check subsystems type option selected

    //_sePspec_Rqmt_PT_bullets
    if (_sePspec_Rqmt_PT_bullets == true) {
      var cp_gt = changeProposal;
      cp_gt = cp_gt.replace(/>/g, "&gt;"); //Replace with gt to use same logic as PT in function checkbullets
      cp_gt = cp_gt.replace(/</g, "&lt;");

      var txt = "shall";

      //Check changeProposal (only process for shalls; other uses of > are ok)
      //---------------------------------------------------------------------------------
      if (cp_gt.indexOf(txt) > -1) {
        checkbullet_rules("Pspec", cp_ref, cp_gt, artAttrs);
      } // end CP shall check

      // Check Primary Text
      // ---------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkbullet_rules("Pspec", pt_ref, pt_clean, artAttrs);
      } // end check for PT shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Bullet format", "Pspec checks complete..."]);
      }
    } //end check bullets option selected

    //_sePspec_Rqmt_PT_x_where_lowercase (chk PT only since errors don't impact reviews)
    if (_sePspec_Rqmt_PT_x_where_lowercase == true) {
      var cp_gt = changeProposal;
      cp_gt = cp_gt.replace(/>/g, "&gt;"); //Replace with gt to use same logic as PT in function checkenum
      cp_gt = cp_gt.replace(/</g, "&lt;");

      var txt = "shall";
      var regex = /&lt;[a-z]{1}?&gt;/gi;

      //Change Proposal
      //--------------
      if (cp_gt.indexOf(txt) > -1 && cp_gt.match(regex) != null) {
        checkEnumList_rules("Pspec", cp_ref, cp_gt, artAttrs);
      } // end shall and regex check

      //Primary Text
      //--------------
      if (pt_clean.indexOf(txt) > -1 && pt_clean.match(regex) != null) {
        checkEnumList_rules("Pspec", pt_ref, pt_clean, artAttrs);
      } // end shall and regex check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Enum list", "Pspec checks complete..."]);
      }
    } //end check enumerated option

    //  _sePspec_Rqmt_PT_MMI_Mismatch  (Not applicable to Pspec)
    //    if (_sePspec_Rqmt_PT_MMI_Mismatch == true) {
    //    } //end check MMI Mismatch option selected

    //_sePspec_Rqmt_PT_COMM_Mismatch (Not applicable to Pspec)
    //    if (_sePspec_Rqmt_PT_COMM_Mismatch == true) {
    //    } //end check COMM Mismatch option selected

    // _sePspec_Rqmt_PT_COMMmessage (Logic differs from SRS since CSC test is only applicable to SRS)
    if (_sePspec_Rqmt_PT_COMMmessage == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR06";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      //Missing shall tested during subsystem test, so not needed here
      //if (changeProposal.toUpperCase().indexOf(txt)<0 && pt.toUpperCase().indexOf(txt)<0)

      // Change Proposal
      // --------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkCOMMmsg_Pspec("Pspec", cp_ref, changeProposal, __test_Method, artAttrs, verify_type);
      } // end CP has a shall check

      // Primary Text
      // --------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkCOMMmsg_Pspec("Pspec", pt_ref, pt_clean, __test_Method, artAttrs, verify_type);
      } // end PT has a shall check

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "COMM message", "Pspec message name checks complete..."]);
      }
    } //end check COMM Message option

    //_sePspec_Rqmt_PT_wordsToAvoid
    if (_sePspec_Rqmt_PT_wordsToAvoid == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR08";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test occurs with Subsystem check; no need to duplicate error msg here
      //if (changeProposal.indexOf(txt)<0 && pt_clean.indexOf(txt)<0)

      // Check Change Proposal
      // ---------------------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkword_rules("Pspec", cp_ref, changeProposal, artAttrs, verify_type);
      }

      // Check Primary Text
      // -------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkword_rules("Pspec", pt_ref, pt_clean, artAttrs, verify_type);
      }

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Words to avoid", "Pspec checks complete..."]);
      }
    } //end Words to Avoid checks

    //_sePspec_Rqmt_PT_wordStyleChecks
    if (_sePspec_Rqmt_PT_wordStyleChecks == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR09"; //Code here for consistency; break rule is unsupported in DOORS for this rule

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test occurs with Subsystem check; no need to duplicate error msg here
      //if (changeProposal.indexOf(txt)<0 && pt.indexOf(txt)<0)

      // Check Change Proposal
      // ---------------------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkwordStyle_rules("Pspec", cp_ref, changeProposal, artAttrs);
      } //end CP has shall

      // Check Primary Text
      // ----------------------
      if (pt_clean.indexOf(txt) > -1) {
        checkwordStyle_rules("Pspec", pt_ref, pt_clean, artAttrs);
      } //end PT has shall

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Writing Style", "Pspec checks complete..."]);
      }
    } //end Word Style Checks

    //_sePspec_Rqmt_pt_textWithoutPeriod
    if (_sePspec_Rqmt_PT_textWithoutPeriod == true) {
      var txt = "shall";
      var verify_type = "";
      var rule_no = "ptR10";

      if (yRuleBreak_OK.indexOf(rule_no) < 0) {
        verify_type = rule_2verify + " " + rule_no;
      } else {
        verify_type = rule_verified + " " + rule_no;
      }

      // Shall test occurs with Subsystem check; no need to duplicate error msg here
      //if (changeProposal.indexOf(txt)<0 && pt.indexOf(txt)<0)

      // Check Change Proposal
      // ---------------------------------
      if (changeProposal.indexOf(txt) > -1) {
        var cp_out = checkEndPeriod_rules("Pspec", cp_ref, changeProposal);
        if (cp_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, cp_out]);
        }
      }

      // Check Primary Text
      // ----------------------
      if (pt_clean.indexOf(txt) > -1) {
        var pt_out = checkEndPeriod_rules("Pspec", pt_ref, pt_clean);
        if (pt_out != "") {
          errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, pt_out]);
        }
      }

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Ending Period", "Pspec checks complete..."]);
      }
    } //end check text without period

    //  _sePspec_Rqmt_PT_unwantedChar;
    if (_sePspec_Rqmt_PT_unwantedChar == true) {
      var pt_noTrim = new String(artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT]);
      //        pt = pt.trim(); don't trim since checking for extra spaces
      pt_noTrim = pt_noTrim.replace(/(<\/p>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<br\/>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<br>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<\/li>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<\/ul>)/gi, "\n");
      pt_noTrim = pt_noTrim.replace(/(<([^>]+)>)/gi, "");

      var txt = "shall";

      // Change Proposal Note: Rule Break is not supported for these checks
      // --------------------
      if (changeProposal.indexOf(txt) > -1) {
        checkBadChar_rules("Pspec", cp_ref, changeProposal, artAttrs);
      }

      // Primary Text
      // ---------------------------------
      if (pt_noTrim.indexOf(txt) > -1) {
        checkBadChar_rules("Pspec", pt_ref, pt_noTrim, artAttrs);
      }

      if (checks_complete_debug == true) {
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_debug, "Unwanted Chars", "Pspec checks complete..."]);
      }
    } //end Unwanted Char check
  } //end artifact type Pspec check

  //_seParameter

  if (type.name == "_seParameter") {
    //_seParameter__Parameter_Value
    var _Parameter_Value = artAttrs.values["_Parameter_Value"];
    if (_Parameter_Value == null || _Parameter_Value == undefined) _Parameter_Value = "";
    var _Parameter_Unit = artAttrs.values["_Parameter_Unit/Type"];
    if (_Parameter_Unit == null || _Parameter_Unit == undefined) _Parameter_Unit = "";

    //_seParameter__Parameter_Value
    if (_seParameter__Parameter_Unit == true) {
      if (name.indexOf("PLACEHOLDER") < 0) {
        if (_Parameter_Unit.length < 1) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "_seParameter - Unit/Type missing"]);
        }
      }
    }
    //_seParameter__Parameter_Value
    if (_seParameter__Parameter_Value == true) {
      if (name.indexOf("PLACEHOLDER") < 0) {
        if (_Parameter_Value.length < 1) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "_seParameter - Value missing"]);
        }
      }
    }

    //_seParameter_PT_period
    if (_seParameter_PT_period == true) {
      if (name.indexOf("PLACEHOLDER") < 0) {
        // Change Proposal
        // -------------------------------------
        if (changeProposal.length > 3) {
          var cp_out = checkEndPeriod_rules("Parameter", cp_ref, changeProposal);
          if (cp_out != "") {
            errorMessages.push([artAttrs.ref.uri, rule_error, "Missing info", cp_out]); //classified as error
          }
        } //end CP check

        // Primary Text
        // -------------------------------------
        if (pt_clean.length > 3 && pt_clean.indexOf("New") < 0) {
          var pt_out = checkEndPeriod_rules("Parameter", pt_ref, pt_clean);
          if (pt_out != "") {
            errorMessages.push([artAttrs.ref.uri, rule_error, "Missing info", pt_out]); //classified as error
          }
        } //end name check
      }
    }

    //_seParameter_PT_notEmpty
    if (_seParameter_PT_notEmpty == true) {
      if (name.indexOf("PLACEHOLDER") < 0) {
        primaryText = primaryText.trim();
        primaryText = primaryText.replace(/(<\/p>)/gi, "\n");
        primaryText = primaryText.replace(/(<br\/>)/gi, "\n");
        primaryText = primaryText.replace(/(<br>)/gi, "\n");
        primaryText = primaryText.replace(/(<\/li>)/gi, "\n");
        primaryText = primaryText.replace(/(<\/ul>)/gi, "\n");
        primaryText = primaryText.replace(/(<([^>]+)>)/gi, "");
        primaryText = primaryText.trim();

        if (primaryText.length == 0 || primaryText.indexOf("PLACEHOLDER") > -1) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "seParameter Primary text description missing"]);
        }
      }
    }

    //_seParameter_parameterBySRS
    if (_seParameter_parameterBySRS == true) {
      if (name.indexOf("PLACEHOLDER") < 0) {
        RM.Data.getLinkedArtifacts(artAttrs.ref, ["Parameter by SRS"], function (response) {
          var artLinks = response.data.artifactLinks;
          if (artLinks.length == 0) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error + "-for resolution after Peer Review",
              rule_grp,
              "Parameter by SRS link missing",
            ]);
          }
        }); // end of function (response) call - used for link checks
      }
    }
  } // end parameter function
  if (type.name == "Term") {
    //term_PT_textOfTerm
    if (term_PT_textOfTerm == true) {
      primaryText = primaryText.trim();
      primaryText = primaryText.replace(/(<\/p>)/gi, "\n");
      primaryText = primaryText.replace(/(<br\/>)/gi, "\n");
      primaryText = primaryText.replace(/(<br>)/gi, "\n");
      primaryText = primaryText.replace(/(<\/li>)/gi, "\n");
      primaryText = primaryText.replace(/(<\/ul>)/gi, "\n");
      primaryText = primaryText.replace(/(<([^>]+)>)/gi, "");
      primaryText = primaryText.trim();
      primaryText = primaryText.toUpperCase();

      if (name.indexOf("PLACEHOLDER") < 0) {
        if (primaryText.length == 0 || primaryText.indexOf("PLACEHOLDER") > -1 || primaryText.indexOf("TODO") > -1) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, "TERM *Primary text* missing"]);
        }
      }
    }
  } // end artifact type term
} //end function

//----------------------------------------------------------------------------------
function checkCSC_rules(rqmt_type, text_ref, checkUC_str, CSC_str, tMeth_str) {
  // check_str passed as uppercase

  var error_out = "";

  if (CSC_str == "MMI" && tMeth_str != "D") {
    //Assume T is ok for the following combinations
    if (
      tMeth_str == "T" &&
      checkUC_str.indexOf("UPON RECEIPT") > -1 &&
      (checkUC_str.indexOf("SHALL DISPLAY") > -1 || checkUC_str.indexOf("SHALL PROVIDE AN ALARM") > -1)
    ) {
      return error_out;
    } else {
      error_out = rqmt_type + " CSC = " + CSC_str + " with TestMeth = " + tMeth_str + text_ref;
    }
  } else if (CSC_str != "MMI" && tMeth_str == "D") {
    if (
      checkUC_str.indexOf("SHALL STORE") < 0 &&
      checkUC_str.indexOf("SHALL ROUTE") < 0 &&
      checkUC_str.indexOf("yyyyyyy") < 0 &&
      CSC_str == "COMM" &&
      checkUC_str.indexOf("SHALL MONITOR") < 0 &&
      checkUC_str.indexOf("SHALL OVERWRITE") < 0 &&
      checkUC_str.indexOf("SHALL SUPPORT UP TO") < 0 &&
      CSC_str == "BM" &&
      checkUC_str.indexOf("TO COMPOSE") < 0 &&
      checkUC_str.indexOf("SHALL PURGE") < 0 &&
      checkUC_str.indexOf("SHALL ROUTE") < 0
    ) {
      error_out = rqmt_type + " CSC = " + CSC_str + " with TestMeth = " + tMeth_str + text_ref;
    }
  }

  return error_out;
} //end function

function checksubsys_rules(rqmt_type, text_ref, check_str, systems, sfty_LOR, sfty_name) {
  // check_str passed as lowercase

  var error_out = "";

  //***********************************************
  // Note: Order of checks is important
  //***********************************************

  if (
    check_str.indexOf("C2 System participants") > -1 ||
    check_str.indexOf("C2 System shall interface") > -1 ||
    check_str.indexOf("C2 System shall support") > -1 ||
    check_str.indexOf("C2 System shall exchange") > -1 ||
    check_str.indexOf("C2 System shall forward") > -1
  ) {
    if (systems != "TLabelP") {
      error_out = rqmt_type + " Subsystem match should = TLabelP" + text_ref;
    }
  } else if (
    check_str.indexOf("C2 System shall be") > -1 ||
    check_str.indexOf("C2 System shall provide") > -1 ||
    check_str.indexOf("C2 System shall operate") > -1 ||
    (check_str.indexOf("C2 System") > -1 && check_str.indexOf("ABT sensors") > -1) ||
    (check_str.indexOf("C2 System") > -1 && check_str.indexOf("Latching") > -1) ||
    check_str.indexOf("C2 Systems") > -1 ||
    check_str.indexOf("C2 Subsystem") > -1
  ) {
    //Subsystem detects singular & plural

    if (systems != "S-LabelS,H-LabelH" && systems != "H-LabelH,S-LabelS") {
      //Double test due to inconsistent value order from DB

      error_out = rqmt_type + " Subsystem match should = S-LabelS,H-LabelH" + text_ref;
    }
  } else if (check_str.indexOf("C2 Sheltered Subsystems and Handheld Subsystems with Ux") > -1) {
    if (systems != "S-LabelS,HLabelW" && systems != "HLabelW,S-LabelS") {
      error_out = rqmt_type + " Subsystem match should = S-LabelS,HLabelW" + text_ref;
    }
  } else if (check_str.indexOf("C2 Sheltered Subsystem") > -1) {
    //Subsys detects singular & plural

    //Singular and plural ( with trailing "s") return the same result, so only need to test singular
    if (systems != "S-LabelS") {
      error_out = rqmt_type + " Subsystem match should = S-LabelS" + text_ref;
    }
  } else if (check_str.indexOf("C2 Sheltered and Handheld Command Post Subsystem") > -1) {
    if (systems != "S-LabelS,HLabelP" && systems != "HLabelP,S-LabelS") {
      error_out = rqmt_type + " Subsystem match should = S-LabelS,HLabelP" + text_ref;
    }
  } else if (check_str.indexOf("C2 Track Producing Subsystems with Ax and Handheld Subsystems with Bx") > -1) {
    if (systems != "TLabelW,HLabelW" && systems != "HLabelW,TLabelW") {
      error_out = rqmt_type + " Subsystem match should = TLabelW,HLabelW" + text_ref;
    }
  } else if (check_str.indexOf("C2 Track Producing Subsystems with Ax") > -1) {
    if (systems != "TLabelW") {
      error_out = rqmt_type + " Subsystem match should = TLabelW" + text_ref;
    }
  } else if (
    check_str.indexOf("C2 Track Producing or Handheld Subsystem") > -1 ||
    check_str.indexOf("C2 Track Producing and Handheld Subsystem") > -1
  ) {
    //The or case only applies to 3914
    if (systems != "H-LabelH,TLabelP" && systems != "TLabelP,H-LabelH") {
      error_out = rqmt_type + " Subsystem match should = TLabelP,H-LabelH" + text_ref;
    }
  } else if (
    check_str.indexOf("C2 Track Producing Subsystems and Handheld Subsystems with Cx") > -1 ||
    check_str.indexOf("C2 Track Producing Subsystems and Handheld Subsystems with Dx") > -1
  ) {
    if (systems != "TLabelP,HLabelW" && systems != "HLabelW,TLabelP") {
      error_out = rqmt_type + " Subsystem match should = TLabelP,HLabelW" + text_ref;
    }
  } else if (check_str.indexOf("C2 Track Producing Subsystem") > -1) {
    if (systems != "TLabelP") {
      error_out = rqmt_type + " Subsystem match should = TLabelP" + text_ref;
    }
  } else if (check_str.indexOf("C2 Non-Track Producing Sheltered and Handheld Command Post Subsystems") > -1) {
    if (systems != "NLabelP,HLabelP" && systems != "HLabelP,NLabelP") {
      error_out = rqmt_type + " Subsystem match should = NLabelP,HLabelP" + text_ref;
    }
  } else if (check_str.indexOf("C2 Non-Track Producing Sheltered and Handheld Subsystems") > -1) {
    if (systems != "NLabelP,H-LabelH" && systems != "H-LabelH,NLabelP") {
      error_out = rqmt_type + " Subsystem match should = NLabelP,HLabelP" + text_ref;
    }
  } else if (check_str.indexOf("C2 Non-Track Producing Sheltered Subsystem") > -1) {
    if (systems != "NLabelP") {
      error_out = rqmt_type + " Subsystem match should = NLabelP" + text_ref;
    }
  } else if (check_str.indexOf("C2 Handheld Command Post Subsystem") > -1) {
    if (systems != "HLabelP") {
      error_out = rqmt_type + " Subsystem match should = HLabelP" + text_ref;
    }
  } else if (
    check_str.indexOf("C2 Handheld Subsystems with Cx") > -1 ||
    check_str.indexOf("C2 Handheld Subsystem with Cx") > -1 ||
    check_str.indexOf("C2 Handheld Subsystem with Dx") > -1 ||
    check_str.indexOf("C2 Handheld Subsystems with Dx") > -1
  ) {
    if (systems != "HLabelW") {
      error_out = rqmt_type + " Subsystem match should = HLabelW" + text_ref;
    }
  } else if (check_str.indexOf("C2 Handheld Subsystem") > -1) {
    if (systems != "H-LabelH") {
      error_out = rqmt_type + " Subsystem match should = H-LabelH" + text_ref;
    }
  } else if (check_str.indexOf("C2 non-Zip2 Handheld Subsystems") > -1) {
    if (systems != "HLabelX") {
      error_out = rqmt_type + " Subsystem match should = HLabelX" + text_ref;
    }
  } else if (rqmt_type == "SRS" && check_str.indexOf("System C2") > -1 && check_str.indexOf("Subsystem") < 0) {
    // Special logic for LOR
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------
    if (sfty_LOR == null || sfty_LOR == undefined || sfty_LOR < 1) {
      if (sfty_name.toUpperCase().indexOf("SFTY") < 0) {
        error_out =
          rqmt_type +
          " Subsystem text to Sfty mismatch - Fix missing LOR and add Sfty to DOORS Name; or add subsystem detail to the text" +
          text_ref;
      } else {
        error_out =
          rqmt_type +
          " Subsystem text to Sfty mismatch - Fix missing LOR or add subsystem detail to the text" +
          text_ref;
      }
    } else if (sfty_name.toUpperCase().indexOf("SFTY") < 0) {
      error_out = rqmt_type + " Subsystem text to Sfty mismatch - add Sfty to DOORS Name" + text_ref;
    }
  } else {
    error_out = rqmt_type + " Subsystem String, Bad or missing" + text_ref;
  } // end check subsystem strings

  return error_out;
} //end function

function checkbullet_rules(rqmt_type, text_ref, check_str, artAttrs) {
  //check_str passed as lowercase
  //ASSUMPTION: Nested bullets are always at the bottom of the list

  var lev1_bullet_check = check_str;
  var nest_bullets = false;
  var rule_error = "Error";
  var rule_grp = "Bullet issues";
  var regex = /&lt;[a-z]{1}?&gt;/gi;

  //  if (check_str.indexOf(regex)>-1)
  if (check_str.match(regex) != null) {
    return; //this is an enumerated list
  } else {
    if (check_str.indexOf(":") > -1 && check_str.indexOf("&gt;&gt;&gt;") < 0) {
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type +
          " colon(:) found without minimum bullets or enumerated list; consider replacing colon with dash" +
          text_ref,
      ]);

      return; //no bullets
    }
  }

  // Check for level 2 (nested) bullets, which must be at the end of the list
  if (check_str.indexOf("&gt;&gt;&gt;&gt;&gt;&gt;") > -1) {
    var lev2_ar = check_str.split("&gt;&gt;&gt;&gt;&gt;&gt;");

    if (lev2_ar.length < 3) {
      //min of 2 nested bullets must have min 3 split segments
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " if nesting, then min of two (2) nested bullets needed" + text_ref,
      ]);
    } else {
      nest_bullets = true;
      lev1_bullet_check = String(lev2_ar[0]); //lev2 nested bullets will be tested separately
    }

    if (lev1_bullet_check.indexOf("&gt;&gt;&gt;") > -1) {
      if (lev1_bullet_check.split("&gt;&gt;&gt;").length < 2) {
        // level 1 bullets must have at least 2 segments if nested bullets exist (normally, should have 3 segments)
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " min of one (1) level1 bullet needed for nested bullets" + text_ref,
        ]);
      }
    }
  } else if (lev1_bullet_check.indexOf("&gt;&gt;&gt;") > -1) {
    if (lev1_bullet_check.split("&gt;&gt;&gt;").length < 3) {
      // 2 bullets lead to min 3 segments
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " min of two (2) level1 bullets needed" + text_ref,
      ]);
    }
  } else {
    //Text contains a "shall" and angle brackets
    if (
      (check_str.indexOf("&gt;") > -1 && check_str.indexOf("&lt;") < 0) ||
      (check_str.indexOf("&gt;") < 0 && check_str.indexOf("&lt;") > -1)
    ) {
      //won't complain if < and > found used for <Future> or <initials>
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " stray angle bracket found; spell-out greater/less than, if applicable" + text_ref,
      ]);
    }
  } //end bullet segments check

  // Level1 bullet checks
  // --------------------------------------
  var pt = lev1_bullet_check.split("&gt;&gt;&gt;"); //deliberately left pt and pr names unchanged
  if (pt.length > 1) {
    for (var i = 0; i < pt.length; i++) {
      var pr = pt[i];

      if (i == 0) {
        // Check bullet lead-in
        if (pr.length == 0) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rqmt_type + " lead-in text missing" + text_ref]);
          break;
        } else {
          //LOCKE ... TO BE TESTED.....HOW DO I MAKE THIS HAPPEN?
          if (pr.charAt(pr.length - 1) != "\n") {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              "rule_debug",
              rule_grp,
              "lead-in invalid char is...@" + pr.charAt(pr.length - 1) + "@",
            ]);

            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type +
                " missing new line or has invalid char between lead-in and first bullet. Lead-in must end with colon (:) followed by shift-enter, and then bullet symbol" +
                text_ref,
            ]);
            break;
          } else {
            //clean white space before checking for colon
            pr = pr.replace("&nbsp;", "");
            pr = pr.trim();
            if (pr.length == 0) {
              logs.push(artAttrs); //does length = 0 after removal of stray whitespace
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type + " lead-in text missing" + text_ref,
              ]);
              break;
            } else {
              if (pr.charAt(pr.length - 1) != ":") {
                //colon position might have shifted with whitespace removal
                logs.push(artAttrs);
                errorMessages.push([
                  artAttrs.ref.uri,
                  rule_error,
                  rule_grp,
                  rqmt_type +
                    " colon (:) missing at end of lead-in text. Lead-in must end with colon (:) followed by shift-enter, and then bullet symbol" +
                    text_ref,
                ]);
                break;
              }
            } //end pr.length==0
          } //pr.charAt
        } //end pr length == 0
      } //end if i==0

      if (i != 0 && i < pt.length) {
        //if level2 bullets exist, then check all level1 bullets
        if (pr.length == 0) {
          logs.push(artAttrs);
          errorMessages.push([
            artAttrs.ref.uri,
            rule_error,
            rule_grp,
            rqmt_type + " Bullet #" + i + " description is missing" + text_ref,
          ]);
          break;
        } else {
          pr = pr.trim();

          if (
            pr.charAt(pr.length - 1) == "," ||
            pr.charAt(pr.length - 1) == ";" ||
            pr.charAt(pr.length - 1) == "." ||
            pr.charAt(pr.length - 1) == "-"
          ) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " Bullet #" + i + " remove punctuation at end of bullet" + text_ref,
            ]);
            break;
          }

          if (pr.indexOf("&gt;") > -1 || pr.indexOf("&lt;") > -1) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type +
                " Bullet #" +
                i +
                " stray angle bracket found; bullet must be in sets of 3 (level 1) or 6 (if nested); spell out greater than/less than, if applicable" +
                text_ref,
            ]);
            break;
          }

          if (i == pt.length - 1 && nest_bullets == true && pr.charAt(pr.length - 1) != ":") {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " Bullet #" + i + " colon (:) missing before nested bullets" + text_ref,
            ]);
            break;
          }
        } // end pr length check
      } // end != 0
    } //end for

    //Level2 bullets check
    //-------------------------------------------------
    if (nest_bullets == true) {
      for (var i = 1; i < lev2_ar.length; i++) {
        //skip [i=0] since lead-in and level1 bullets processed above
        var pr = lev2_ar[i];

        if (i < lev2_ar.length) {
          if (pr.length == 0) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " Nested Bullet #" + i + " descriptive text is missing" + text_ref,
            ]);
            break;
          } else {
            pr = pr.trim();
            if (
              pr.charAt(pr.length - 1) == "," ||
              pr.charAt(pr.length - 1) == ";" ||
              pr.charAt(pr.length - 1) == "." ||
              pr.charAt(pr.length - 1) == "-"
            ) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type + " Nested Bullet #" + i + " remove punctuation at end of bullet" + text_ref,
              ]);
              break;
            }

            if (pr.indexOf("&gt;") > -1 || pr.indexOf("&lt;") > -1) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type +
                  " Nested Bullet #" +
                  i +
                  " stray angle bracket found; spell-out greater/less than, if applicable. If another level 1 bullet, then move it ahead of nested bullets; nested bullets must be last" +
                  text_ref,
              ]);
            }
          } // end pr length check
        } // end i < pt length
      } //end for

      var last = lev2_ar[lev2_ar.length - 1];
      if (last.indexOf("@@@") < 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " missing @@@ on last line of text" + text_ref,
        ]);
      }
    } //no nested bullets
    else {
      var last = pt[pt.length - 1];
      if (last.indexOf("@@@") < 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " missing @@@ on last line of text" + text_ref,
        ]);
      }
    } //end nested bullet check
  } // end if (pt.length > 1)
} //end bullet function

function checkEnumList_rules(rqmt_type, text_ref, check_str, artAttrs) {
  //check_str passed as lowercase
  //ASSUMPTION: Nested bullets are always at the bottom of the list

  var lev1_enum_check = check_str;
  var nest_bullets = false;
  var rule_error = "Error";
  var rule_grp = "Enumerated list issues";
  var regex = /&lt;[a-z]{1}?&gt;/gi;

  if (check_str.match(regex) == null) {
    return; //this is not a valid enumerated list
  }

  // Check for level 2 bullets
  if (check_str.indexOf("&gt;&gt;&gt;&gt;&gt;&gt;") > -1) {
    var lev2_ar = check_str.split("&gt;&gt;&gt;&gt;&gt;&gt;");

    if (lev2_ar.length < 3) {
      //min of 2 nested bullets must have min 3 segments
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " if nesting, then min of two (2) nested bullets needed" + text_ref,
      ]);
    } else {
      nest_bullets = true;
      lev1_enum_check = String(lev2_ar[0]); //lev2 nested bullets will be tested separately
    }

    if (lev1_enum_check.match(regex) != null) {
      if (lev1_enum_check.split(regex).length < 3) {
        // level 1 enum list must have at least 3 segments if nested bullets exist
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " min of two (2) level1 list items (a,b) needed for nested bullets" + text_ref,
        ]);
      }
    }
  } else if (lev1_enum_check.match(regex) != null) {
    if (lev1_enum_check.split(regex).length < 3) {
      // 2 list items lead to min 3 segments
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " min of two (2) level1 bullets needed" + text_ref,
      ]);
    }
  } else {
    if (
      (check_str.indexOf("&gt;") > -1 && check_str.indexOf("&lt;") < 0) ||
      (check_str.indexOf("&gt;") < 0 && check_str.indexOf("&lt;") > -1)
    ) {
      //won't complain if < and > found used for <Future> or <initials>
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " stray angle bracket found; spell-out greater/less than, if applicable" + text_ref,
      ]);
    }
  } //end bullet segments check

  // Level1 enum checks
  // --------------------------------------
  var pt = lev1_enum_check.split(regex); //deliberately left pt and pr names unchanged
  if (pt.length > 1) {
    for (var i = 0; i < pt.length; i++) {
      var pr = pt[i];

      if (i == 0) {
        // Check enum lead-in
        if (pr.length == 0) {
          logs.push(artAttrs);
          errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rqmt_type + " lead-in text missing" + text_ref]);
          break;
        } else {
          if (pr.charAt(pr.length - 1) != "\n") {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              "rule_debug",
              rule_grp,
              "lead-in invalid char is...@" + pr.charAt(pr.length - 1) + "@",
            ]);

            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type +
                " missing new line or has invalid char between lead-in and first bullet. Lead-in must end with colon (:) followed by shift-enter, and then bullet symbol" +
                text_ref,
            ]);
            break;
          } else {
            //clean white space before checking for colon
            pr = pr.replace("&nbsp;", "");
            pr = pr.trim();
            if (pr.length == 0) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type + " lead-in text missing" + text_ref,
              ]);
              break;
            } else {
              if (pr.charAt(pr.length - 1) != ":") {
                logs.push(artAttrs);
                errorMessages.push([
                  artAttrs.ref.uri,
                  rule_error,
                  rule_grp,
                  rqmt_type + " colon (:) missing at end of lead-in string" + text_ref,
                ]);
                break;
              }
            } //end pr.length==0
          } //pr.charAt
        } // end if pr.length==0
      } //end if i==0

      if (i != 0 && i < pt.length) {
        //if level2 bullets exist, then check all level1 enums
        if (pr.length == 0) {
          logs.push(artAttrs);
          errorMessages.push([
            artAttrs.ref.uri,
            rule_error,
            rule_grp,
            rqmt_type + " enum item #" + i + " description is missing" + text_ref,
          ]);
          break;
        } else {
          pr = pr.trim();
          if (
            pr.charAt(pr.length - 1) == "," ||
            pr.charAt(pr.length - 1) == ";" ||
            pr.charAt(pr.length - 1) == "." ||
            pr.charAt(pr.length - 1) == "-"
          ) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " enum item #" + i + " remove punctuation at end of enum string" + text_ref,
            ]);
          }

          if (pr.indexOf("&gt;") > -1 || pr.indexOf("&lt;") > -1) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type +
                " enum #" +
                i +
                " stray angle bracket found; spell-out greater/less than, if applicable" +
                text_ref,
            ]);
            break;
          }

          if (i == pt.length - 1 && nest_bullets == true && pr.charAt(pr.length - 1) != ":") {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " enum #" + i + " colon (:) missing before nested bullets" + text_ref,
            ]);
            break;
          }
        } // end pr length check
      } // end if i!=0
    } // end for loop

    //Level2 bullets check
    //-------------------------------------------------
    if (nest_bullets == true) {
      for (var i = 1; i < lev2_ar.length; i++) {
        //skip [i=0] since lead-in and level1 enums processed above
        var pr = lev2_ar[i];

        if (i < lev2_ar.length) {
          //unlike above test (where last row=@@@), always check last row of level2 bullets????
          if (pr.length == 0) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              rule_error,
              rule_grp,
              rqmt_type + " Nested Bullet #" + i + " - bullet text is missing" + text_ref,
            ]);
            break;
          } else {
            pr = pr.trim();
            if (
              pr.charAt(pr.length - 1) == "," ||
              pr.charAt(pr.length - 1) == ";" ||
              pr.charAt(pr.length - 1) == "." ||
              pr.charAt(pr.length - 1) == "-"
            ) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type + " Nested Bullet #" + i + " - remove punctuation at end of bullet" + text_ref,
              ]);
              break;
            }

            if (pr.indexOf("&gt;") > -1 || pr.indexOf("&lt;") > -1) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                rule_error,
                rule_grp,
                rqmt_type +
                  " Nested Bullet #" +
                  i +
                  " stray angle bracket found; spell-out greater/less than, if applicable. If another enum item, then move it ahead of nested bullets; nested bullets must be last" +
                  text_ref,
              ]);
              break;
            }
          } // end pr length check
        } // end i < pt length
      } //end for

      var last = lev2_ar[lev2_ar.length - 1];
      if (last.indexOf("@@@") < 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " missing @@@ on last line of text" + text_ref,
        ]);
      }
    } //no nested bullets
    else {
      var last = pt[pt.length - 1];
      if (last.indexOf("@@@") < 0) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          rule_error,
          rule_grp,
          rqmt_type + " missing @@@ on last line of text" + text_ref,
        ]);
      }
    } // end nested bullet check
  }
} // end function enumerated list

function checkMMImismatch_rules(rqmt_type, text_ref, check_str, CSC_str, name_str) {
  //check_str passed as lowercase

  var error_out = "";

  if (
    name_str.toUpperCase().indexOf("MMI GENERICS") > -1 ||
    check_str.toUpperCase().indexOf("SHALL PROVIDE THE OPERATOR A MEANS") > -1 ||
    check_str.toUpperCase().indexOf("MEANS TO DISPLAY") > -1 ||
    check_str.toUpperCase().indexOf("MEANS TO SEND") > -1 ||
    check_str.toUpperCase().indexOf("MEANS TO DESIGNATE") > -1 ||
    check_str.toUpperCase().indexOf("SHALL DISPLAY") > -1 ||
    check_str.toUpperCase().indexOf("SHALL NOTIFY") > -1 ||
    check_str.toUpperCase().indexOf("BSD") > -1 ||
    check_str.toUpperCase().indexOf("SYMBOL") > -1 ||
    check_str.toUpperCase().indexOf("ALARM") > -1 ||
    check_str.toUpperCase().indexOf("HOOK") > -1 ||
    check_str.toUpperCase().indexOf("COLOR") > -1 ||
    check_str.toUpperCase().indexOf("zzzzzzzz") > -1 ||
    check_str.toUpperCase().indexOf("zzzzzzzz") > -1 ||
    check_str.toUpperCase().indexOf("zzzzzzzz") > -1 ||
    check_str.toUpperCase().indexOf("zzzzzzzz") > -1
  ) {
    if (CSC_str != "MMI" && CSC_str != "SIM") {
      //All SIM grouped together, whether MMI or not
      error_out = rqmt_type + " MMI text mismatch with CSC = " + CSC_str + text_ref;
    } //end CSC check
  } //end MMI text check

  return error_out;
} //end function checkMMImismatch

function checkCOMMmismatch_SRS(rqmt_type, text_ref, check_str, CSC_str) {
  //check_str passed as lowercase

  var error_out = "";

  if (
    check_str.toUpperCase().indexOf("SHALL TRANSMIT") > -1 ||
    check_str.toUpperCase().indexOf("SHALL RECEIVE") > -1 ||
    check_str.toUpperCase().indexOf("SHALL PROVIDE THE MEANS TO TRANSMIT") > -1 ||
    check_str.toUpperCase().indexOf("TRANSMISSION SEQUENCE") > -1 ||
    check_str.toUpperCase().indexOf("PERIODICALLY TRANSMIT") > -1 ||
    check_str.toUpperCase().indexOf("SHALL MONITOR DATA") > -1 ||
    check_str.toUpperCase().indexOf("future-zzzzzzzz") > -1 ||
    check_str.toUpperCase().indexOf("future-zzzzzzzz") > -1 ||
    check_str.toUpperCase().indexOf("future-zzzzzzzz") > -1
  ) {
    if (CSC_str != "COMM" && CSC_str != "SIM") {
      //All SIM are grouped together, whether COMM or not
      error_out = rqmt_type + " COMM text mismatch with CSC = " + CSC_str + text_ref;
    } //end CSC COMM check
  } // end COMM list check

  return error_out;
} // end function

function checkCOMMmsg_SRS(rqmt_type, text_ref, check_str, CSC_str, tMeth_str, artAttrs, verify_type) {
  //check_str passed as lowercase

  var rule_grp = "COMM message check";
  var rule_detail = "";

  //Note: All uses of the word "message" should be lowercase; this condition cannot be tagged as pre-Verified
  if (check_str.toUpperCase().indexOf("MESSAGE") > -1 || check_str.toUpperCase().indexOf("MESSAGES") > -1) {
    //Double check for case where both message and Message exist in rqmt
    if (
      (check_str.indexOf("message") < 0 || check_str.indexOf("Message") > -1) &&
      check_str.indexOf("Pre-Exercise Message Script") < 0 &&
      check_str.indexOf("Host Message Filter Words") < 0
    ) {
      rule_detail = rqmt_type + " mixed-case word message(s). For most cases, change to lowercase." + text_ref;
      logs.push(artAttrs);
      errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, rule_detail]);
    }
  } else {
    //Skip output if TM == D. Artifact can be tagged as Verified in DOORS if message name not needed for TM = T, E or A
    if (CSC_str == "COMM" && tMeth_str != "D") {
      if (
        check_str.indexOf("IAW") < 0 ||
        (check_str.indexOf("shall transmit") < 0 &&
          check_str.indexOf("shall receive") < 0 &&
          check_str.indexOf("shall interface") < 0)
      ) {
        rule_detail = rqmt_type + " text - message name or word message(s) not missing or misspelled" + text_ref;
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, rule_detail]);
      }
    } //end CSC & TM check
  } //end Message check
} // end checkCOMMmsg

function checkCOMMmsg_Pspec(rqmt_type, text_ref, check_str, tMeth_str, artAttrs, verify_type) {
  // check_string passed as lowercase

  var rule_error = "Error";
  var rule_grp = "COMM message check";
  var rule_detail = "";

  //Note: All uses of the word "message" should be lowercase; this condition cannot be tagged as pre-Verified
  if (check_str.toUpperCase().indexOf("MESSAGE") > -1 || check_str.toUpperCase().indexOf("MESSAGES") > -1) {
    if (
      check_str.toUpperCase().indexOf("SHALL TRANSMIT") < 0 &&
      check_str.toUpperCase().indexOf("SHALL RECEIVE") < 0 &&
      check_str.toUpperCase().indexOf("SHALL FORWARD") < 0 &&
      check_str.toUpperCase().indexOf("FORWARD MESSAGES") < 0 &&
      check_str.toUpperCase().indexOf("MESSAGE TRANSMISSION") < 0 &&
      check_str.toUpperCase().indexOf("FOR TRANSMISSION") < 0 &&
      check_str.toUpperCase().indexOf("UPON TRANSMISSION") < 0 &&
      check_str.toUpperCase().indexOf("AFTER TRANSMISSION") < 0 &&
      check_str.toUpperCase().indexOf("UPON RECEIPT OF") < 0 &&
      check_str.toUpperCase().indexOf("AFTER RECEIPT OF") < 0 &&
      check_str.toUpperCase().indexOf("FDL TMGMT MESSAGE") < 0 &&
      check_str.toUpperCase().indexOf("FDL BMGMT MESSAGE") < 0 &&
      check_str.toUpperCase().indexOf("FDL SMGMT MESSAGE") < 0 &&
      check_str.toUpperCase().indexOf("TO EXCHANGE") < 0 &&
      check_str.toUpperCase().indexOf("SHALL EXCHANGE") < 0
    ) {
      //Double check so if message is found in addition to Message, error is generated
      if (check_str.indexOf("message") < 0 || check_str.indexOf("Messages") > -1) {
        rule_detail =
          rqmt_type +
          " text - use of message name or word message is intentional. If yes, then change word message(s) to lowercase" +
          text_ref;
      } else {
        rule_detail = rqmt_type + " text - use of message name or word message is intentional." + text_ref;
      }
      logs.push(artAttrs);
      errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, rule_detail]);
    } //Strings accepted, but message must still be lowercase
    else {
      if (check_str.indexOf("message") < 0 || check_str.indexOf("Messages") > -1) {
        rule_detail = rqmt_type + " text - change word message(s) to lowercase" + text_ref;
        logs.push(artAttrs);
        errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]); //this cannot be skipped
      }
    } // end check strings
  } //end word Message check
} // end function checkCOMMmsg

function checkCOMMsend_rules(rqmt_type, text_ref, check_str, CSC_str) {
  //check_str passed as UPPERcase

  var error_out = "";

  if (CSC_str == "COMM") {
    if (check_str.indexOf("SHALL SEND") > -1) {
      error_out = rqmt_type + " COMM text - change SHALL SEND to SHALL TRANSMIT" + text_ref;
    }
  } //end CSC check

  return error_out;
} // end function

function checkword_rules(rqmt_type, text_ref, check_str, artAttrs, verify_type) {
  //check_str passed as lowercase
  var rule_detail = "";
  var rule_grp = "Word choice issues";

  if (verify_type.indexOf("Please") > -1) verify_type = "Error"; //override since currently regarded as Error

  if (check_str.toUpperCase().indexOf("BEING") > -1) {
    rule_detail = rqmt_type + " reword requirement to Avoid use of transitory word BEING" + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, verify_type, rule_grp, rule_detail]);
  }
} //end function checkword

function checkwordStyle_rules(rqmt_type, text_ref, check_str, artAttrs) {
  //check_str passed as lowercase

  var rule_detail = "";
  var rule_error = "Error";
  var rule_grp = "Writing style";

  //Sfty updates are deferred until subsequent change is needed
  var yRuleBreak_OK = new String(artAttrs.values["yRuleBreak_OK"]);
  if (yRuleBreak_OK == null || yRuleBreak_OK == undefined) yRuleBreak_OK = "";

  if (yRuleBreak_OK.indexOf("ptRsafety") > -1) {
    rule_error = "Future Sfty Change";
  }
  // ---------------------------------------------------------------------

  if (check_str.indexOf("System C2 transitioning") > -1) {
    rule_detail = rqmt_type + ' change "Upon System C2 transitioning" to "Upon transition"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  } else if (check_str.indexOf("transitioning") > -1) {
    rule_detail = rqmt_type + ' change "transitioning" to "transition" (e.g., Upon transition)' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.toUpperCase().indexOf("ACKNOWLEDGEMENT") > -1) {
    rule_detail = rqmt_type + ' change "acknowledgement" to "acknowledgment" for consistency' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("After receiving") > -1) {
    rule_detail = rqmt_type + ' change "After receiving..." to D&R style "After receipt of".' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("changing to") > -1) {
    rule_detail = rqmt_type + ' change "changing to" to "changes to"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("control code command") > -1) {
    rule_detail = rqmt_type + ' change "control code command" to "command"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  var count_str = check_str.match("Air Track");
  if (check_str.indexOf("for the Air Track") > -1 && count_str.length > 1) {
    rule_detail =
      rqmt_type +
      ' Multiple instances of Air Track in text, so change "for the Air Track" to "for the track"' +
      text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("organic ABT sensors") > -1) {
    rule_detail = rqmt_type + ' change "organic ABT sensors" to "ABT sensors"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("non-friend") > -1) {
    rule_detail = rqmt_type + ' change "non-friend" to "non-Friend"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("not to exceed") > -1) {
    rule_detail = rqmt_type + ' change "not to exceed" to "not exceeding"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("two separate operations by the operator") > -1) {
    rule_detail = rqmt_type + ' change "two separate operations by the operator" to "two operator actions"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon detecting") > -1) {
    rule_detail = rqmt_type + ' change "Upon detecting" to "Upon detection of" or "Upon detection that".' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon determining") > -1) {
    rule_detail =
      rqmt_type +
      ' change "Upon determining" to "Upon determination of" or "Upon determination that". Should determination be replaced with detection?' +
      text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon issuing") > -1) {
    rule_detail = rqmt_type + ' change "Upon issuing..." to D&R style "Upon issuance of".' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon operator authorization to") > -1) {
    rule_detail =
      rqmt_type +
      ' change "Upon operator authorization to..." to D&R style "Upon operator action to authorize...".' +
      text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("After receiving") > -1) {
    rule_detail = rqmt_type + ' change "After receiving..." to D&R style "After receipt of".' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon receiving") > -1) {
    rule_detail = rqmt_type + ' change "Upon receiving..." to D&R style "Upon receipt of".' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("Upon the operator") > -1) {
    rule_detail =
      rqmt_type +
      ' change "Upon the operator..." to D&R style "Upon operator action to create/modify/etc.".' +
      text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }

  if (check_str.indexOf("which has been sent") > -1 || check_str.indexOf("which have been sent") > -1) {
    rule_detail = rqmt_type + ' change "which" to "that"' + text_ref;
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rule_detail]);
  }
} //end function checkwordStyle

function checkEndPeriod_rules(artifact_type, text_ref, check_str) {
  //check_str passed as lowercase
  //function only called for non-blank check_str

  var error_out = "";

  //period not expected for bullets or enum lists
  if (check_str.indexOf("@@@") < 0 && check_str.charAt(check_str.length - 1) != ".") {
    if (text_ref.indexOf("CP") > 1 || (text_ref.indexOf("PT") > 1 && check_str.indexOf("New") < 0)) {
      error_out = artifact_type + " end Period missing or trailing white-space" + text_ref;
    }
  } //end of @@@ & missing period test

  return error_out;
} //end function

function checkBadChar_rules(rqmt_type, text_ref, check_str, artAttrs) {
  //check_str passed as lowercase

  var rule_error = "Error";
  var rule_grp = "Unwanted Chars";

  //Rule Break is not supported for these checks

  if (check_str.indexOf(`  `) > -1 || check_str.indexOf(`&nbsp; `) > -1 || check_str.indexOf(`&nbsp;&nbsp;`) > -1) {
    logs.push(artAttrs);
    errorMessages.push([
      artAttrs.ref.uri,
      rule_error,
      rule_grp,
      rqmt_type + " double spaces or trailing white space" + text_ref,
    ]);
  }

  if (check_str.indexOf("..") > -1) {
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rqmt_type + " double period" + text_ref]);
  }

  if (check_str.indexOf("&nbsp;") > -1) {
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rqmt_type + " non-breaking space" + text_ref]);
  }

  if (check_str.length > 0) {
    if (check_str.charAt(check_str.length - 1) == ` ` && check_str.charAt(check_str.length - 1) == "\n") {
      logs.push(artAttrs);
      errorMessages.push([
        artAttrs.ref.uri,
        rule_error,
        rule_grp,
        rqmt_type + " trailing white space at end of text" + text_ref,
      ]);
    } else {
      if (check_str.length > 6) {
        if (check_str.slice(-6) == "&nbsp;") {
          logs.push(artAttrs);
          errorMessages.push([
            artAttrs.ref.uri,
            rule_error,
            rule_grp,
            rqmt_type + " trailing white space at end of text" + text_ref,
          ]);
        }
      }
    }
  }
  if (check_str.toUpperCase().indexOf("\r") > -1 || check_str.toUpperCase().indexOf(String.fromCharCode(13)) > -1) {
    logs.push(artAttrs);
    errorMessages.push([artAttrs.ref.uri, rule_error, rule_grp, rqmt_type + " hard carriage return" + text_ref]);
  }
} //end function

function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

function removeDuplicatesOfArtifacts(arr) {
  var ids = "";
  var tmpArray = [];
  for (var i = 0; i < arr.length; i++) {
    var uri = arr[i].ref.uri;
    var str = uri.split("/rm/resources/")[1];
    if (ids.indexOf(str) < 0) {
      tmpArray.push(arr[i]);
    }
    ids = ids + str + ",";
  }
  return tmpArray;
}

async function displayLogs(logs, errorMessages) {
  if (logs.length > 0) {
    logs = removeDuplicates(logs);
    $("#logs").removeAttr("hidden");
    $("#log").html(
      `<p class="p-1 m-0"><b>Log:</b><span id="all" class="badge badge-success" style="float:right;">Select all</span><br/><span id="none" class="badge badge-danger" style="float:right;">Deselect all</span></p>`
    );
    enableResultsControl();
    logs.forEach(function (item, index) {
      var id = item.values[RM.Data.Attributes.IDENTIFIER];
      var name = item.values[RM.Data.Attributes.NAME];
      var type = item.values[RM.Data.Attributes.ARTIFACT_TYPE].name;

      var uri = item.ref.uri;
      var title = "";
      for (var z = 0; z < errorMessages.length; z++) {
        var element = errorMessages[z];
        if (uri == element[0]) {
          if (title == "") {
            title = element[3];
          } else {
            title = title + ",\n" + element[3];
          }
        }
      }
      refs.push(logs[index].ref);

      if (type == "Term") {
        $("<p></p>")
          .html(
            `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-warning">${type}</span></li><ul></p>`
          )
          .appendTo("#log")
          .on("click", function () {
            RM.Client.setSelection(logs[index].ref);
          });
      } else if (type == "_seParameter") {
        $("<p></p>")
          .html(
            `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-success"">${type}</span></li><ul></p>`
          )
          .appendTo("#log")
          .on("click", function () {
            RM.Client.setSelection(logs[index].ref);
          });
      } else if (type == "_seSRS_Rqmt") {
        $("<p></p>")
          .html(
            `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-danger"">${type}</span></li><ul></p>`
          )
          .appendTo("#log")
          .on("click", function () {
            RM.Client.setSelection(logs[index].ref);
          });
      } else if (type == "_sePSPEC_Rqmt") {
        $("<p></p>")
          .html(
            `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-info"">${type}</span></li><ul></p>`
          )
          .appendTo("#log")
          .on("click", function () {
            RM.Client.setSelection(logs[index].ref);
          });
      } else {
        $("<p></p>")
          .html(
            `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-info"">${type}</span></li><ul></p>`
          )
          .appendTo("#log")
          .on("click", function () {
            RM.Client.setSelection(logs[index].ref);
          });
      }
    });
  }
}

function enableResultsControl() {
  $("#all").off("click");
  $("#all").on("click", function () {
    RM.Client.setSelection(refs);
  });

  $("#none").off("click");
  $("#none").on("click", function () {
    RM.Client.setSelection([]);
  });
}
function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

function getAttributesForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref /* ArtifactAttributes[] */, // RULES#
      [
        RM.Data.Attributes.ARTIFACT_TYPE,
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.IDENTIFIER,
        RM.Data.Attributes.PRIMARY_TEXT,
        "State (Rqmt_WF_01)",
        "_Parameter_Value",
        "_Parameter_Unit/Type",
        "__Chng_Proposal",
        "__Subsystems",
        "__test_Method",
        "__test_SafeLOR",
        "__Vers_Initial",
        "__Vers_Rev",
        "__Vers_RevSub",
        "_seGrp_Mission",
        "_seGrp_Tags",
        "_seAssignedPOC",
        "_seSRS_CSC",
        "_seSRS_CSCSub",
        "_test_PassCriteria",
        "_xLnk_Pspec",
        "_xTr_Rqmt_Src",
        "_xTr_SPR_SCR",
        "yRuleBreak_OK",
      ],
      resolve
    );
  });
}

function setsAreEqual(a, b) {
  if (a.size === b.size) {
    for (item of a) {
      if (!b.has(item)) return false;
    }
    for (item of b) {
      if (!a.has(item)) return false;
    }
    return true;
  } else {
    return false;
  }
}
