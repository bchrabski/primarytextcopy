/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 *
 *
 */

var attributeName;
var artRef;
var moduleRef;
var debugMode = false;
var foundData = [];

$(function () {
  if (window.RM) {
    if (debugMode == "true") console.log("Debug Mode ... Widget styleClean Started");
    if (debugMode == "true") console.group("Widget Initialization");

    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();
    debugMode = prefs.getString("debugMode");

    if (debugMode == "true") console.log("Debug Mode ... debug mode = " + debugMode);

    $("#cleanButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      $("#cleanButton").attr("disabled", "disabled");
      $("#status").removeClass("warning correct incorrect");
      $("#status").addClass("warning");
      $("#status").html("<b>Message:</b> Removing styles from primary text in progress.");

      if (debugMode == "true") console.log("Debug Mode ... Clean button clicked by the user");
      cleanHtmlInPrimaryText(artRef);
    });
    if (debugMode == "true") console.groupEnd("Widget Initialization");
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

function lockAllButtons() {
  $("#findButton").attr("disabled", "disabled");
  $("#findButton").removeClass("btn-primary");
  $("#findButton").addClass("btn-secondary");
  $("#cleanButton").attr("disabled", "disabled");
  $("#cleanButton").removeClass("btn-primary");
  $("#cleanButton").addClass("btn-secondary");
}

function cleanHtmlInPrimaryText(ref) {
  if (debugMode == "true") console.group("Function cleanHtmlInPrimaryText");
  if (debugMode == "true") console.log("Debug Mode ... Function -> cleanHtmlInPrimaryText Start");
  if (debugMode == "true") console.log("Debug Mode ... Getting primary text for selected artifacts");
  RM.Data.getAttributes(ref /* ArtifactAttributes[] */, [RM.Data.Attributes.PRIMARY_TEXT], function (opResult) {
    if (opResult.code !== RM.OperationResult.OPERATION_OK) {
      if (debugMode == "true") {
        console.log("Debug Mode ... Getting artifact primary text failed with error: " + opResult.code);
        console.log("Debug Mode ... Error message: " + opResult.message);
        console.log("Debug Mode ... Function -> cleanHtmlInPrimaryText End");
        console.groupEnd("Function cleanHtmlInPrimaryText");
      }
      clearSelectedInfo();
      $("#cleanButton").removeAttr("disabled");
      $("#status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
    } else {
      if (debugMode == "true") {
        console.log("Debug Mode ... Fetched artifact primary text");
        console.log("Debug Mode ... Found number of artifacts: " + result.data.length);
        console.log("Debug Mode ... data:");
        console.log(result.data);
        console.log("Debug Mode ... data end.");
      }
      var toSave = [];
      if (debugMode == "true") console.log("Debug Mode ... Starting removing styles from primary text");
      opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
        var text = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
        if (text) {
          console.log(" ! --- ");
          console.log(text);
          text = text.replace(/(style=")([a-zA-Z0-9:;\.\s\(\)\-\,]*)(")/gi, "");
          console.log(" @ --- ");
          console.log(text);
          console.log(" --- ");

          if (debugMode == "true") console.log(`Debug Mode ... Cleaining styles in ${str}`);
          artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT] = text;
          toSave.push(artAttrs);
        }
      });

      if (debugMode == "true") console.log("Debug Mode ... Finished modyfing primary text.");

      if (toSave.length == 0) {
        if (debugMode == "true")
          console.log(
            "Debug Mode ... None of the selected artifacts was updated. Attribute did not exist or was empty."
          );
        clearSelectedInfo();
        $("#cleanButton").removeAttr("disabled");
        $("#status").addClass("correct").html(`<b> Message:</b > None of the selected artifacts was updated.`);
        gadgets.window.adjustHeight();
      }

      // SAVE
      // Perform a bulk save for all changed attributes
      RM.Data.setAttributes(toSave, function (result) {
        if (debugMode == "true") console.log("Debug Mode ... Saving modified atrifacts.");
        if (result.code !== RM.OperationResult.OPERATION_OK) {
          if (debugMode == "true") {
            console.log("Debug Mode ... Saving artifact failed with error: " + opResult.code);
            console.log("Debug Mode ... Error message: " + opResult.message);
            console.log("Debug Mode ... Function -> cleanHtmlInPrimaryText End");
            console.groupEnd("Function cleanHtmlInPrimaryText");
          }
          clearSelectedInfo();
          $("#cleanButton").removeAttr("disabled");
          $("#status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
          // error handling code here
        } else {
          if (debugMode == "true") console.log("Debug Mode ... Mofified artifact(s) without errors");
          if (debugMode == "true") console.log("Debug Mode ... Function -> cleanHtmlInPrimaryText End");
          if (debugMode == "true") console.groupEnd("Function cleanHtmlInPrimaryText");
          clearSelectedInfo();
          $("#cleanButton").removeAttr("disabled");
          $("#status").addClass("correct").html(`<b> Message:</b > ${toSave.length} artifact(s) was/were updated. `);
        }
      });
    }
  });
}

function findAllArtifactsWithSyles() {
  if (debugMode == "true") console.group("Function findAllArtifactsWithSyles");
  if (debugMode == "true") console.log("Debug Mode ... Function -> findAllArtifactsWithSyles Start");
  if (debugMode == "true") console.log("Debug Mode ... Getting all the artifacts from the module");

  // Blocking the button and starting creation process

  $("#log").html(
    "This log message is displayed because there are more than 200 artifact in the module with embedded HTML styles<p><b>Log:</b></p></div>"
  );

  clearSelectedInfo();
  $("#status").addClass("warning");
  $("#status").html("<b>Message:</b> Searching for artifacts with styles in primary text.");
  $("#findButton").attr("disabled", "disabled");

  RM.Data.getContentsAttributes(
    moduleRef,
    [RM.Data.Attributes.NAME, RM.Data.Attributes.IDENTIFIER, RM.Data.Attributes.PRIMARY_TEXT],
    function (result) {
      if (result.code !== RM.OperationResult.OPERATION_OK) {
        if (debugMode == "true") {
          console.log("Debug Mode ... Getting artifact primary text failed with error: " + result.code);
          console.log("Debug Mode ... Error message: " + result.message);
          console.log("Debug Mode ... Function -> findAllArtifactsWithSyles End");
          console.groupEnd("Function findAllArtifactsWithSyles");
          $("#findButton").removeAttr("disabled");
        }
        clearSelectedInfo();
        $("#cleanButton").removeAttr("disabled");
        $("#status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
      } else {
        var found = [];
        foundData = [];
        if (debugMode == "true") {
          console.log("Debug Mode ... Found number of objects: " + result.data.length);
          console.log("Debug Mode ... data:");
          console.log(result.data);
          console.log("Debug Mode ... data end.");
        }
        for (var i = 0; i < result.data.length; i++) {
          var item = result.data[i];
          var text = item.values[RM.Data.Attributes.PRIMARY_TEXT];
          var id = item.values[RM.Data.Attributes.IDENTIFIER];
          var name = item.values[RM.Data.Attributes.NAME];
          if (text.indexOf("style=") > 0) {
            found.push(item.ref);
            foundData.push(item);
            if (debugMode == "true") {
              console.log(`Debug Mode ... Found artifact ${id}: ${name}.`);
            }
          }
        }

        if (debugMode == "true") {
          console.log(`Debug Mode ... Found ${found.length} artifact(s) with HTML styles.`);
        }

        if (found.length == 0) {
          clearSelectedInfo();
          $("#findButton").removeAttr("disabled");
          $("#status")
            .addClass("correct")
            .html(`<b> Message:</b > None of artifacts in the module contains HTML styles.`);
        } else if (found.length > 200) {
          clearSelectedInfo();
          $("#findButton").removeAttr("disabled");
          $("#status")
            .addClass("warning")
            .html(
              `<b> Message:</b > Found more than 200 artifacts. DOORS Next does not allow to select more than 200 artifacts in the module.`
            );
          $("#cleanButton").removeAttr("disabled");
          $("#log").removeAttr("hidden");

          $("#log").html(
            `This log message is displayed because there are more than 200 artifact in the module with embedded HTML styles<p><b>Log:</b></p></div>`
          );

          foundData.forEach(function (item, index) {
            var id = item.values[RM.Data.Attributes.IDENTIFIER];
            var name = item.values[RM.Data.Attributes.NAME];

            $("<p></p>")
              .html(`<p><ul><li>${id}: ${name}</li><ul></p>`)
              .appendTo("#log")
              .on("click", function () {
                if (debugMode == "true") console.log("Debug Mode ... Selected element with index: " + index);
                if (debugMode == "true") console.log("Debug Mode ... Data:");
                if (debugMode == "true") console.log(foundData[index]);
                RM.Client.setSelection(foundData[index].ref);
              });
          });

          gadgets.window.adjustHeight();
        } else {
          RM.Client.setSelection(found);
          clearSelectedInfo();
          $("#findButton").removeAttr("disabled");
          $("#status")
            .addClass("correct")
            .html(`<b> Message:</b > Found ${found.length} artifacts and marked them as selected`);
          $("#cleanButton").removeAttr("disabled");
          $("#log").removeAttr("hidden");

          $("#log").html("<b>Log:</b></div>");

          foundData.forEach(function (item, index) {
            var id = item.values[RM.Data.Attributes.IDENTIFIER];
            var name = item.values[RM.Data.Attributes.NAME];

            $("<p></p>")
              .html(`<p><ul><li>${id}: ${name}</li><ul></p>`)
              .appendTo("#log")
              .on("click", function () {
                if (debugMode == "true") console.log("Debug Mode ... Selected element with index: " + index);
                if (debugMode == "true") console.log("Debug Mode ... Data:");
                if (debugMode == "true") console.log(foundData[index]);
                RM.Client.setSelection(foundData[index].ref);
              });
          });

          gadgets.window.adjustHeight();
        }
      }
      console.groupEnd("Function findAllArtifactsWithSyles");
      $("#findButton").removeAttr("disabled");
    }
  );
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (selected.length === 1) {
    if (debugMode == "true") console.log("Debug Mode ... Single artifact selected");
    artRef = selected;
    $("#cleanButton").removeAttr("disabled");
    clearSelectedInfo();
    $("#status").html(`<b>Message:</b> Press button "Clean HTML styles for ${selected.length} artifact`);
  } else if (selected.length > 1) {
    if (debugMode == "true") console.log("Debug Mode ... Multiple artifacts selected");
    artRef = selected;
    $("#cleanButton").removeAttr("disabled");
    clearSelectedInfo();
    $("#status").html(`<b>Message:</b> Press button "Clean HTML styles for ${selected.length} artifacts`);
  } else {
    // clear the display area...
    if (debugMode == "true") console.log("Debug Mode ... None attrToPrimaryTextCopy selected");
    $("#cleanButton").attr("disabled", "disabled");
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $("#status").html(
      `<b>Message:</b> Please select requirements for which You want to clean primary text and then press "Clean HTML styles for selected artifacts" button.`
    );
  }
  gadgets.window.adjustHeight();
});

function clearSelectedInfo() {
  $("#status").removeClass("incorrect correct warning");
}

function checkIfModule(opResult) {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("Function checkIfModule");
  if (debugMode == "true") console.log("Debug Mode ... Function -> checkIfModule Start");
  // ***

  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    if (debugMode == "true") console.log("Debug Mode ... Get information about module attributes");
    var attrs = opResult.data[0];
    moduleRef = attrs.ref;

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      $("#findButton").removeAttr("disabled").on("click", findAllArtifactsWithSyles);
      $("#cleanButton").attr("disabled", "disabled");
      $("#log").html(
        "This log message is displayed because there are more than 200 artifact in the module with embedded HTML styles<p><b>Log:</b></p></div>"
      );
      if (debugMode == "true") console.log("Debug Mode ... Get information about module attributes");
    } else {
      $("#findButton").attr("disabled", "disabled");
    }
  }

  if (debugMode == "true") console.log("Debug Mode ... Function -> checkIfModule End");
  if (debugMode == "true") console.groupEnd("Function checkIfModule");
}

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  if (debugMode == "true") console.group("Subscription");
  if (debugMode == "true") console.log("Debug Mode ... Opening an artifact.");

  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
  if (debugMode == "true") console.groupEnd("Subscription");
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  if (debugMode == "true") console.group("Subscription");
  if (debugMode == "true") console.log("Debug Mode ... Closing an artifact.");
  $("#log").html(
    "This log message is displayed because there are more than 200 artifact in the module with embedded HTML styles<p><b>Log:</b></p></div>"
  );
  $("#findButton").attr("disabled", "disabled");
  $("#log").attr("hidden", "hidden");
  gadgets.window.adjustHeight();

  moduleRef = null;
  if (debugMode == "true") console.groupEnd("Subscription");
});
