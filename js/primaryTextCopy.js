/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2021
 * Contact information: info@reqpro.com
 */

var attributeName;
var prefix;
var artRef;
var space;
var debug = false;
runningProcess = false;

if (!String.prototype.decodeHTML) {
  String.prototype.decodeHTML = function () {
    return this.replace(/&apos;/g, "'")
      .replace(/&quot;/g, '"')
      .replace(/&gt;/g, ">")
      .replace(/&lt;/g, "<")
      .replace(/&amp;/g, "&");
  };
}

if (!String.prototype.encodeHTML) {
  String.prototype.encodeHTML = function () {
    return this.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&apos;");
  };
}

window.addEventListener("load", function () {
  gadgets.window.adjustHeight();
});

window.addEventListener("resize", function () {
  gadgets.window.adjustHeight();
});

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();
    attributeName = prefs.getString("attributeName");
    prefix = prefs.getString("prefix");
    space = prefs.getBool("space");
    $("#copyButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      lockAllButtons();
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html("<b>Message:</b> Copying primary text to attribute in progress. PLEASE WAIT...");

      if (attributeName == null || attributeName == "" || attributeName == "undefined" || attributeName == undefined) {
        clearSelectedInfo();
        $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
        lockAllButtons();
      } else {
        copyAttributeToPrimaryText(artRef);
      }
      gadgets.window.adjustHeight();
    });
  } else {
    clearSelectedInfo();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    lockAllButtons();
    gadgets.window.adjustHeight();
  }
});

function copyAttributeToPrimaryText(ref) {
  runningProcess = true;
  RM.Client.getCurrentConfigurationContext(function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;
      var projectURL = getProjectAreaURL(componentURL);

      var serverURL = projectURL.split("/process/project-areas/")[0];
      var data = getParametersURL(attributeName, componentURL, projectURL, configurationURL);

      if (data.parameterURI == "" || data.parameterURI == null || data.parameterURI == undefined) {
        clearSelectedInfo();
        $(".status")
          .addClass("incorrect")
          .html(`<b>Error:</b> The attribute ${attributeName} does not exist in the project area.`);
        lockAllButtons();
        gadgets.window.adjustHeight();
        runningProcess = false;
        return;
      }

      RM.Data.getAttributes(
        ref /* ArtifactAttributes[] */,
        [RM.Data.Attributes.PRIMARY_TEXT, attributeName, RM.Data.Attributes.ARTIFACT_TYPE],
        function (opResult) {
          if (opResult.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            lockAllButtons();
            $(".status")
              .addClass("incorrect")
              .html(
                "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
              );
            runningProcess = false;
            return;
          } else {
            var toSave = [];
            opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
              var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];

              if (data.types.includes(type.name)) {
                var pText = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
                if (pText) {
                  pText = pText.trim();
                  let strippedString = pText.replace(/(<\/p>)/gi, "\n");
                  strippedString = strippedString.replace(/(<br\/>)/gi, "\n");
                  strippedString = strippedString.replace(/(<br>)/gi, "\n");
                  strippedString = strippedString.replace(/(<\/li>)/gi, "\n");
                  strippedString = strippedString.replace(/(<\/ul>)/gi, "\n");
                  strippedString = strippedString.replace(/(<([^>]+)>)/gi, "");
                  strippedString = strippedString.decodeHTML();
                  strippedString = strippedString.trim();
                  if (prefix.length == 0) {
                    console.log(pText);
                    artAttrs.values[attributeName] = strippedString;
                  } else {
                    if (space) {
                      artAttrs.values[attributeName] = prefix + " " + strippedString;
                    } else {
                      artAttrs.values[attributeName] = prefix + strippedString;
                    }
                  }
                  delete artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
                  toSave.push(artAttrs);
                }
              }
            });

            if (toSave.length == 0) {
              clearSelectedInfo();
              unlockAllButtons();
              $(".status")
                .addClass("correct")
                .html(
                  `<b> Message:</b > None of the selected artifacts were updated. Please verify specified attribute applies to the selected artifact type(s).`
                );
              gadgets.window.adjustHeight();
              runningProcess = false;
              return;
            }

            // SAVE
            // Perform a bulk save for all changed attributes
            RM.Data.setAttributes(toSave, function (result) {
              if (result.code !== RM.OperationResult.OPERATION_OK) {
                clearSelectedInfo();
                lockAllButtons();

                $(".status")
                  .addClass("incorrect")
                  .html(
                    "<b>Error:</b> Something went wrong during data update. Please check for locked artifacts, verify specified attribute is of type string or text, and then try again."
                  );
                runningProcess = false;
                // error handling code here
              } else {
                clearSelectedInfo();
                unlockAllButtons();
                runningProcess = false;
                $(".status")
                  .addClass("correct")
                  .html(
                    `<b> Message:</b > ${
                      toSave.length
                    } artifact(s) was/were updated. Numbers of artifacts that were not modified ${
                      ref.length - toSave.length
                    }.`
                  );
                gadgets.window.adjustHeight();
              }
              gadgets.window.adjustHeight();
            });
          }
        }
      );
    }
  });
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (runningProcess == false) {
    if (selected.length === 1) {
      artRef = selected;

      unlockAllButtons();

      clearSelectedInfo();
      $(".status").html(
        `<b>Message:</b> Continue artifact selection or press the "Copy Primary Text to attribute" button for ${selected.length} artifact(s).`
      );
    } else if (selected.length > 1) {
      artRef = selected;

      unlockAllButtons();

      clearSelectedInfo();
      $(".status").html(
        `<b>Message:</b> Continue artifact selection or press the "Copy Primary Text to attribute" button for ${selected.length} artifact(s).`
      );
    } else {
      // clear the display area...
      lockAllButtons();
      clearSelectedInfo();
      // inform the user that they need to select only one thing.
      $(".status").html(
        `<b>Message:</b> Please select artifact(s) for processing, and then press the "Copy Primary Text to attribute" button.`
      );
    }
    gadgets.window.adjustHeight();
  }
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();
    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");
    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    clearSelectedInfo();
    $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
    lockAllButtons();
    gadgets.window.adjustHeight();
    return;
  }
}

function getParametersURL(parameterName, componentURL, projectURL, configurationURL) {
  var serverURL = projectURL.split("/process/project-areas/")[0];

  var query = serverURL + "/types?resourceContext=" + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open("GET", query, false);
  parameter_req.setRequestHeader("Accept", "application/xml");
  parameter_req.setRequestHeader("Content-Type", "application/xml");
  parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
  parameter_req.setRequestHeader("Configuration-Context", configurationURL);
  parameter_req.send();
  var data = { parameterURI: "", types: [] };
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS(
      "http://www.ibm.com/xmlns/rdm/rdf/",
      "AttributeDefinition"
    );
    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          if (object.childNodes[c].innerHTML == parameterName) {
            data.parameterURI = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
          }
        }
      }
    }

    if (data.parameterURI == "" || data.parameterURI == null || data.parameterURI == undefined) {
      return data;
    }

    objects = parameter_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rdm/rdf/", "ObjectType");
    for (i = 0; i < objects.length; i++) {
      var name = "";
      var type = "";
      var object = objects[i];
      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          name = object.childNodes[c].innerHTML;
        }

        if (object.childNodes[c].nodeName == "rm:hasAttribute") {
          var hsAbout = object.childNodes[c].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
          if (hsAbout == data.parameterURI) {
            type = hsAbout;
          }
          var children = object.childNodes[c].childNodes;
          for (var q = 0; q < children.length; q++) {
            if (children[q].nodeName == "rm:AttributeDefinition") {
              var adAbout = children[q].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
              if (adAbout == data.parameterURI) {
                type = adAbout;
              }
            }
          }
        }
      }
      if (type != "") {
        data.types.push(name);
      }
    }

    return data;
  } catch (err) {
    console.log(err);
    clearSelectedInfo();
    $(".status")
      .addClass("incorrect")
      .html(
        "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
      );
    lockAllButtons();
    gadgets.window.adjustHeight();
    return data;
  }
}

function lockAllButtons() {
  $("#copyButton").attr("disabled", "disabled");
  $("#copyButton").removeClass("btn-primary");
  $("#copyButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#copyButton").removeAttr("disabled");
  $("#copyButton").addClass("btn-primary");
  $("#copyButton").removeClass("btn-secondary");
}
