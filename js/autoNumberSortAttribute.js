/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2022
 * Contact information: info@reqpro.com
 */

var attributeName;
var startValue;
var incrementValue;
var artRef;
var moduleRef;
var runningProcess = false;

if (!String.prototype.decodeHTML) {
  String.prototype.decodeHTML = function () {
    return this.replace(/&apos;/g, "'")
      .replace(/&quot;/g, '"')
      .replace(/&gt;/g, ">")
      .replace(/&lt;/g, "<")
      .replace(/&amp;/g, "&");
  };
}

if (!String.prototype.encodeHTML) {
  String.prototype.encodeHTML = function () {
    return this.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&apos;");
  };
}

window.addEventListener("load", function () {
  gadgets.window.adjustHeight();
});

window.addEventListener("resize", function () {
  gadgets.window.adjustHeight();
});

$(function () {
  gadgets.window.adjustHeight();
  var prefs = new gadgets.Prefs();
  attributeName = prefs.getString("attributeName");
  startValue = prefs.getString("startValue");
  incrementValue = prefs.getString("incrementValue");

  $(".info").html(
    `<b>Sort Attribute:</b> ${attributeName}, <b>Start:</b> ${startValue}, <b>Incr:</b> ${incrementValue}.`
  );
  if (window.RM) {
    if (isNaN(startValue) || isNaN(incrementValue)) {
      clearSelectedInfo();
      lockButton("#copyButton");
      $(".status").addClass("incorrect").html(`<b>Error:</b> Value of start value or increment value is not a number.`);
      gadgets.window.adjustHeight();
      return;
    }

    gadgets.window.adjustHeight();

    $("#copyButton").on("click", function () {
      runningProcess = true;
      clearSelectedInfo();
      // Blocking the button and starting creation process
      lockAllButtons();
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html("<b>Message:</b> Processing... PLEASE WAIT...");

      if (attributeName == null || attributeName == "" || attributeName == "undefined" || attributeName == undefined) {
        clearSelectedInfo();
        $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
        lockButton("#copyButton");
      } else {
        runningProcess = true;
        copyNumberAttribute(artRef);
      }
      gadgets.window.adjustHeight();
    });
  } else {
    clearSelectedInfo();
    lockButton("#copyButton");
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

function copyNumberAttribute(ref) {
  runningProcess = true;

  RM.Client.getCurrentConfigurationContext(function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;
      var projectURL = getProjectAreaURL(componentURL);

      var serverURL = projectURL.split("/process/project-areas/")[0];
      var data = getParametersURL(attributeName, componentURL, projectURL, configurationURL);

      if (data.parameterURI == "" || data.parameterURI == null || data.parameterURI == undefined) {
        clearSelectedInfo();
        lockButton("#copyButton");
        $(".status")
          .addClass("incorrect")
          .html(`<b>Error:</b> The attribute ${attributeName} does not exist in the project area.`);
        gadgets.window.adjustHeight();
        return;
      }

      RM.Data.getAttributes(
        ref /* ArtifactAttributes[] */,
        [attributeName, RM.Data.Attributes.ARTIFACT_TYPE, RM.Data.Attributes.SECTION_NUMBER],
        function (opResult) {
          if (opResult.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            lockButton("#copyButton");
            $(".status")
              .addClass("incorrect")
              .html(
                "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
              );

            return;
          } else {
            var toSave = [];
            var value = parseInt(startValue);
            if (moduleRef != "" && moduleRef != null && moduleRef != undefined) {
              console.log("SURE");
              for (var i = 0; i < opResult.data.length; i++) {
                var artAttrs = opResult.data[i];
                var section = artAttrs.values[RM.Data.Attributes.SECTION_NUMBER];
                section = section.replace("-", ".");
                artAttrs.values[RM.Data.Attributes.SECTION_NUMBER] = section;
              }

              opResult.data.sort((a, b) =>
                a.values[RM.Data.Attributes.SECTION_NUMBER]
                  .replace(/\d+/g, (n) => +n + 100000)
                  .localeCompare(b.values[RM.Data.Attributes.SECTION_NUMBER].replace(/\d+/g, (n) => +n + 100000))
              );
            }

            opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
              var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
              if (data.types.includes(type.name)) {
                artAttrs.values[attributeName] = parseInt(value);
                value += parseInt(incrementValue);
                delete artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
                toSave.push(artAttrs);
              }
            });

            if (toSave.length == 0) {
              unlockButton("#copyButton");
              clearSelectedInfo();
              $(".status").addClass("correct").html(`<b> Message:</b > None of the selected artifacts were updated.`);
              gadgets.window.adjustHeight();
              return;
            }

            // SAVE
            // Perform a bulk save for all changed attributes
            RM.Data.setAttributes(toSave, function (result) {
              if (result.code !== RM.OperationResult.OPERATION_OK) {
                clearSelectedInfo();
                lockButton("#copyButton");

                $(".status")
                  .addClass("incorrect")
                  .html(
                    "<b>Error:</b> Something went wrong during data update. Please check if selected attribute has type = integer and then please try again."
                  );
                // error handling code here
              } else {
                clearSelectedInfo();
                unlockAllButtons();
                runningProcess = false;
                $(".status")
                  .addClass("correct")
                  .html(
                    `<b> Message:</b > ${toSave.length} artifacts were updated. Artifacts were skipped ${
                      ref.length - toSave.length
                    }.`
                  );
                gadgets.window.adjustHeight();
              }
              gadgets.window.adjustHeight();
            });
          }
        }
      );
    }
  });
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (!runningProcess) {
    if (isNaN(startValue) || isNaN(incrementValue)) {
      clearSelectedInfo();
      lockButton("#copyButton");
      $(".status").addClass("incorrect").html(`<b>Error:</b> Value of start value or increment value is not a number.`);
      return;
    }

    if (moduleRef != null) {
      unlockButton("#copyInModuleButton");
    }

    if (selected.length === 1) {
      artRef = selected;
      unlockButton("#copyButton");

      clearSelectedInfo();
      $(".status").html(
        `<b>Message:</b> Press button "Update values of selected attribute" for ${selected.length} artifacts`
      );
    } else if (selected.length > 1) {
      artRef = selected;
      unlockButton("#copyButton");

      clearSelectedInfo();
      $(".status").html(
        `<b>Message:</b> Press button "Update values of selected attribute" for ${selected.length} artifacts`
      );
    } else {
      // clear the display area...
      lockButton("#copyButton");
      clearSelectedInfo();
      // inform the user that they need to select only one thing.
      $(".status").html(
        `<b>Message:</b>  Please select artifacts for processing, and then press "Update values for selected artifacts" button or select the Module mode button, if active.`
      );
      gadgets.window.adjustHeight();
    }
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  lockButton("#copyInModuleButton");
  moduleRef = null;
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();
    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");
    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    clearSelectedInfo();
    $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
    lockButton("#copyButton");
    return;
  }
}

function getParametersURL(parameterName, componentURL, projectURL, configurationURL) {
  var serverURL = projectURL.split("/process/project-areas/")[0];

  var query = serverURL + "/types?resourceContext=" + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open("GET", query, false);
  parameter_req.setRequestHeader("Accept", "application/xml");
  parameter_req.setRequestHeader("Content-Type", "application/xml");
  parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
  parameter_req.setRequestHeader("Configuration-Context", configurationURL);
  parameter_req.send();
  var data = { parameterURI: "", types: [] };
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS(
      "http://www.ibm.com/xmlns/rdm/rdf/",
      "AttributeDefinition"
    );
    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          if (object.childNodes[c].innerHTML == parameterName) {
            data.parameterURI = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
          }
        }
      }
    }

    if (data.parameterURI == "" || data.parameterURI == null || data.parameterURI == undefined) {
      return data;
    }

    objects = parameter_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rdm/rdf/", "ObjectType");
    for (i = 0; i < objects.length; i++) {
      var name = "";
      var type = "";
      var object = objects[i];
      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          name = object.childNodes[c].innerHTML;
        }

        if (object.childNodes[c].nodeName == "rm:hasAttribute") {
          var hsAbout = object.childNodes[c].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
          if (hsAbout == data.parameterURI) {
            type = hsAbout;
          }
          var children = object.childNodes[c].childNodes;
          for (var q = 0; q < children.length; q++) {
            if (children[q].nodeName == "rm:AttributeDefinition") {
              var adAbout = children[q].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
              if (adAbout == data.parameterURI) {
                type = adAbout;
              }
            }
          }
        }
      }
      if (type != "") {
        data.types.push(name);
      }
    }

    return data;
  } catch (err) {
    console.log(err);
    clearSelectedInfo();
    $(".status")
      .addClass("incorrect")
      .html(
        "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
      );

    lockButton("#copyButton");
    gadgets.window.adjustHeight();
    return data;
  }
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      lockButton("#copyButton");
      unlockButton("#copyInModuleButton");
      $("#copyInModuleButton").on("click", copyForModule);
    }
  }
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(
      ref,
      [attributeName, RM.Data.Attributes.ARTIFACT_TYPE, RM.Data.Attributes.SECTION_NUMBER],
      resolve
    );
  });
}

function copyForModule() {
  runningProcess = true;
  lockAllButtons();
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning");
  $(".status").html("<b>Message:</b> Processing... PLEASE WAIT...");
  gadgets.window.adjustHeight();

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;
      var projectURL = getProjectAreaURL(componentURL);

      var serverURL = projectURL.split("/process/project-areas/")[0];
      var data = getParametersURL(attributeName, componentURL, projectURL, configurationURL);

      if (data.parameterURI == "" || data.parameterURI == null || data.parameterURI == undefined) {
        clearSelectedInfo();
        lockButton("#copyButton");
        $(".status")
          .addClass("incorrect")
          .html(`<b>Error:</b> The attribute ${attributeName} does not exist in the project area.`);
        gadgets.window.adjustHeight();
        runningProcess = false;
        return;
      }
      var opResult = await getModuleArtifacts(moduleRef);

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockButton("#copyInModuleButton");
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("incorrect")
          .html(
            "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
          );
      } else {
        var toSave = [];
        var ref = 0;
        var value = parseInt(startValue);

        for (var i = 0; i < opResult.data.length; i++) {
          var artAttrs = opResult.data[i];
          var section = artAttrs.values[RM.Data.Attributes.SECTION_NUMBER];
          section = section.replace("-", ".");
          artAttrs.values[RM.Data.Attributes.SECTION_NUMBER] = section;
        }

        opResult.data.sort((a, b) =>
          a.values[RM.Data.Attributes.SECTION_NUMBER]
            .replace(/\d+/g, (n) => +n + 100000)
            .localeCompare(b.values[RM.Data.Attributes.SECTION_NUMBER].replace(/\d+/g, (n) => +n + 100000))
        );

        // opResult.data.sort((a, b) =>
        //   a[1].replace(/\d+/g, (n) => +n + 100000).localeCompare(b[1].replace(/\d+/g, (n) => +n + 100000))
        // );

        opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
          ref += 1;
          var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
          if (data.types.includes(type.name)) {
            artAttrs.values[attributeName] = parseInt(value);
            value += parseInt(incrementValue);
            delete artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE];
            toSave.push(artAttrs);
          }
        });

        if (toSave.length == 0) {
          unlockButton("#copyInModuleButton");
          clearSelectedInfo();
          $(".status").addClass("correct").html(`<b> Message:</b > None of the selected artifacts were updated.`);
          gadgets.window.adjustHeight();
          runningProcess = false;
          return;
        }

        for (var q = 0; q < toSave.length; q = q + 50) {
          var slicedArray = toSave.slice(q, q + 50);
          var result = await saveArtifact(slicedArray);
          if (result.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            lockAllButtons();
            $(".status")
              .addClass("incorrect")
              .html(
                "<b>Error:</b> Something went wrong during data update. Please check if selected attribute has type = integer and then try again."
              );
            runningProcess = false;
            return;
          } else {
            if (q + 50 < toSave.length) {
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("warning");
              $(".status").html(
                `<b>Message:</b> Processing... PLEASE WAIT... <b>Updated ${q + 50} of ${
                  toSave.length
                } artifacts</b> containing ${attributeName}.<br/> Total number of artifacts in the module ${
                  opResult.data.length
                }`
              );
              gadgets.window.adjustHeight();
            }
          }
        }
        clearSelectedInfo();
        unlockButton("#copyInModuleButton");
        $(".status")
          .addClass("correct")
          .html(
            `<b> Message:</b > ${toSave.length} artifacts were updated.<br/>${
              ref - toSave.length
            } artifacts were skipped.<br>Total number of artifacts in the module ${ref}.`
          );
        gadgets.window.adjustHeight();
        runningProcess = false;
      }
    }
  });
}

async function saveArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(ref, resolve);
  });
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

function lockAllButtons() {
  $("#copyButton").attr("disabled", "disabled");
  $("#copyButton").removeClass("btn-primary");
  $("#copyButton").addClass("btn-secondary");
  $("#copyInModuleButton").attr("disabled", "disabled");
  $("#copyInModuleButton").removeClass("btn-primary");
  $("#copyInModuleButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#copyButton").removeAttr("disabled");
  $("#copyButton").addClass("btn-primary");
  $("#copyButton").removeClass("btn-secondary");

  if (moduleRef == null) return;
  $("#copyInModuleButton").removeAttr("disabled");
  $("#copyInModuleButton").addClass("btn-primary");
  $("#copyInModuleButton").removeClass("btn-secondary");
}
