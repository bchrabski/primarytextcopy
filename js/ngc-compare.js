/* Property of NGC Northrop Grumman
	Author Bartosz Chrabski
*/
$(function () {

	RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {

		if (selected.length == 1) {

			RM.Data.getAttributes(selected[0], ['_seChng_Proposal', RM.Data.Attributes.PRIMARY_TEXT], function (result) {
				if (result.code !== RM.OperationResult.OPERATION_OK) {
					return;
				}
				// Generate a summary of high priority items in the module
				var primaryText = result.data[0].values[RM.Data.Attributes.PRIMARY_TEXT];
				var _seChng_Proposal = result.data[0].values["_seChng_Proposal"];
				var plainPrimaryText = toPlainText(primaryText);

				if (_seChng_Proposal == null) _seChng_Proposal = "";

				var diff = Diff.createTwoFilesPatch("Primary Text", "_seChng_Proposal", primaryText, _seChng_Proposal,);

				var diffHtml = Diff2Html.getPrettyHtml(diff, { inputFormat: 'diff', showFiles: false, matching: 'lines', outputFormat: 'line-by-line' });
				$("#compareHTML").empty();

				$("#compareHTML").append(diffHtml);

				if (plainPrimaryText.length > 1) {
					plainPrimaryText = plainPrimaryText.substring(0, plainPrimaryText.length - 1);
				}

				var diffPlain = Diff.createTwoFilesPatch("Primary Text", "_seChng_Proposal", plainPrimaryText, _seChng_Proposal,);
				var diffHtmlPlain = Diff2Html.getPrettyHtml(diffPlain, { inputFormat: 'diff', showFiles: false, matching: 'lines', outputFormat: 'line-by-line' });



				$("#comparePLAIN").empty();
				$("#attributes").empty();
				$("#attributes2").empty();
				$("#links").empty();

				$("#comparePLAIN").append(diffHtmlPlain);


				$("#attributes").append(plainPrimaryText);
				$("#attributes2").append(primaryText);
				$("#links").append(_seChng_Proposal);
				$("#info").hide();

				$("#inputs").show();
				$("#comparePLAIN").show();
				$("#compareHTML").show();



			});


		} else {
			// Otherwise, clear our display.
			$("#inputs").hide();
			$("#comparePLAIN").hide();
			$("#compareHTML").hide();
			$("#info").show();
		}
	});

});

function toPlainText(text) {

	var textTmp;
	textTmp = text.replace(/<a(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<\/a(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<h(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<\/h(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<span(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<\/span(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<div(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<\/div(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<b>/gi, '');
	textTmp = textTmp.replace(/<\/b>/gi, '');
	textTmp = textTmp.replace(/<i>/gi, '');
	textTmp = textTmp.replace(/<\/i>/gi, '');
	textTmp = textTmp.replace(/<u>/gi, '');
	textTmp = textTmp.replace(/<\/u>/gi, '');
	textTmp = textTmp.replace(/style="[a-zA-Z0-9:;\.\s\(\)\-\,\#\:]*"/gi, '');
	textTmp = textTmp.replace(/<p(.|\n|\d)*?>/gi, '');
	textTmp = textTmp.replace(/<\/p(.|\n|\d)*?>/gi, '\n');
	textTmp = textTmp.replace(/^\s*\n/gm, '');
	textTmp = textTmp.replace(/<br>/gm, '');
	textTmp = textTmp.replace(/&.+;/gm, '');
	return textTmp;
}
