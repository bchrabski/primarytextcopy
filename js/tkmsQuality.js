/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2021
 * Contact information: info@reqpro.com
 */

var moduleRef;
var artRef;
var logs = [];
var errorMessages = [];
var refs = [];

var all_PT_embedded;
var all_PT_embedded_link;
var all_PT_specialCharacters;
var all_moduleLinks;

var runningProcess = false;

window.addEventListener("load", function () {
  gadgets.window.adjustHeight();
});

window.addEventListener("resize", function () {
  gadgets.window.adjustHeight();
});

function enableResultsControl() {
  $("#all").off("click");
  $("#all").on("click", function () {
    RM.Client.setSelection(refs);
  });

  $("#none").off("click");
  $("#none").on("click", function () {
    RM.Client.setSelection([]);
  });
}

$("#all-all").on("click", function () {
  var prefs = new gadgets.Prefs();
  $("#all_moduleLinks").prop("checked", true);
  prefs.set("all_moduleLinks", true);
  all_moduleLinks = true;

  $("#all_PT_embedded").prop("checked", true);
  prefs.set("all_PT_embedded", true);
  all_PT_embedded = true;

  $("#all_PT_embedded_link").prop("checked", true);
  prefs.set("all_PT_embedded_link", true);
  all_PT_embedded_link = true;

  $("#all_PT_specialCharacters").prop("checked", true);
  prefs.set("all_PT_specialCharacters", true);
  all_PT_specialCharacters = true;
});

$("#none-all").on("click", function () {
  var prefs = new gadgets.Prefs();
  $("#all_PT_embedded").prop("checked", false);
  prefs.set("all_PT_embedded", false);
  all_PT_embedded = false;

  $("#all_moduleLinks").prop("checked", false);
  prefs.set("all_moduleLinks", false);
  all_moduleLinks = false;

  $("#all_PT_embedded_link").prop("checked", false);
  prefs.set("all_PT_embedded_link", false);
  all_PT_embedded_link = false;

  $("#all_PT_specialCharacters").prop("checked", false);
  prefs.set("all_PT_specialCharacters", false);
  all_PT_specialCharacters = false;
});

$(".btn").on("click", function () {
  setTimeout(function () {
    gadgets.window.adjustHeight();
  }, 200);
});

$("#generateExcelReport").on("click", function () {
  $("#generateExcelReport").attr("disabled", "disabled");
  var wb = XLSX.utils.book_new();
  wb.Props = {
    Title: "Widget Results",
    Subject: "Widget Results",
    Author: "Widget Results",
  };

  wb.SheetNames.push("Widget Results");
  var wscols = [{ wch: 10 }, { wch: 30 }, { wch: 20 }, { wch: 40 }];

  var ws_data = [["ID", "NAME", "ARTIFACT TYPE", "RULE"]];

  console.log(errorMessages);

  for (var q = 0; q < errorMessages.length; q++) {
    var element = errorMessages[q];

    ws_data.push([element[2], element[3], element[4], element[1]]);
  }

  var ws = XLSX.utils.aoa_to_sheet(ws_data);
  ws["!cols"] = wscols;
  wb.Sheets["Widget Results"] = ws;

  var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });

  saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "widget-Results.xlsx");

  $("#generateExcelReport").removeAttr("disabled");
});

function s2ab(s) {
  var buf = new ArrayBuffer(s.length);
  var view = new Uint8Array(buf);
  for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
  return buf;
}

$("#all_PT_embedded").on("change", function () {
  var val = $("#all_PT_embedded").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("all_PT_embedded", val);
  all_PT_embedded = val;
  gadgets.window.adjustHeight();
});

$("#all_moduleLinks").on("change", function () {
  var val = $("#all_moduleLinks").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("all_moduleLinks", val);
  all_moduleLinks = val;
  gadgets.window.adjustHeight();
});

$("#all_PT_embedded_link").on("change", function () {
  var val = $("#all_PT_embedded_link").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("all_PT_embedded_link", val);
  all_PT_embedded_link = val;
  gadgets.window.adjustHeight();
});

$("#all_PT_specialCharacters").on("change", function () {
  var val = $("#all_PT_specialCharacters").prop("checked");
  var prefs = new gadgets.Prefs();
  prefs.set("all_PT_specialCharacters", val);
  all_PT_specialCharacters = val;
  gadgets.window.adjustHeight();
});

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();

    all_PT_embedded = prefs.getBool("all_PT_embedded");
    if (all_PT_embedded == true) $("#all_PT_embedded").prop("checked", true);

    all_moduleLinks = prefs.getBool("all_moduleLinks");
    if (all_moduleLinks == true) $("#all_moduleLinks").prop("checked", true);

    all_PT_specialCharacters = prefs.getBool("all_PT_specialCharacters");
    if (all_PT_specialCharacters == true) $("#all_PT_specialCharacters").prop("checked", true);

    all_PT_embedded_link = prefs.getBool("all_PT_embedded_link");
    if (all_PT_embedded_link == true) $("#all_PT_embedded_link").prop("checked", true);

    $("#verifyButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      lockAllButtons();
      $("#logs").attr("hidden", "hidden");
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html("<b>Message:</b> Verifing selected artifacts. PLEASE WAIT...");
      runningProcess = true;
      verifyArtifacts(artRef);

      gadgets.window.adjustHeight();
    });
  } else {
    clearSelectedInfo();
    lockButton("#verifyButton");
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

async function verifyArtifactsInModule() {
  runningProcess = true;
  logs = [];
  refs = [];
  errorMessages = [];
  $("#logs").attr("hidden", "hidden");
  $("#log").html(
    `<p class="p-1 m-0"><b>Log:</b><span id="all" class="badge badge-success" style="float:right;">Select all</span><br/><span id="none" class="badge badge-danger" style="float:right;">Deselect all</span></p>`
  );
  enableResultsControl();
  lockAllButtons();
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning");
  $(".status").html("<b>Message:</b> Verifing selected artifacts. PLEASE WAIT..");
  gadgets.window.adjustHeight();

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var opResult = await getModuleArtifacts(moduleRef);
      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockButton("#verifyInModuleButton");
        $("#rulesButton").removeAttr("disabled");
        $("#rulesButton").addClass("btn-warning");
        $("#rulesButton").removeClass("btn-secondary");
        $("#termButton").removeAttr("disabled");
        $("#termButton").removeClass("btn-secondary");
        $("#termButton").addClass("btn-dark");
        $(".status").removeClass("warning correct incorrect");
        runningProcess = false;
        $(".status")
          .addClass("incorrect")
          .html(
            "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
          );
      } else {
        var data = opResult.data;
        data = removeDuplicatesOfArtifacts(data);
        for (var k = 0; k < data.length; k++) {
          $(".status").removeClass("warning correct incorrect");
          $(".status").addClass("warning");
          var id = data[k].values[RM.Data.Attributes.IDENTIFIER];
          $(".status").html(
            `<b>Message:</b> Verifying artifact <b>${id}</b> (Progress: <b>${k + 1}/${data.length}</b>).`
          );
          await validate(data[k]);
        }

        await displayLogs(logs, errorMessages);

        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("correct");
        $(".status").html("<b>Message:</b> Verification process is finished.");
        gadgets.window.adjustHeight();
        runningProcess = false;
      }
    }
  });
}

function verifyArtifacts(ref) {
  logs = [];
  refs = [];
  errorMessages = [];
  $("#logs").attr("hidden", "hidden");
  $("#log").html(
    `<p class="p-1 m-0"><b>Log:</b><span id="all" class="badge badge-success" style="float:right;">Select all</span><br/><span id="none" class="badge badge-danger" style="float:right;">Deselect all</span></p>`
  );
  enableResultsControl();
  RM.Client.getCurrentConfigurationContext(function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      RM.Data.getAttributes(
        ref /* ArtifactAttributes[] */, // RULES#
        [
          RM.Data.Attributes.ARTIFACT_TYPE,
          RM.Data.Attributes.NAME,
          RM.Data.Attributes.IDENTIFIER,
          RM.Data.Attributes.PRIMARY_TEXT,
          RM.Data.Attributes.SECTION_NUMBER,
          RM.Data.Attributes.CONTAINING_MODULE,
        ],
        async function (opResult) {
          if (opResult.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            lockButton("#verifyButton");
            $("#rulesButton").removeAttr("disabled");
            $("#rulesButton").addClass("btn-warning");
            $("#rulesButton").removeClass("btn-secondary");

            $("#termButton").removeAttr("disabled");
            $("#termButton").removeClass("btn-secondary");
            $("#termButton").addClass("btn-dark");

            $("#_seParameterButton").removeAttr("disabled");
            $("#_seParameterButton").removeClass("btn-secondary");
            $("#_seParameterButton").addClass("btn-dark");

            $("#_seSRS_RqmtButton").removeAttr("disabled");
            $("#_seSRS_RqmtButton").removeClass("btn-secondary");
            $("#_seSRS_RqmtButton").addClass("btn-dark");

            $("#_sePspec_RqmButton").removeAttr("disabled");
            $("#_sePspec_RqmButton").removeClass("btn-secondary");
            $("#_sePspec_RqmButton").addClass("btn-dark");
            runningProcess = false;
            $(".status")
              .addClass("incorrect")
              .html(
                "<b>Error:</b> Something went wrong. Please check for locked artifacts, try again, or contact your administrator."
              );

            return;
          } else {
            var data = opResult.data;
            data = removeDuplicatesOfArtifacts(data);
            for (var k = 0; k < data.length; k++) {
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("warning");
              var id = data[k].values[RM.Data.Attributes.IDENTIFIER];
              $(".status").html(
                `<b>Message:</b> Verifying artifact <b>${id}</b> (Progress: <b>${k + 1}/${data.length}</b>).`
              );
              await validate(data[k]);
            }

            await displayLogs(logs, errorMessages);

            unlockAllButtons();
            $(".status").removeClass("warning correct incorrect");
            $(".status").addClass("correct");
            $(".status").html("<b>Message:</b> Verification process is finished.");
            gadgets.window.adjustHeight();
            runningProcess = false;
          }
        }
      );
    }
  });
}

async function displayLogs(logs, errorMessages) {
  if (logs.length > 0) {
    logs = removeDuplicates(logs);
    $("#logs").removeAttr("hidden");
    $("#log").html(
      `<p class="p-1 m-0"><b>Log:</b><span id="all" class="badge badge-success" style="float:right;">Select all</span><br/><span id="none" class="badge badge-danger" style="float:right;">Deselect all</span></p>`
    );
    enableResultsControl();
    logs.forEach(function (item, index) {
      var id = item.values[RM.Data.Attributes.IDENTIFIER];
      var name = item.values[RM.Data.Attributes.NAME];
      var type = item.values[RM.Data.Attributes.ARTIFACT_TYPE].name;

      var uri = item.ref.uri;
      var title = "";
      for (var z = 0; z < errorMessages.length; z++) {
        var element = errorMessages[z];
        if (uri == element[0]) {
          if (title == "") {
            title = element[1];
          } else {
            title = title + ",\n" + element[1];
          }
        }
      }
      refs.push(logs[index].ref);

      $("<p></p>")
        .html(
          `<p><ul><li title="${title}">${id}: ${name} <span class="badge badge-pill badge-info">${type}</span></li><ul></p>`
        )
        .appendTo("#log")
        .on("click", function () {
          RM.Client.setSelection(logs[index].ref);
        });
    });
  }
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (!runningProcess) {
    if (moduleRef != null) {
      unlockButton("#verifyInModuleButton");
    }

    if (selected.length === 1) {
      artRef = selected;
      unlockButton("#verifyButton");
      clearSelectedInfo();
      $(".status").html(`<b>Message:</b> Press button "Verify ${selected.length} artifact(s)`);
    } else if (selected.length > 1) {
      artRef = selected;
      unlockButton("#verifyButton");
      clearSelectedInfo();
      $(".status").html(`<b>Message:</b> Press button "Verify ${selected.length} artifact(s)`);
    } else {
      lockButton("#verifyButton");
      clearSelectedInfo();
      $(".status").html(
        `<b>Message:</b> Please select requirements for which You want to verify then press "Verify selected artefacts" button.`
      );
      gadgets.window.adjustHeight();
    }
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  lockButton("#verifyInModuleButton");
  $("#verifyInModuleButton").off("click");
  moduleRef = null;
  ref = null;
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();
    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");
    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    clearSelectedInfo();
    $(".status").addClass("incorrect").html("<b>Error:</b> Attribute name can not be empty.");
    lockButton("#verifyButton");
    return;
  }
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];
    if (
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE ||
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.COLLECTION
    ) {
      if (moduleRef != attrs.ref) {
        moduleRef = attrs.ref;
        lockButton("#verifyButton");
        unlockButton("#verifyInModuleButton");
        $("#verifyInModuleButton").on("click", verifyArtifactsInModule);
      }
    }
  }
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(
      // RULES#
      ref,
      [
        RM.Data.Attributes.ARTIFACT_TYPE,
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.IDENTIFIER,
        RM.Data.Attributes.PRIMARY_TEXT,
        RM.Data.Attributes.SECTION_NUMBER,
        RM.Data.Attributes.CONTAINING_MODULE,
      ],
      resolve
    );
  });
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

function lockAllButtons() {
  $("#verifyButton").attr("disabled", "disabled");
  $("#verifyButton").removeClass("btn-primary");
  $("#verifyButton").addClass("btn-secondary");
  $("#verifyInModuleButton").attr("disabled", "disabled");
  $("#verifyInModuleButton").removeClass("btn-primary");
  $("#verifyInModuleButton").addClass("btn-secondary");

  $("#rulesButton").attr("disabled", "disabled");
  $("#rulesButton").removeClass("btn-warning");
  $("#rulesButton").addClass("btn-secondary");

  $("#_sePspec_RqmButton").attr("disabled", "disabled");
  $("#_sePspec_RqmButton").removeClass("btn-dark");
  $("#_sePspec_RqmButton").addClass("btn-secondary");

  $("#_seSRS_RqmtButton").attr("disabled", "disabled");
  $("#_seSRS_RqmtButton").removeClass("btn-dark");
  $("#_seSRS_RqmtButton").addClass("btn-secondary");

  $("#_seParameterButton").attr("disabled", "disabled");
  $("#_seParameterButton").removeClass("btn-dark");
  $("#_seParameterButton").addClass("btn-secondary");

  $("#termButton").attr("disabled", "disabled");
  $("#termButton").removeClass("btn-dark");
  $("#termButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  if (moduleRef == null) {
    $("#verifyButton").removeAttr("disabled");
    $("#verifyButton").addClass("btn-primary");
    $("#verifyButton").removeClass("btn-secondary");
  }
  $("#rulesButton").removeAttr("disabled");
  $("#rulesButton").addClass("btn-warning");
  $("#rulesButton").removeClass("btn-secondary");

  $("#termButton").removeAttr("disabled");
  $("#termButton").removeClass("btn-secondary");
  $("#termButton").addClass("btn-dark");

  $("#_seParameterButton").removeAttr("disabled");
  $("#_seParameterButton").removeClass("btn-secondary");
  $("#_seParameterButton").addClass("btn-dark");

  $("#_seSRS_RqmtButton").removeAttr("disabled");
  $("#_seSRS_RqmtButton").removeClass("btn-secondary");
  $("#_seSRS_RqmtButton").addClass("btn-dark");

  $("#_sePspec_RqmButton").removeAttr("disabled");
  $("#_sePspec_RqmButton").removeClass("btn-secondary");
  $("#_sePspec_RqmButton").addClass("btn-dark");

  if (moduleRef == null) return;
  $("#verifyInModuleButton").removeAttr("disabled");
  $("#verifyInModuleButton").addClass("btn-primary");
  $("#verifyInModuleButton").removeClass("btn-secondary");
}
function removeDuplicates(arr) {
  const uniqueIds = [];

  const unique = arr.filter((element) => {
    const isDuplicate = uniqueIds.includes(element.ref);

    if (!isDuplicate) {
      uniqueIds.push(element.ref);

      return true;
    }
  });
  return unique;
}

function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

async function validate(artAttrs) {
  console.log("Veryfining artifact: " + artAttrs.ref.uri);
  var name = artAttrs.values[RM.Data.Attributes.NAME];
  if (name == null || name == undefined) name = "";
  var id = artAttrs.values[RM.Data.Attributes.IDENTIFIER];
  if (id == null || id == undefined) id = "";
  var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE].name;
  if (type == null || type == undefined) type = "";
  var primaryText = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
  if (primaryText == null || primaryText == undefined) primaryText = "";

  var sectionNumber = artAttrs.values[RM.Data.Attributes.SECTION_NUMBER];
  if (sectionNumber == null || sectionNumber == undefined) sectionNumber = "";

  var containingModule = artAttrs.values[RM.Data.Attributes.CONTAINING_MODULE];
  if (containingModule == null || containingModule == undefined) containingModule = "";

  if (true) {
    // kill switch for checking of all the rules

    if (all_moduleLinks == true) {
      // Executed in the module only
      if (sectionNumber != "" && containingModule != "") {
        var result = await getLinksForArtifact(artAttrs.ref);
        if (result.code == RM.OperationResult.OPERATION_OK) {
          var artLinks = result.data.artifactLinks;
          var externalLinks = result.data.externalLinks;
          var number = 0;

          for (var item of artLinks) {
            if (item.art.moduleUri != null) {
              number = number + 1;
            }
          }

          for (var item of externalLinks) {
            if (item.art.moduleUri != null) {
              number = number + 1;
            }
          }

          if (number != 0) {
            logs.push(artAttrs);
            errorMessages.push([
              artAttrs.ref.uri,
              "All artifacts - Module links (Works only in the opened module).",
              id,
              name,
              type,
            ]);
          }
        }
      }
    }

    if (all_PT_specialCharacters == true) {
      // display special characters like greek letters with another font
      // capital greek letters
      if (primaryText.indexOf(String.fromCharCode(913)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(913),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(914)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(914),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(915)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(915),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(916)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(916),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(917)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(917),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(918)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(918),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(919)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(919),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(920)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(920),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(921)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(921),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(922)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(922),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(923)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(923),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(924)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(924),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(925)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(925),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(926)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(926),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(927)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(927),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(928)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(928),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(930)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(930),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(931)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(931),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(932)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(932),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(933)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(933),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(934)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(934),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(935)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(935),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(936)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(936),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(937)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(937),
          id,
          name,
          type,
        ]);
      }

      // small greek letters

      if (primaryText.indexOf(String.fromCharCode(945)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(945),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(946)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(946),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(947)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(947),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(948)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(948),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(949)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(949),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(950)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(950),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(951)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(951),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(952)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(952),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(953)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(953),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(954)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(954),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(955)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(955),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(956)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(956),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(957)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(957),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(958)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(958),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(958)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(958),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(959)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(959),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(960)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(960),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(961)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(961),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(962)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(962),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(963)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(963),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(964)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(964),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(965)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(965),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(966)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(966),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(967)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(967),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(968)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(968),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(969)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(969),
          id,
          name,
          type,
        ]);
      }

      // (math) symbols

      if (primaryText.indexOf(String.fromCharCode(36)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(36),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(43)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(43),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf("&gt;") > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(62),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf("&lt;") > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(60),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(169)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(169),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(172)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(172),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(174)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(174),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(176)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(176),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(177)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(177),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(178)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(178),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(179)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(179),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(185)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(185),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(188)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(188),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(189)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(189),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(190)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(190),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(215)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(215),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(223)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(223),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(247)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(247),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(307)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(307),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(457)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(457),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(460)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(460),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8364)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8364),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8482)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8482),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8592)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8592),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8594)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8594),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8656)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8656),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8658)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8658),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8660)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8660),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8706)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8706),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8711)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8711),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8719)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8719),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8721)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8721),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8722)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8722),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8730)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8730),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8731)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8730),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8732)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8730),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8734)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8730),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8736)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8736),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8737)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8737),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8738)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8738),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8741)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8741),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8742)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8742),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8743)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8743),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8744)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8744),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8747)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8747),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8748)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8748),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8749)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8749),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8750)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8750),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8751)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8751),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8752)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8752),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8764)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8764),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8764)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8764),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8776)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8776),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8793)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8793),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8793)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8793),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8800)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8800),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8801)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8801),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8804)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8804),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8805)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8805),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8806)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8806),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8807)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8807),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(8960)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(8960),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(9744)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(9744),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(9746)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(9746),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(64257)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(64257),
          id,
          name,
          type,
        ]);
      }

      if (primaryText.indexOf(String.fromCharCode(64258)) > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Special characters in the primary text that can not be printed " +
            String.fromCharCode(64258),
          id,
          name,
          type,
        ]);
      }
    }

    if (all_PT_embedded == true) {
      if (primaryText.indexOf("com-ibm-rdm-editor-EmbeddedResourceDecorator") > -1) {
        logs.push(artAttrs);
        errorMessages.push([
          artAttrs.ref.uri,
          "All artifacts - Primary text of artifact - Embedded artifacts in the primary text",
          id,
          name,
          type,
        ]);
      }
    }

    if (all_PT_embedded_link == true) {
      if (primaryText.indexOf("com-ibm-rdm-editor-EmbeddedResourceDecorator") > -1) {
        var links = [];
        var obj = [];
        obj = primaryText.match(/class=\"embedded\"\shref=\"((.*?))\"/gi);

        var result = await getLinksForArtifact(artAttrs.ref);
        if (result.code == RM.OperationResult.OPERATION_OK) {
          var artLinks = result.data.artifactLinks;
          for (var z = 0; z < artLinks.length; z++) {
            if (
              artLinks[z].linktype.uri == "http://www.ibm.com/xmlns/rdm/types/Embedding" &&
              artLinks[z].linktype.direction == "_SUB"
            ) {
              for (var q = 0; q < artLinks[z].targets.length; q++) {
                links.push(artLinks[z].targets[q].uri);
              }
            }
          }
        }
        if (obj != null && obj != undefined) {
          for (var i = 0; i < obj.length; i++) {
            var flag = false;
            var uri = obj[i];
            uri = uri.split('embedded" ')[1];
            uri = uri.replace('href="', "");
            uri = uri.replace('"', "");

            for (var g = 0; g < links.length; g++) {
              if (links[g].indexOf(uri) > -1) {
                flag = true;
              }
            }

            if (flag == false) {
              logs.push(artAttrs);
              errorMessages.push([
                artAttrs.ref.uri,
                "All artifacts - Primary text of artifact - Embedded artifacts in the primary text and embedded link",
                id,
                name,
                type,
              ]);
            }
          }
        }
      }
    }
  }
}

function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

function removeDuplicatesOfArtifacts(arr) {
  var ids = "";
  var tmpArray = [];
  for (var i = 0; i < arr.length; i++) {
    var uri = arr[i].ref.uri;
    var str = uri.split("/rm/resources/")[1];
    if (ids.indexOf(str) < 0) {
      tmpArray.push(arr[i]);
    }
    ids = ids + str + ",";
  }
  return tmpArray;
}
