/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2021
 * Contact information: info@reqpro.com
 */

var moduleRef;
var configurationRef;
var componentRef;
var artifacts = [];
var artifactsNoStrucutre = [];
var idURIMap = new Map();
var headingType;
var maxLevel = 0;
var usedIds = new Map();

$(function () {
  if (window.RM) {
    var prefs = new gadgets.Prefs();
    headingType = prefs.getString("headingType");
    lockAllButtons();
    gadgets.window.adjustHeight();
    $("#action0").unbind();
    $("#action1").unbind();

    $("#action0").on("click", () => {
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html(
        `<b> Info:</b> Searching for requirements and checking proposed structure. Please wait until done.`
      );
      lockAllButtons();
      gadgets.window.adjustHeight();
      setTimeout(() => {
        analyzeArtifacts();
        unlockButton("#action0");
        gadgets.window.adjustHeight();
      }, 1000);
    });

    function analyzeArtifacts() {
      return new Promise((res, rej) => {
        action();
        res({});
      });
    }

    $("#action1").on("click", () => {
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      $(".status").html(`<b> Info:</b> Building module structure using selected artifacts.`);
      lockAllButtons();
      gadgets.window.adjustHeight();
      setTimeout(() => {
        fillModule(moduleRef.uri, configurationRef, componentRef);
        unlockAllButtons();
        gadgets.window.adjustHeight();
      }, 500);
    });
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    $("#artifactList").attr("disabled", "disabled");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  moduleRef = null;
  lockAllButtons();
});

// Subscribe to the Artifact Selection event

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning").html("");
  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");
}

function getParametersURL(componentURL, projectURL, configurationURL) {
  var serverURL = projectURL.split("/process/project-areas/")[0];

  var query = serverURL + "/types?resourceContext=" + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open("GET", query, false);
  parameter_req.setRequestHeader("Accept", "application/xml");
  parameter_req.setRequestHeader("Content-Type", "application/xml");
  parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
  parameter_req.setRequestHeader("Configuration-Context", configurationURL);

  parameter_req.send();
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rdm/rdf/", "ObjectType");

    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          if (object.childNodes[c].innerHTML == testCaseTemplateName) {
            for (var g = 0; g < object.childNodes.length; g++) {
              if (object.childNodes[g].nodeName == "rm:objectTypeRole") {
                var objTmp = object.childNodes[g];

                if (objTmp.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource")) {
                  var parameterURI = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");

                  return parameterURI;
                }
              }
            }
          }
        }
      }
    }
  } catch (err) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status")
      .addClass("incorrect")
      .html(`<b>Status:</b> Something went wrong please contact your administator or try again.`);

    return;
  }
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();

    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");

    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    // *** DEBUG MODE

    // ***
    return projectURI;
  } catch (err) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");

    return;
  }
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [RM.Data.Attributes.IDENTIFIER, RM.Data.Attributes.NAME, RM.Data.Attributes.PRIMARY_TEXT],
      resolve
    );
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

function lockAllButtons() {
  $("#action0").attr("disabled", "disabled");
  $("#action0").removeClass("btn-primary");
  $("#action0").addClass("btn-secondary");
  $("#action1").attr("disabled", "disabled");
  $("#action1").removeClass("btn-success");
  $("#action1").addClass("btn-secondary");
}

function unlockAllButtons() {
  if (moduleRef == null) return;
  $("#action0").removeAttr("disabled");
  $("#action0").addClass("btn-primary");
  $("#action0").removeClass("btn-secondary");
  $("#action1").removeAttr("disabled");
  $("#action1").addClass("btn-success");
  $("#action1").removeClass("btn-secondary");
  gadgets.window.adjustHeight();
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).removeClass("btn-success");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  if (moduleRef == null) return;
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];
    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      unlockButton("#action0");
    }
  }
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri, configurationURL) {
  var tmp = uri.replace("/resources/", "/publish/resources?resourceURI=");
  var url_req = new XMLHttpRequest();
  url_req.open("GET", tmp, false);
  url_req.setRequestHeader("Accept", "application/xml");
  url_req.setRequestHeader("Content-Type", "application/xml");

  if (configurationURL != null || configurationURL != undefined || configurationURL != "") {
    url_req.setRequestHeader("Configuration-Context", configurationURL);
  }

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rrm/1.0/", "core");
    var newURL = url[0].innerHTML;
    var type = "";
    var oType = url_req.responseXML.getElementsByTagNameNS("http://jazz.net/xmlns/alm/rm/attribute/v0.1", "objectType");
    if (oType) {
      type = oType[0].getAttributeNS("http://jazz.net/xmlns/alm/rm/attribute/v0.1", "name");
    }

    var isHeading = false;
    if (type === headingType) isHeading = true;

    // tutaj zmienic 2 ponizsze pozycje
    if (newURL) return { uri: uri.split("/resources/")[0] + "/resources/" + newURL, isHeading: isHeading };
    else return { uri: uri, isHeading: isHeading };
  } catch (err) {
    return;
  }
}

function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

async function action() {
  if (headingType == null || headingType == undefined || headingType == "") {
    unlockButton("#action0");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Status:</b> Artifact type of heading can't be empty.");
    return;
  }

  idURIMap = new Map();

  var requirementArrayID = [];

  let regexp = /\[([\d]*,[\d.]*)\]/g;

  var text = $("#artifactList").val();

  var artifactsInternal = text.match(regexp);

  if (
    text == null ||
    text == undefined ||
    text.length == 0 ||
    artifactsInternal == null ||
    artifactsInternal.length == 0
  ) {
    unlockButton("#action0");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html("<b>Status:</b> List of artifacts is empty.");
    return;
  }

  for (artefact of artifactsInternal) {
    artefact = artefact.replace("[", "").replace("]", "");
    artefact = artefact.split(",");
    var id = artefact[0];
    var level = artefact[1];
    if (id == null || id == "" || id.length == 0 || level == null || level == "" || level.length == 0) {
      unlockButton("#action0");
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning").html("<b>Status:</b> List of artifact is not valid");
    }
    requirementArrayID.push([id, level]);
  }

  $(".status").removeClass("warning correct incorrect");
  $(".status")
    .addClass("warning")
    .html(`<b>Status:</b> Getting information about ${requirementArrayID.length} found requirements.`);

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      var componentURL = context.localComponentUri;
      componentRef = componentURL;
      var configurationURL = context.localConfigurationUri;
      configurationRef = configurationURL;
      var projectURL = getProjectAreaURL(componentURL);
      var serverURL = projectURL.split("/process/project-areas/")[0];

      artifactsNoStrucutre = generateParents(requirementArrayID);

      var result = validation(artifactsNoStrucutre);

      if (!result) {
        unlockButton("#action0");
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("incorrect")
          .html(`<b>Status:</b> The artifacts are not organized in the valid structure.`);
        gadgets.window.adjustHeight();
        return;
      }

      artifacts = structure(artifactsNoStrucutre, 0, artifactsNoStrucutre.length, 0);

      for (item of requirementArrayID) {
        var check = idURIMap.get(item[0]);
        if (check != undefined || check != null) {
          console.log(`Data for artifact ${item[0]} are already cached.`);
        } else {
          console.log("Getting information about requirement " + item[0] + ".");
          var rm = getRequirementURL(serverURL, projectURL, configurationURL, item[0]);

          if (rm == null || rm == undefined || rm == "") {
            unlockButton("#action0");
            $(".status").removeClass("warning correct incorrect");
            $(".status")
              .addClass("incorrect")
              .html(
                `<b>Status:</b> Artifact with id: ${item[0]} does not exist in the current steam or is a module/collection.`
              );
            gadgets.window.adjustHeight();
            return;
          }
          var baseObject = uriToPublishURI(rm, configurationURL);

          if (baseObject == null || baseObject == undefined) {
            unlockButton("#action0");
            $(".status").removeClass("warning correct incorrect");
            $(".status")
              .addClass("incorrect")
              .html(`<b>Status:</b> Something went wrong please contact your administator or try again.`);
            gadgets.window.adjustHeight();
            return;
          }
          idURIMap.set(item[0], baseObject);
        }
      }

      unlockAllButtons();
      $(".status").removeClass("warning correct incorrect");
      $(".status")
        .addClass("correct")
        .html(
          `<b>Status:</b> Step 1 done, found ${artifactsNoStrucutre.length} artifact(s). Execute Fill a module function.`
        );
      gadgets.window.adjustHeight();
    } else {
      unlockButton("#action0");
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("incorrect").html("<b>Status:</b> Widget was not able to get project data.");
    }
  });

  return;
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(ref, resolve);
  });
}

function generateParents(arr) {
  var tmp = [];

  //TODO: Dodac walidacje
  // Piersze zero
  // Kolejne mniejsze tylko o jeden
  // Wieksze dowolnie

  for (item of arr) {
    var level = item[1].toString().split(".").length - 1;
    var i = {};
    i["id"] = item[0];
    i["level"] = level;
    tmp.push(i);
  }

  return tmp;
}

function xmlEncodeCharacters(string) {
  string = string.replace(/&(?!.+;)/g, "&amp;");
  string = string.replace(/</g, "&lt;");
  string = string.replace(/>/g, "&gt;");
  string = string.replace(/'/g, "&#39;");
  string = string.replace(/"/g, "&#34;");

  return string;
}

function prepareAcceptanceCritieria(string) {
  string = string.replace(/&nbsp;/gi, " ");
  string = string.replace(/<style([\s\S]*?)<\/style>/gi, "");
  string = string.replace(/<script([\s\S]*?)<\/script>/gi, "");
  string = string.replace(/<\/div>/gi, "");
  string = string.replace(/<\/li>/gi, "\n");
  string = string.replace(/<li(.|\n|\d)*?>/gi, "  -  ");
  string = string.replace(/<\/ul>/gi, "\n");
  string = string.replace(/<\/p>/gi, "\n");
  string = string.replace(/<br\s*[\/]?>/gi, "\n");
  string = string.replace(/<[^>]+>/gi, "");

  return string;
}

function getRequirementURL(rmURL, projectNameURI, configurationURI, id) {
  var projectURL =
    rmURL +
    `/views?oslc.query=true&projectURL=${projectNameURI}&oslc.prefix=dcterms=<http://purl.org/dc/terms/>&oslc.where=dcterms:identifier=${id}`;

  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", projectURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");
  projectArea_req.setRequestHeader("OSLC-Core-Version", "2.0");

  if (configurationURI != null || configurationURI != undefined || configurationURI != "") {
    projectArea_req.setRequestHeader("Configuration-Context", configurationURI);
  }

  try {
    projectArea_req.send();
    http: var requirement = projectArea_req.responseXML.getElementsByTagNameNS(
      "http://open-services.net/ns/rm#",
      "Requirement"
    );

    var requirementURI = requirement[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
    // *** DEBUG MODE

    return requirementURI;
  } catch (err) {
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");

    return;
  }
}

function fillModule(moduleURI, configurationURI, componentURI) {
  var url = moduleURI + "/structure";
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", url, false);
  projectArea_req.setRequestHeader("Accept", "application/rdf+xml");
  projectArea_req.setRequestHeader("Content-Type", "application/rdf+xml");
  projectArea_req.setRequestHeader("DoorsRP-Request-Type", "public 2.0");
  if (configurationURI != null || configurationURI != undefined || configurationURI != "") {
    projectArea_req.setRequestHeader("Configuration-Context", configurationURI);
  }
  try {
    projectArea_req.send();

    if (projectArea_req.status != 200) {
      $(".status").removeClass("warning correct incorrect");
      $(".status")
        .addClass("incorrect")
        .html(`<b>Status:</b> Something went wrong please contact your administator or try again.`);
      gadgets.window.adjustHeight();
      return;
    }

    var oldResponse = new XMLSerializer().serializeToString(projectArea_req.responseXML.documentElement);
    var etag = projectArea_req.getResponseHeader("etag");
    var emptyModule = oldResponse.split(`j.0:childBindings`);

    if (emptyModule.length > 2) {
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning").html(`<b>Error:</b> Module is not empty.`);
      return;
    }

    var payload =
      `<j.0:childBindings rdf:parseType="Collection">` +
      generatePayload(artifacts, componentURI, moduleURI) +
      `</j.0:childBindings>`;

    var newResponse = oldResponse.replace(
      `<j.0:childBindings rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>`,
      payload
    );

    projectArea_req = new XMLHttpRequest();
    projectArea_req.open("PUT", url, false);
    projectArea_req.setRequestHeader("Accept", "application/rdf+xml");
    projectArea_req.setRequestHeader("Content-Type", "application/rdf+xml");
    projectArea_req.setRequestHeader("DoorsRP-Request-Type", "public 2.0");
    projectArea_req.setRequestHeader("If-Match", etag);

    if (configurationURI != null || configurationURI != undefined || configurationURI != "") {
      projectArea_req.setRequestHeader("Configuration-Context", configurationURI);
    }

    projectArea_req.send(newResponse);
    var location = projectArea_req.getResponseHeader("location");
    console.log("Result: " + location);

    if (projectArea_req.status != 202) {
      $(".status").removeClass("warning correct incorrect");
      $(".status")
        .addClass("incorrect")
        .html(`<b>Status:</b> Something went wrong please contact your administator or try again.`);
      gadgets.window.adjustHeight();
      return;
    } else {
    }
    $(".status").removeClass("warning correct `incorrect`");
    $(".status")
      .addClass("correct")
      .html(
        `<b>Status:</b> Module was filled with selected artifacts. Please refresh the artifact or page to see the results.`
      );

    return;
  } catch (err) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status")
      .addClass("incorrect")
      .html(`<b>Status:</b> Something went wrong please contact your administator or try again.`);
    return;
  }
}

function generatePayload(elements, componentURI, moduleURI) {
  var payload = "";
  for (item of elements) {
    var id = item.id;
    var obj = idURIMap.get(id);

    var post = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();

    if (item.children.length == 0) {
      payload =
        payload +
        `
      <j.0:Binding rdf:about="${obj.uri}/strucure#${post}">
      <j.0:isHeading rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">${obj.isHeading}</j.0:isHeading>
      <oslc_config:component rdf:resource="${componentURI}"/>
      <j.0:boundArtifact rdf:resource="${obj.uri}"/>
      <j.0:module rdf:resource="${moduleURI}"/>
      <j.0:childBindings rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
      </j.0:Binding>`;
    } else {
      payload =
        payload +
        `
      <j.0:Binding rdf:about="${obj.uri}/strucure#${post}">
      <j.0:isHeading rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">${obj.isHeading}</j.0:isHeading>
      <oslc_config:component rdf:resource="${componentURI}"/>
      <j.0:boundArtifact rdf:resource="${obj.uri}"/>
      <j.0:module rdf:resource="${moduleURI}"/>
      <j.0:childBindings rdf:parseType="Collection">${generatePayload(
        item.children,
        componentURI,
        moduleURI
      )}</j.0:childBindings>
      </j.0:Binding>`;
    }
  }
  return payload;
}

function validation(arr) {
  var valid = true;
  var first = true;
  var previousLevel = 0;
  for (item of arr) {
    if (first) {
      first = false;
      if (item.level != 0) {
        valid = false;
        return valid;
      }
    }
    if (item.level == previousLevel || previousLevel == item.level - 1 || previousLevel > item.level) {
      previousLevel = item.level;
      valid = true;
    } else {
      valid = false;
      return valid;
    }
  }
  return valid;
}

function structure(arr, start, end, level) {
  var tmp = [];
  var i = start;
  while (i < end) {
    if (arr[i].level < level) {
      return tmp;
    } else if (arr[i].level == level) {
      var children = structure(arr, i + 1, arr.length, level + 1);
      tmp.push({ id: arr[i].id, index: i, children: children });
    }
    i++;
  }
  return tmp;
}
